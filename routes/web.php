<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$adminURL = env('APP_ADMIN_URL', 'test');
//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();
//Внешняя ссылка c редиректом ?i=ссылка-внешняя
Route::get('/link', 'Front\LinkController@redirectToLinks');

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/navlogin/referer', 'Auth\AuthController@saveRefererToSession');

////auditor bot NEWS
Route::get('/auditorbots', 'Bots\Auditor\AuditorUrlsController@listDomain')->name('listDomain');
Route::get('/auditorbotlist', 'Bots\Auditor\AuditorUrlsController@list_url')->name('auditorbotlist');
Route::get('/twoturn', 'Bots\Auditor\AuditorTwoTurnController@index')->name('twoturn');


Route::get('/test_url', 'Bots\Auditor\AuditorUrlsController@test_url')->name('test.url');


//Route::get('/auditorlist', 'Bots\Auditor\AuditorUrlsController@startUrl')->name('startUrl');
//
Route::get('/startparsurls/{domain}', 'Bots\Auditor\AuditorUrlsController@startParseUrls')->name('startParsUrls');
Route::get('/tableTmp', 'Bots\Auditor\AuditorUrlsController@tableTmp')->name('tableTmp');
//
////JewelerBot
Route::get('/jewelerbot', 'Bots\JewelerBot\JewelerController@analysisContent')->name('jewelerbot');
Route::get('/jewelerbot/tags', 'Bots\JewelerBot\JewelerController@analysisTags')->name('analysisTags');
Route::get('/jewelerbot/search-links', 'Bots\JewelerBot\JewelerController@analysisLiks')->name('analysisLiks');
//Route::get('/jewelerbot/tags/delete', 'Bots\JewelerBot\JewelerController@deleteContent')->name('analysisTagsdd');
//Route::get('/jewelerbot/elasticIndexisContent', 'Bots\ElasticIndex\ElasticMetaController@elasticIndexisContent')->name('analysisContentUrl');
//
////auditor bot
//Route::get('/auditor_bot', 'AuditorBot\AuditorController@index')->name('index');
//Route::get('/auditor_bot/clear_links', 'AuditorBot\AuditorController@clearLinks')->name('index');
////miner bot
//Route::get('/miner_bot', 'MinerBot\MinerController@index')->name('index');
////miner bot NEWS
//Route::get('/miner_bot_tags', 'Bots\Miner\MinerContentController@parseMetaTags')->name('parseMetaTags');
//Route::get('/miner_bot_tags_news', 'Bots\Miner\MinerContentController@parseMetaTagsNew')->name('parseMetaTagsNew');
//Route::get('/miner_bot_tags_newss', 'Bots\Miner\MinerContentController@parseMetaTagsNew')->name('parseMetaTagsNew');

//Admin middleware
Route::group(
    [
        'prefix' => $adminURL,
        'namespace' => 'Admin',
        'as' => 'admin.',
        'middleware' => ["admin","auth"],
    ],
    function(){
        Route::get('/test_link',['as' => 'dashboard','uses' => 'DashboardController@testing']);
        Route::get('/',['as' => 'dashboard','uses' => 'DashboardController@index']);
        Route::get('/fresh_content',['as' => 'dashboard','uses' => 'DashboardController@freshContent']);
        Route::post('/ajax/stat', 'DashboardController@statistics');
        /**
         * Routs Domains
         */
        Route::resource('/domains', 'DomainsController');
        Route::get('/domains/type/{type}', 'DomainsController@type')->name('domains.type');
        Route::post('/domains/error-parser', 'DomainsController@error_parser')->name('domains.error.parser');
        Route::post('/domains/tag/delete', 'DomainsController@tagDelete');
        Route::post('/domains_enable',['as' => 'dashboard','uses' => 'DomainsController@onOffDomains']);
        Route::post('/domains/get', 'DomainsController@getDomains')->name('domains.get');

        /**
         * Routs Tags
         */
        Route::get('/tags',['uses' => 'TagsController@index'])->name('tags.index');
        Route::get('/tags/add_tags', 'TagsController@addTag')->name('tags.add_tags');
        Route::post('/tags/add_tags',['as' => 'dashboard','uses' => 'TagsController@addTag']);
        Route::get('/tags/delete/{id}',['as' => 'dashboard','uses' => 'TagsController@delete']);
        Route::post('/tags/get', 'TagsController@getTags')->name('tags.get');
        Route::post('/tags/show_switch_active', 'TagsController@showSwitchActive')->name('tags.show.switch.active');
        Route::post('/tags/search', 'TagsController@search')->name('tags.search');
        Route::get('/tags/slugs-tags-gen', 'TagsController@slugsTagsGen')->name('tags.slugs_tags_gen');
        Route::get('/tags/edit/{id}', 'TagsController@edit')->name('tags.edit_get');
        Route::post('/tags/edit/{id}','TagsController@edit')->name('tags.edit_post');
        /**
         * Routs Proxy
         */
        Route::get('/proxy',['as' => 'dashboard','uses' => 'ProxyController@index']);
        Route::get('/proxy/addProxyForm',['as' => 'dashboard','uses' => 'ProxyController@forms']);
        Route::post('/proxy/addProxy',['as' => 'dashboard','uses' => 'ProxyController@formsSend']);
        /**
         * Routs hint with dictionary
         */
        Route::get('/hint_with_dictionary',['as' => 'dashboard','uses' => 'DictionaryController@index']);
        Route::get('/hint_with_dictionary/add',['as' => 'dashboard','uses' => 'DictionaryController@create']);
        Route::post('/hint_with_dictionary/add',['as' => 'dashboard','uses' => 'DictionaryController@create']);
        Route::get('/hint_with_dictionary/add_file',['as' => 'dashboard','uses' => 'DictionaryController@create_file']);
        Route::post('/hint_with_dictionary/add_file',['as' => 'dashboard','uses' => 'DictionaryController@create_file']);
        /**
         * Routs search history
         */
        Route::get('/search_history',['as' => 'dashboard','uses' => 'SearchHistoryController@index']);

        /**
         * Routs group pages
         */
        Route::get('/pages',['as' => 'dashboard','uses' => 'PagesController@index']);
        Route::group(['prefix' => 'pages'], function () {
            Route::get('/create',['as' => 'dashboard','uses' => 'PagesController@create']);
            Route::post('/store',['as' => 'dashboard','uses' => 'PagesController@store']);
            Route::get('/destroy/{id}',['as' => 'dashboard','uses' => 'PagesController@destroy']);
            Route::get('/edit/{id}',['as' => 'dashboard','uses' => 'PagesController@edit']);
            Route::post('/edit/{id}',['as' => 'dashboard','uses' => 'PagesController@update']);

        });
        Route::get('/demons',['as' => 'dashboard','uses' => 'DemonsController@index']);
        Route::group(['prefix' => 'demons'], function () {
            Route::get('/create',['as' => 'dashboard','uses' => 'DemonsController@create']);
            Route::post('/store',['as' => 'dashboard','uses' => 'DemonsController@store']);
            Route::get('/destroy/{id}-{id_pid}',['as' => 'dashboard','uses' => 'DemonsController@destroy']);


        });
        //Routes settings
        Route::group(['prefix' => 'settings'], function () {
            Route::get('/paser',['as' => 'dashboard','uses' => 'SettingsParserController@index']);
            Route::post('/paser/delete',['as' => 'dashboard','uses' => 'SettingsParserController@delete']);
            Route::get('/paser/create',['as' => 'dashboard','uses' => 'SettingsParserController@create']);
            Route::post('/paser/create',['as' => 'dashboard','uses' => 'SettingsParserController@store']);
            Route::post('/paser/stopstart',['as' => 'dashboard','uses' => 'SettingsParserController@stopstart']);
            Route::post('/paser/setting',['as' => 'dashboard','uses' => 'SettingsParserController@setting']);
            Route::get('/paser/status',['as' => 'dashboard','uses' => 'SettingsParserController@ajax_status_parser']);

            Route::get('/tags', 'SettingsParserController@tags')->name('settings.tags');
            Route::post('/tags', 'SettingsParserController@tagsPost')->name('settings.tagsPost');
        });
        //Routes organizers
        Route::resource('/organizers', 'OrganizerController');
        Route::post('/organizers/loading_city', 'OrganizerController@loading_city')->name('organizers.loading.city');

        //Routes channels
        Route::get('/channels',['as' => 'channels','uses' => 'ChannelsController@index']);
        Route::group(['prefix' => 'channels'], function () {
            Route::get('/create',['as' => 'channels1','uses' => 'ChannelsController@create']);
            Route::post('/store',['as' => 'channels2','uses' => 'ChannelsController@store']);
            Route::get('/destroy/{id}',['as' => 'channels3','uses' => 'ChannelsController@destroy']);
            Route::get('/edit/{id}','ChannelsController@edit')->name('admin.channeleditget');
            Route::post('/edit/{id}',['as' => 'channeleditpost','uses' => 'ChannelsController@update']);
            Route::get('/generate',['as' => 'channels4','uses' => 'ChannelsController@generate']);
        });
        //Routes posts
        Route::get('/posts',['as' => 'posts','uses' => 'PostsController@index']);
        Route::group(['prefix' => 'posts'], function () {
            Route::get('/moderation',['as' => 'posts.moderation','uses' => 'PostsController@moderationPosts']);
            Route::get('/approved',['as' => 'posts.approved','uses' => 'PostsController@approvedPosts']);
            Route::get('/create',['as' => 'posts.create','uses' => 'PostsController@create']);
            Route::post('/store',['as' => 'posts.store','uses' => 'PostsController@store']);
            Route::get('/destroy/{id}',['as' => 'posts.destroy','uses' => 'PostsController@destroy']);
            Route::get('/edit/{id}',['as' => 'posts.edit','uses' => 'PostsController@edit']);
            Route::post('/edit/{id}',['as' => 'posts.update','uses' => 'PostsController@update']);
        });
        //Routes statistics
        Route::get('/statistics', 'StatisticsController@index')->name('statistics');
        Route::get('/statistics/tags', 'StatisticsController@tags')->name('statistics.tags');
        Route::post('/statistics/tags/chart_new', 'StatisticsController@chartNewTags')->name('statistics.tags.chart.new');
        Route::get('/statistics/domains', 'StatisticsController@domains')->name('statistics.domains');
        Route::get('/statistics/domains/params', 'StatisticsController@domainsParams')->name('statistics.domains.params');

        Route::get('/statistics/click', 'StatisticsController@clickLinks')->name('statistics.click.links');

        //Routes parser logs
        Route::get('/statistics/parser_logs', 'ParserLogController@index')->name('statistics.parser.logs');
        Route::get('/statistics/parser_logs/{id}', 'ParserLogController@showProcessLogs')->name('statistics.parser.logs.process');
        Route::post('/statistics/parser_logs/switch_active', 'ParserLogController@switchActive')->name('statistics.parser.logs.switch.active');

        /**
         * Routs Seo
         *
         */
        Route::resource('seo', 'SeoController');
        Route::post('/seo/switch_active', 'SeoController@switchActive')->name('seo.switch.active');
    });

/**
 * rounts users front
 */
//Live search
Route::get('/live_search', 'HomePageController@liveSearch')->name('index');

Route::get('/', 'Front\SearchContentController@search')->name('main.page');
Route::post('/highlighting', 'Front\SearchContentController@highlighting');

Route::post('/tips', 'HomePageController@tips');

Route::get('/tags', 'Front\TagsController@index')->name('tags');
Route::post('/tags', 'Front\TagsController@index')->name('tags');
Route::post('/tags/add-user', 'Front\TagsController@addUser')->name('tags.add-user');
Route::post('/tags', 'Front\TagsController@index');
Route::get('/tag/{tags}', 'Front\TagsController@urls')->name('tags.urls');

Route::get('/pages/{slug}', 'Front\PagesController@page');

Route::get('/searches', 'Front\SearchContentController@testSearch');

// Front pages
/*
 * feeds pages
 */
Route::get('/feeds', 'Front\FeedsController@index')->name('front.feeds');
Route::get('/archive', 'Front\FeedsController@archive_feeds');

Route::get('/profile/site/add', 'Front\ProfileController@siteAdd')->name('front.profile.site.add');
Route::post('/profile/site/store', 'Front\ProfileController@store')->name('front.profile.site.store');

Route::get('/profile/posts/my', 'Front\ProfileController@myPosts')->name('front.profile.posts.my');
Route::get('/profile/posts/my/moderation', 'Front\ProfileController@myModerationPosts')->name('front.profile.posts.my.moderation');
Route::get('/profile/posts/my/approved', 'Front\ProfileController@myApprovedPosts')->name('front.profile.posts.my.approved');
Route::get('/profile/posts/add', 'Front\ProfileController@postsAdd')->name('front.profile.posts.add');
Route::post('/profile/posts/store', 'Front\ProfileController@postsStore')->name('front.profile.posts.store');
Route::get('/profile/send-admin', 'Front\ProfileController@adminNewPostSendMail')->name('front.profile.admin_send');

Route::get('/blog', 'Front\BlogController@index')->name('front.blog');
Route::get('/blog/{id}', 'Front\BlogController@post')->name('front.blog.post');

//ajax request front
//add user tags
Route::get('/offer-tags', function (){
    if(\request()->ajax()){
        return view('Domains.forms.add_user_tags');
    } else {
        abort(404);
    }
})->name('offer-tags');

/*
 * domains pages
 */
Route::get('/domains/{name_domains}', 'Front\DomainsController@domain')->name('front.domains');
Route::get('/domains/{name_domains}/tags', 'Front\DomainsController@domainTags')->name('front.domains.tags');
/*
 * organizers pages
 */
Route::get('/organizers', 'Front\OrganizerController@index')->name('front.organizers');
Route::get('/organizers/add', 'Front\OrganizerController@add')->name('organizers.add');
Route::post('/organizers/add', 'Front\OrganizerController@add')->name('organizers.add');

Route::post('/organizers/get-liked-users', 'Front\OrganizerController@getLikeUsers')->name('organizers.add');
Route::post('/organizers/like', 'Front\OrganizerController@markAsLiked')->name('organizers.mark_as_liked');

Route::post('/organizers/user-save', 'Front\OrganizerController@userSave')->name('organizers.user_save');

Route::post('/organizers/user-subscribe', 'Front\OrganizerController@userSubscribe')->name('organizers.user_subscribe');

Route::post('/organizers', 'Front\OrganizerController@savingStateSortingInSession')->name('front.organizers.sort');

/*
 * Channels
 */
Route::get('/channels', 'Front\ChannelsController@index')->name('front.channels');
Route::post('/channels', 'Front\ChannelsController@savingStateSortingInSession')->name('front.channels.sort');
Route::post('/channels/get-liked-users', 'Front\ChannelsController@getLikeUsers')->name('channels.get_liked');
Route::post('/channels/like', 'Front\ChannelsController@markAsLiked')->name('channels.mark_as_liked');
Route::post('/channels/user-save', 'Front\ChannelsController@userSave')->name('channels.user_save');
Route::post('/channels/user-subscribe', 'Front\ChannelsController@userSubscribe')->name('organizers.user_subscribe');

/*
 * news pages
 */
Route::get('/news', 'Front\NewsController@index')->name('news');
/*
 * save click remote link on user
 */
Route::post('/follow/click', 'Front\UserFixedController@clickRemoteLinks')->name('follow.click');
Route::post('/follow/channel', 'Front\UserFixedController@followChannel')->name('follow.channel');
/*
 * Auth user social
 */
Route::get(
    '/socialite/{provider}',
    [
        'as' => 'socialite.auth',
        function ( $provider ) {
            return \Socialite::driver( $provider )->redirect();
        }
    ]
);


Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
//Route::get('/login/{provider}/callback', function ($provider) {
//    $user = \Socialite::driver($provider)->user();
//});
Route::get('auth/logout', 'Auth\AuthController@getLogout')->name('user.logout');
//ajx get usertokenize2
Route::post('/users/get', 'Auth\AuthController@getUsers')->name('users.get');

Route::post('/front_fresh_content', 'Front\SearchContentController@getFreshContent')->name('front.fresh.content');

Route::get('custom-test', 'TestController@🔧🔨⚒');
Route::get('my-test', 'TestController@test');
