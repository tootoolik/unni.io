<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditorUrlChildren extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auditor_url_children', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_domain')->unsigned();
            $table->string('url')->unique();
            $table->timestamps();
            $table->foreign('id_domain')->references('id')->on('auditor_domain');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auditor_url_children');
    }
}
