$(document).ready(function(){

    $(document).on('submit', '#addDomain', function(e){
        e.preventDefault();

        var position = $(".value-output").text();
        console.log(position);
        $.ajax({
            type: 'POST',
            url: '/test/domains_add',
            data: $('#addDomain').serialize(),
            success: function(result){
                $('div.modal-body').empty();
                $('form.modal-body').empty();
                $('.modal-body').html('<div class="alert alert-success">'+result+'</div>');
                setTimeout(function() {window.location.reload();}, 1000);

                console.log(result);
            }
        });
    });
    /**
     * Call modal window for add domain
     */
    $('.addDomain').on('click', function(e){
        e.preventDefault();
        $.ajax({
            type: 'GET',
            url: '/test/domains_add_form',
            dataType: "html",
            success: function(response){
                $( "#app" ).append(response);
                $( ".modal" ).addClass('show');
                $( ".modal" ).css('display', 'block');
                $('body').append('<div class="modal-backdrop fade show"></div>');
            }
        });
    });
    $(document).on('click', '.closeDomain', function () {
        $("div.modal").remove();
        $(".modal-backdrop").remove();
        $("body").removeClass('modal-open')
    });
    /**
     * Edit domain in modal window
     */
    $(document).on('submit', '#editDomain', function(e){
        e.preventDefault();

        var position = $(".value-output").text();
        console.log(position);
        $.ajax({
            type: 'POST',
            url: '/test/domains_edit',
            data: $('#editDomain').serialize(),
            success: function(result){
                $('div.modal-body').empty();
                $('form.modal-body').empty();
                $('.modal-body').html('<div class="alert alert-success">'+result+'</div>');
                setTimeout(function() {window.location.reload();}, 1000);

                console.log(result);
            }
        });
    });
    /**
     * Call modal window for add tag
     */
    $('.addTagButton').on('click', function(e){
        var callForm = $(this).attr('data-form');
        if(callForm == 'addTagForm') {
            var url = '/test/tags/add_tags_form';
        }
        if(callForm == 'editTagForm') {
            var url = '/test/tags/edit_tags';
        }
            $.ajax({
                url: url,
                type: 'GET',
                dataType: "html",
                success: function(response){
                    $( "#app" ).append(response);
                    $( ".modal" ).addClass('show');
                    $( ".modal" ).css('display', 'block');
                    $('body').append('<div class="modal-backdrop fade show"></div>');
                    $('.modal').attr('id', callForm)
                    $('form').attr('id', callForm+'Send')
                }
            });
    });
    /**
     * Close ajax modal
     */
    $(document).on('click', '.closeModal', function () {
        var callForm = $('.modal').attr('id');
        $("div#"+callForm).remove();
        $(".modal-backdrop").remove();
        $("body").removeClass('modal-open')
    });
    /**
     * Add tag in modal window
     */
    $(document).on('click', '.tagSend', function(e){
        var callForm = $('form').attr('id');
    $(document).on('submit', '#'+callForm, function(e){
        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: '/test/tags/add_tags',
            data: $('#'+callForm).serialize(),
            success: function(result){
                $('div.modal-body').empty();
                $('form.modal-body').empty();
                $('.modal-body').html('<div class="alert alert-success">'+result+'</div>');
                setTimeout(function() {window.location.reload();}, 1000);
            }
        });
    });
    });

    /**
     * Modal proxy
     */
        $('#buttonAddProxy').on('click', function(e){
            e.preventDefault();
            $.ajax({
                type: 'GET',
                url: '/test/proxy/addProxyForm',
                dataType: "html",
                success: function(response){
                    $( "#app" ).append(response);
                    $( ".modal" ).addClass('show');
                    $( ".modal" ).css('display', 'block');
                    $('body').append('<div class="modal-backdrop fade show"></div>');
                }
            });
        });

        $(document).on('submit', '#addProxySend', function(e){
            e.preventDefault();

            $.ajax({
                type: 'POST',
                url: '/test/proxy/addProxy',
                data: $('#addProxySend').serialize(),
                success: function(result){
                    // $('div.modal-body').empty();
                    // $('form.modal-body').empty();
                    // $('.modal-body').html('<div class="alert alert-success">'+result+'</div>');
                    // setTimeout(function() {window.location.reload();}, 1000);

                    console.log(result);
                }
            });
        });

    /**
     * Page tags, add input for added tags
     */
    $(document).on('click', '.tagAddButton', function(e) {
        e.preventDefault();
        $(".addTagBlocks").append('<div class="form-group"><input type="text" name="addTag[]" value="" class="col-lg-10 form-control" style="display: inline-block;"><span class="deleteInputTag btn btn-danger"><i class="fa fa-ban" aria-hidden="true"></i></span></div>');
    });
    $('html').on('click','.deleteInputTag', function () {
        $(this).parent().remove();
    });

    /**
     * control deamons
     */


    /**
     * Call modal window for add domain
     */
    $('.checkbox').on('click', function(e){
        // e.preventDefault();
        var idDomain = $(this).attr('id');
        // console.log(idDomain);
        var form_data = $(this).serializeArray();
        var channelOnOff = confirm("Изменить статус активности канала в зависимости от статуса домена?");
        form_data.push({name: "channelOnOff", value: channelOnOff});
        form_data.push({name: "id_domain", value: idDomain});
        console.log(JSON.stringify($.param(form_data)));
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            url: '/test/domains_enable',
            dataType: "json",
            data: $.param(form_data),
            success: function(response){

            }
        });
    });

    $('.bootstrap-tagsinput > span > span').on('click', function () {
        var tag = $(this).parent().text();
        alert('Удалить тег?');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            url: '/test/domains/tag/delete',
            dataType: "html",
            data: {'tag': tag},
            success: function(response){
                $('.bootstrap-tagsinput').after('<div class="alert alert-success">Тег удален</div>');
            }
        });
    });
    if ($('#curve_chart').length > 0){




        function drawChart() {
            var jsonData = $.ajax({
                url: "/test/ajax/stat",
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: "json",
                async: false
            }).responseText;

            // Create our data table out of JSON data loaded from server.
            var data = new google.visualization.DataTable(jsonData);

            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
            chart.draw(data, {width: 400, height: 240});
        }
    }


    // формируем новые поля
    jQuery('.plus').click(function(){
        jQuery('.information_json_plus').before(
            '<tr>' +
            '<td><input type="text" class="form-control" id="information_json_name[]" placeholder="Название поля" name="name[]"></td>' +
            '<td><input type="text" class="form-control" id="information_json_val[]" placeholder="Значение поля" name="value[]"></td>' +
            '<td><span class="btn btn-danger minus pull-right">&ndash;</span></td>' +
            '</tr>'
        );
    });
// on - так как элемент динамически создан и обычный обработчик с ним не работает
    jQuery(document).on('click', '.minus', function(){
        jQuery( this ).closest( 'tr' ).remove(); // удаление строки с полями
    });

    $('.tags-input').tokenize2({
        searchMinLength: 3,
        dataSource: function(text, object){
            $.ajax('/' + window.adminURL + '/tags/get', {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    text: text
                },
                type: 'POST',
                dataType: 'json',
                success: function(data){
                    var $items = [];
                    $.each(data, function(k, v){
                        $items.push(v);
                    });

                    object.trigger('tokenize:dropdown:fill', [$items]);
                }
            });
        }
    });

    $('.domain-input').tokenize2({
        searchMinLength: 3,
        tokensMaxItems: 1,
        dataSource: function(text, object){
            $.ajax('/' + window.adminURL + '/domains/get', {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    text: text
                },
                type: 'POST',
                dataType: 'json',
                success: function(data){
                    var $items = [];
                    $.each(data, function(k, v){
                        $items.push(v);
                    });

                    object.trigger('tokenize:dropdown:fill', [$items]);
                }
            });
        }
    });

    $('.authors-input').tokenize2({
        searchMinLength: 3,
        tokensMaxItems: 1,
        dataSource: function(text, object){
            $.ajax('/users/get', {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    text: text
                },
                type: 'POST',
                dataType: 'json',
                success: function(data){
                    var $items = [];
                    $.each(data, function(k, v){
                        $items.push(v);
                    });

                    object.trigger('tokenize:dropdown:fill', [$items]);
                }
            });
        }
    });

});