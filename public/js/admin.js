$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).on("click", ".statistic-parser-log-switch-active", function() {
    let url = '/' + window.adminURL + '/statistics/parser_logs/switch_active';
    let processId = $(this).parents('.statistics-parser-log-process-container').data('id');

    $.ajax({
        type: "POST",
        url: url,
        data: {
            processId
        },
        success: function (processList) {
            $('.statistic-parser-log-process-list').html(processList);
        }
    });
});

$(document).on("click", ".tag-show-switch", function() {
    let $this = $(this);
    let url = '/' + window.adminURL + '/tags/show_switch_active';
    let tagId = $this.parents('.tag-box').data('id');
    let checkOff = $this.hasClass('fa-rotate-180');
    let parentTd = $this.parents('.tag-show-switch-box');

    if(checkOff)
    {
        $this.removeClass('fa-rotate-180 inactive');
        parentTd.removeClass('text-danger');
        parentTd.addClass('text-success');
    }
    else
    {
        $this.addClass('fa-rotate-180 inactive');
        parentTd.removeClass('text-success');
        parentTd.addClass('text-danger');
    }

    $.ajax({
        type: "POST",
        url: url,
        data: {
            tagId
        },
        success: function (data) {
        }
    });
});

$(document).ready(function () {
    $('.delete-seo-item').click(function(e){
        e.preventDefault();
        if (confirm('Вы действительно хотите удалить ключ?')) {
            $(e.target).closest('form').submit()
        }
    });

    if(document.querySelector('#seo-title'))
    {
        CKEDITOR.replace('seo-title', {
            height: 100
        });
    }

    if(document.querySelector('#seo-description'))
    {
        CKEDITOR.replace('seo-description', {
            height: 100
        });
    }
});

$(document).on('click', '.seo-item-active', function() {
    let $this = $(this);
    let td = $this.parent();
    var seoItemId = $this.parents('.item-box').data('id');

    if(td.attr('class') === 'text-success')
    {
        td.removeClass('text-success');
        td.addClass('text-danger');
        $this.addClass('fa-rotate-180 inactive');
    }
    else
    {
        td.removeClass('text-danger');
        td.addClass('text-success');
        $this.removeClass('fa-rotate-180 inactive');
    }

    $.ajax({
        type: "POST",
        url: '/' + window.adminURL + '/seo/switch_active',
        data: { seoItemId },
        success: function (table) {
            console.log(table);
            $('#table-seo-items').html(table);
        }
    });
});

$(document).ready(function(){
    var timer;
    $('#tag-search').on('keyup', function () {
        clearTimeout(timer);
        let search = $(this).val();
        timer = setTimeout(function() {
            $.ajax({
                type: 'POST',
                url: '/' + window.adminURL + '/tags/search',
                data: {search},
                success: function (data) {
                    $('#tags-content').html(data);
                }
            });
        }, 500);

    });
});
