$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(function () {
    // $(".who").keyup(function(){
    //     if(this.value.length >= 3) {
    var options = {

        url: function (phrase) {
            return "/tips";
        },

        getValue: function (element) {
            return element.title;
        },

        ajaxSettings: {
            dataType: "json",
            method: "POST",
            data: {
                dataType: "json"
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        },

        preparePostData: function (data) {
            data.phrase = $(".who").val();
            return data;
        },

        requestDelay: 400
    };

    $(".who").easyAutocomplete(options);
    // }
    // });
});


$(document).ready(function () {
    $(document).on('click', '.tags-link', function () {
        textPopular = $(this).text();
        $('.header__search').val(textPopular);
    });


    var ul = document.getElementById('resSearch');
    var liSelected;
    var index = -1;

    document.addEventListener('keydown', function (event) {
        var len = ul.getElementsByTagName('li').length - 1;
        if(len) {
            if (event.which === 40) {
                index++;
                //down
                if (liSelected) {
                    removeClass(liSelected, 'selected');
                    next = ul.getElementsByTagName('li')[index];
                    if (typeof next !== undefined && index <= len) {

                        liSelected = next;
                    } else {
                        index = 0;
                        liSelected = ul.getElementsByTagName('li')[0];
                    }
                    addClass(liSelected, 'selected');
                }
                else {
                    index = 0;

                    liSelected = ul.getElementsByTagName('li')[0];
                    addClass(liSelected, 'selected');
                }
            }
            else if (event.which === 38) {

                //up
                if (liSelected) {
                    removeClass(liSelected, 'selected');
                    index--;
                    next = ul.getElementsByTagName('li')[index];
                    if (typeof next !== undefined && index >= 0) {
                        liSelected = next;
                    } else {
                        index = len;
                        liSelected = ul.getElementsByTagName('li')[len];
                    }
                    addClass(liSelected, 'selected');
                }
                else {
                    index = 0;
                    liSelected = ul.getElementsByTagName('li')[len];
                    addClass(liSelected, 'selected');
                }
            }
        }
    }, false);

    function removeClass(el, className) {
        if (el.classList) {
            el.classList.remove(className);
        } else {
            el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
        }
    };

    function addClass(el, className) {
        if (el.classList) {
            el.classList.add(className);
        } else {
            el.className += ' ' + className;
        }
    };

    $('.offerTags').magnificPopup({
        type: 'ajax',
        removalDelay: 300,
        mainClass: 'mfp-fade',
        callbacks: {
            parseAjax: function (mfpResponse) {
                console.log('Ajax content loaded:', mfpResponse);
            },
            ajaxContentAdded: function () {
                var id = $('.offerTags').attr('data-id');
                $('.addTagId').val(id);
            }
        }
    });

    $(function () {
        $('body').on('click', '.pagination a', function (e) {
            e.preventDefault();

            $('#load a').css('color', '#dfecf6');
            $('#load').append('<img style="position: fixed; left: 40%; top: 40%; z-index: 100000;" src="/images/loading.gif" />');

            var url = $(this).attr('href');
            getArticles(url);
            window.history.pushState("", "", url);
        });

        function getArticles(url) {
            $.ajax({
                url: url
            }).done(function (data) {
                $('.searchPageAjax').html(data);
                $('body,html').animate({scrollTop: 0}, 800);
                runApeendsCropper();
            }).fail(function () {
                alert('Tags could not be loaded.');
            });
        }
    });
    /*
    Изображения в модальном окне
     */
    $('.images_popup').magnificPopup({
        type: 'image'
    });
    /*
    Отправка формы со страницы организатора
     */
    $('.add_organizer').magnificPopup({
        type: 'ajax',
        removalDelay: 300,
        mainClass: 'mfp-fade',
        callbacks: {
            parseAjax: function (mfpResponse) {
                console.log('Ajax content loaded:', mfpResponse);
            },
            ajaxContentAdded: function () {
                var id = $('.offerTags').attr('data-id');
                $('.addTagId').val(id);
            }
        }
    });

    /* TOGGLE CLICK FOR NAVBAR*/
    $('.navbar-hamburger, .navbar-profile').on('click', function () {
        var $this = $(this);
        $this.toggleClass('active');
        if ($this.hasClass('active'))
            $this.find('.dropdown-content').show();
        else
            $this.find('.dropdown-content').hide();
    });

    $('.navbar-hamburger, .navbar-profile').hover(function () {
        $(this).find('.dropdown-content').show();
    }, function () {
        $(this).removeClass('active').find('.dropdown-content').hide();
    });

});


/* Show/hide tags */

$(document).ready(function () {
    runApeendsCropper();
});

function runApeendsCropper() {
    appendCropper('.list_result_ul');
    appendCropper('.domain-tags');
}

function appendCropper(selector) {
    var body = $(selector).children();

    body.each(function () {
        var body = $(this);
        cropperTag(body);
    });
}

function cropperTag(body) {
    let tags = body.find('.tags-link:not(#cropper-tags)');

    if (tags.length > 4) {
        tags.each(function (key) {
            let $tag = $(this);

            if (key > 3) $tag.css('display', 'none');

            if (tags.length === key + 1 && !body.find('#cropper-tags').length)
                $tag.after('<span class="tags-link" id="cropper-tags" onclick="showHideTags(this)">→</span>');
        });
    }
}

function showHideTags(ele) {
    if ($(ele).hasClass('closeMe')) {
        cropperTag($(ele).parents('li'));
        $(ele).text('→');
    } else {
        $(ele).parent().find('.tags-link').css('display', 'inline-block');
        $(ele).text('←');
    }

    $(ele).toggleClass('closeMe');
}


/* Organizer likes */
$(document).on('click', '.like-box-organizer', function () {

    let self = $(this);
    let panel = self.find('.panel-body');
    let organizerId = self.data('organizer-id');
    let likeHeart = self.find('.heart-subscribe i');
    let countBlock = panel.find('.organizer-likes-count');
    let count = parseInt(countBlock.text());

    panel.css('box-shadow', 'inset 0px 0px 6px rgba(0, 0, 0, 0.5)');

    // Check like
    if(likeHeart.hasClass('empty-heart-icon')) // was like
    {
        likeHeart.removeClass('far empty-heart-icon');
        likeHeart.addClass('fas filled-heart-icon');
        countBlock.text(++count)
    }
    else //wasn't like
    {
        likeHeart.addClass('far empty-heart-icon');
        likeHeart.removeClass('fas filled-heart-icon');
        countBlock.text(--count)
    }

    if (window.authCheck) {
        $.ajax({
            type: "POST",
            url: "/organizers/like",
            dataType: "json",
            data: {
                organizerId: organizerId
            },
            success: function (response) {
                panel.html(response);
            }
        });
    }
    else
    {
        $.ajax({
            type: "POST",
            url: "/organizers/get-liked-users",
            dataType: "json",
            data: {
                organizerId: organizerId
            },
            success: function (users) {
                $('#modal-organizer-likes').html(users)
            }
        });

        $('#modal-when-like-organizer').modal('show');
    }

    setTimeout(function () {
        panel.css('box-shadow', 'none');
    }, 100);

});

/* Channels likes */
$(document).on('click', '.like-box-channels', function () {

    let self = $(this);
    let panel = self.find('.panel-body');
    let channelId = self.data('channel-id');
    let likeHeart = self.find('.heart-subscribe i');
    let countBlock = panel.find('.channel-likes-count');
    let count = parseInt(countBlock.text());

    panel.css('box-shadow', 'inset 0px 0px 6px rgba(0, 0, 0, 0.5)');

    // Check like
    if(likeHeart.hasClass('empty-heart-icon')) // was like
    {
        likeHeart.removeClass('far empty-heart-icon');
        likeHeart.addClass('fas filled-heart-icon');
        countBlock.text(++count)
    }
    else //wasn't like
    {
        likeHeart.addClass('far empty-heart-icon');
        likeHeart.removeClass('fas filled-heart-icon');
        countBlock.text(--count)
    }

    if (window.authCheck) {
        $.ajax({
            type: "POST",
            url: "/channels/like",
            dataType: "json",
            data: {
                channelId: channelId
            },
            success: function (response) {
                panel.html(response);
            }
        });
    }
    else
    {
        $.ajax({
            type: "POST",
            url: "/channels/get-liked-users",
            dataType: "json",
            data: {
                channelId: channelId
            },
            success: function (users) {
                $('#modal-organizer-likes').html(users)
            }
        });

        $('#modal-when-like-organizer').modal('show');
    }

    setTimeout(function () {
        panel.css('box-shadow', 'none');
    }, 100);

});

/* User save organizer*/
$(document).on('click', '.organizer-save', function () {
    if (window.authCheck)
    {
        let $this = $(this);
        let organizerId = $this.data('organizer-id');

        $.ajax({
            type: "POST",
            url: "/organizers/user-save",
            dataType: "json",
            data: {
                organizerId: organizerId
            },
            success: function (response) {
                $this.parents('.organizer-user-save-content').html(response);
            }
        });
    }
    else
    {
        $('#login-with-social').modal('show');
    }
});

/* User save channel */
$(document).on('click', '.channel-save', function () {
    if (window.authCheck)
    {
        let $this = $(this);
        let channelId = $this.data('channel-id');

        $.ajax({
            type: "POST",
            url: "/channels/user-save",
            dataType: "json",
            data: {
                channelId: channelId
            },
            success: function (response) {
                $this.parents('.channel-user-save-content').html(response);
            }
        });
    }
    else
    {
        $('#login-with-social').modal('show');
    }
});

/* User subscribe on organizer*/
$(document).on('click', '.organizer-subscribe, .organizer-unsubscribe', function () {

    if (window.authCheck)
    {
        let $this = $(this);
        let organizerId = $this.data('organizer-id');
        let subscribeBtn = $('[data-organizer-id = ' + organizerId + ']');

        if($this.attr('class') === 'organizer-subscribe')
        {
            $this.text('Отменить подписку на эту страницу');
            $this.removeClass('organizer-subscribe');
            $this.addClass('organizer-unsubscribe');
            subscribeBtn.find('.organizer-subscribe-btn-text').text('Подписан');
            subscribeBtn.find('.organizer-bookmark-subscribe').addClass('organizer-bookmark-subscribe-saved');
        }
        else
        {
            $this.text('Подписаться на эту страницу');
            $this.addClass('organizer-subscribe');
            $this.removeClass('organizer-unsubscribe');
            subscribeBtn.find('.organizer-subscribe-btn-text').text('Подписаться');
            subscribeBtn.find('.organizer-bookmark-subscribe').removeClass('organizer-bookmark-subscribe-saved');
        }

        $.ajax({
            type: "POST",
            url: "/organizers/user-subscribe",
            dataType: "json",
            data: {
                organizerId: organizerId
            },
            success: function (response) {
                $this.parents('.organizer-user-subscribe-content').html(response);
            }
        });
    }
    else
    {
        $('#login-with-social').modal('show');
    }
});

/* User subscribe on channel */
$(document).on('click', '.channel-subscribe, .channel-unsubscribe', function () {

    if (window.authCheck)
    {
        let $this = $(this);
        let channelId = $this.data('channel-id');
        let subscribeBtn = $('[data-channel-id = ' + channelId + ']');

        if($this.attr('class') === 'channel-subscribe')
        {
            $this.text('Отменить подписку на эту страницу');
            $this.removeClass('channel-subscribe');
            $this.addClass('channel-unsubscribe');
            subscribeBtn.find('.channel-subscribe-btn-text').text('Подписан');
            subscribeBtn.find('.channel-bookmark-subscribe').addClass('channel-bookmark-subscribe-saved');
        }
        else
        {
            $this.text('Подписаться на эту страницу');
            $this.addClass('channel-subscribe');
            $this.removeClass('channel-unsubscribe');
            subscribeBtn.find('.channel-subscribe-btn-text').text('Подписаться');
            subscribeBtn.find('.channel-bookmark-subscribe').removeClass('channel-bookmark-subscribe-saved');
        }

        $.ajax({
            type: "POST",
            url: "/channels/user-subscribe",
            dataType: "json",
            data: {
                channelId: channelId
            },
            success: function (response) {
                $this.parents('.channel-user-subscribe-content').html(response);
            }
        });
    }
    else
    {
        $('#login-with-social').modal('show');
    }
});

/* Organizer & Channels autoloader */
$(window).scroll(function () {
    if (!$('#loader').length) return;
    let wTop = $(window).scrollTop();
    let wTopBot = wTop + $(window).height();
    let eTop = $('#loader').offset().top;
    let pageNum = $('#loader').data('page');

    if (wTopBot > eTop - 100) {
        if (window.loadTimeout){
            clearTimeout(window.loadTimeout);
        }
        let organiz = document.getElementById('list-organizers');
        if (organiz != null ){
            window.loadTimeout = setTimeout(function () {
                window.tmpTop = eTop - $('.list-group-item.list-domains-item').last().height() - 42;
                $.ajax({
                    type: "GET",
                    url: "/organizers",
                    dataType: "html",
                    data: {
                        page: pageNum
                    },
                    success: function (data) {
                        $('#list-organizers').append(data);
                        $('#loader').data('page', ++pageNum);
                        if (data){
                            $(window).scrollTop(window.tmpTop);
                        }
                    }
                });
            }, 150);
        }
        let channels = document.getElementById('list-channels');
        if (channels != null ) {
            window.loadTimeout = setTimeout(function () {
                window.tmpTop = eTop - $('.list-group-item.list-domains-item').last().height() - 42;
                $.ajax({
                    type: "GET",
                    url: "/channels",
                    dataType: "html",
                    data: {
                        page: pageNum
                    },
                    success: function (data) {
                        $('#list-channels').append(data);
                        $('#loader').data('page', ++pageNum);
                        if (data){
                            $(window).scrollTop(window.tmpTop);
                        }
                    }
                });
            }, 150);
        }
    }
});

$(document).ready(function () {

    /* organizer sort */
    $('.organizer-sort-list-item-link, .organizer-sort-other-list-item-link, .organizers-categories-title').on('click', function () {
        var $this = $(this),
            type = $this.data('type'),
            sort = $this.data('sort');

        $.ajax({
            type: "POST",
            url: "/organizers",
            dataType: "html",
            data: {
                type: type,
                sort: sort,
            },
            success: function () {
                location.reload();
            }
        });
    });

    /* channels sort */
    $('.channels-sort-list-item-link, .channels-sort-other-list-item-link, .channels-categories-title').on('click', function () {
        var $this = $(this),
            type = $this.data('type'),
            sort = $this.data('sort');

        $.ajax({
            type: "POST",
            url: "/channels",
            dataType: "html",
            data: {
                type: type,
                sort: sort,
            },
            success: function () {
                location.reload();
            }
        });
    });

    $('.nav-login, .organizer-save, .organizer-subscribe, .like-box').on('click', function () {
        $.ajax({
            type: "POST",
            url: "/navlogin/referer",
            data: {
                "referer": $(location).attr('href'),
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                // console.log(data);
            }
        });
    });

});


$(function () {
    $('.tags-input').tokenize2({
        searchMinLength: 3,
        dataSource: function(text, object){
            $.ajax('/test/tags/get', {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    text: text
                },
                type: 'POST',
                dataType: 'json',
                success: function(data){
                    var $items = [];
                    $.each(data, function(k, v){
                        $items.push(v);
                    });

                    object.trigger('tokenize:dropdown:fill', [$items]);
                }
            });
        }
    });
});