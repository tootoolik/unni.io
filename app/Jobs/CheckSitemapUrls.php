<?php

namespace App\Jobs;

use App\Facades\Analyzer;
use App\Model\Sitemap;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckSitemapUrls implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $sitemap;

    public $retryAfter = 86400; // 1 day

    public $tries = 25;

    public $timeout = 180;

    /**
     * Create a new job instance.
     *
     * @param $sitemap
     */
    public function __construct(Sitemap $sitemap)
    {
        $this->sitemap = $sitemap;
    }

    /**
     * Execute the job.
     *
     * @throws \App\Exceptions\UnknownFormat
     * @return void
     *
     */
    public function handle()
    {
        if($this->sitemap->urls->count() != Analyzer::sitemap()->countUrls($this->sitemap->url))
        {
            $this->sitemap->urls()->delete();

            dispatch(new ParseSitemap($this->sitemap));
        }
    }
}
