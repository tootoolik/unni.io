<?php

namespace App\Jobs;

use App\Facades\Analyzer;
use App\Model\Sitemap;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ParseSitemap implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $sitemap;

    public $retryAfter = 86400; // 1 day

    public $tries = 25;

    public $timeout = 300;

    /**
     * Create a new job instance.
     *
     * @param Sitemap $sitemap
     */
    public function __construct(Sitemap $sitemap)
    {
        $this->sitemap = $sitemap;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Analyzer::sitemap()->storeUrls($this->sitemap);
    }
}
