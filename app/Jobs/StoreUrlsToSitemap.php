<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class StoreUrlsToSitemap implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $urls = [];

    private $requiredFileds = [
        'loc',
        'lastmod',
        'changefreq',
        'priority'
    ];

    /**
     * Create a new job instance.
     *
     * @param $urls
     */
    public function __construct($urls)
    {
        $this->urls = $urls;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->prepareDataRelevance();

        $query = $this->prepareQuery();

        if($query) DB::insert($query);
    }

    /**
     * Implementing a query string to eliminate duplicate URLs
     *
     * @return string|null
     */
    private function prepareQuery() :?string
    {
        $values = array_map(function ($value) {
            return implode($value, '","');
        }, $this->urls);

        if($values)
        {
            $values = '("' . implode($values, '"),("') . '")';

            return DB::raw('INSERT INTO `sitemap_urls` (`loc`, `lastmod`, `changefreq`, `priority`,`parsing`,  `created_at` ,`updated_at`) VALUES ' . $values . ' ON DUPLICATE KEY UPDATE id=id');
        }

        return null;
    }

    /**
     * To check that the array is full, i.e. add the missing field.
     *
     * @return  void
     */
    private function prepareDataRelevance() :void
    {
        foreach ($this->urls as &$url)
            foreach ($this->requiredFileds as $field)
                if(!isset($url[$field]))
                    $url[$field] = null;
    }
}
