<?php

namespace App;

use App\Front\Organizer;
use App\Front\Channels;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'avatar',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
    {
        return $this->is_admin;
    }

    public function organizersSave()
    {
        return $this->belongsToMany(Organizer::class,'organizer_saves');
    }

    public function organizersSubscribe()
    {
        return $this->belongsToMany(Organizer::class,'organizer_subscriptions');
    }

    public function organizersLike()
    {
        return $this->belongsToMany(Organizer::class,'organizer_likes');
    }

    public function checkOrganizerLike($organizerId)
    {
        $checkSubscribe = User::where('id',Auth::id())->whereHas('organizersLike', function ($query) use ($organizerId){
            $query->where('organizers.id', '=', $organizerId);
        })->first();

        return $checkSubscribe ? true : false;
    }

    public function checkOrganizerSaved($organizerId)
    {
        $checkSubscribe = User::where('id',Auth::id())->whereHas('organizersSave', function ($query) use ($organizerId){
            $query->where('organizers.id', '=', $organizerId);
        })->first();

        return $checkSubscribe ? true : false;
    }

    public function checkOrganizerSubscribe($organizerId)
    {
        $checkSubscribe = User::where('id',Auth::id())->whereHas('organizersSubscribe', function ($query) use ($organizerId){
            $query->where('organizers.id', '=', $organizerId);
        })->first();

        return $checkSubscribe ? true : false;
    }

    public function channelsSave()
    {
        return $this->belongsToMany(Channels::class,'channels_saves');
    }

    public function channelsSubscribe()
    {
        return $this->belongsToMany(Channels::class,'channels_subscriptions');
    }

    public function channelsLike()
    {
        return $this->belongsToMany(Channels::class,'channels_likes');
    }

    public function checkChannelsLike($channelId)
    {
        $checkSubscribe = User::where('id',Auth::id())->whereHas('channelsLike', function ($query) use ($channelId){
            $query->where('channels.id', '=', $channelId);
        })->first();

        return $checkSubscribe ? true : false;
    }

    public function checkChannelsSaved($channelId)
    {
        $checkSubscribe = User::where('id',Auth::id())->whereHas('channelsSave', function ($query) use ($channelId){
            $query->where('channels.id', '=', $channelId);
        })->first();

        return $checkSubscribe ? true : false;
    }

    public function checkChannelsSubscribe($channelId)
    {
        $checkSubscribe = User::where('id',Auth::id())->whereHas('channelsSubscribe', function ($query) use ($channelId){
            $query->where('channels.id', '=', $channelId);
        })->first();

        return $checkSubscribe ? true : false;
    }

}
