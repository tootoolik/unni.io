<?php

namespace App\Admin;

use App\Model\Bots\Auditor\AuditorErrorUrls;
use App\Model\Bots\Auditor\CountParser;
use App\Model\Tags;
use App\Front\Channels;
use Illuminate\Database\Eloquent\Model;

class Domains extends Model
{
    protected $table = 'auditor_domain';
    protected $fillable = ['domain', 'ping', 'priority', 'parser_content', 'status_delete', 'seo_title', 'seo_description', 'description', 'logo'];
    public function tags(){
        return $this->belongsToMany(Tags::class,'tags_urls', 'url_id', 'id')->where('type', 'domain');
    }
    public function countContent(){
        return $this->hasOne(CountParser::class, 'id_domain', 'id');
    }
    public function errorsDomain(){
        return $this->hasOne(AuditorErrorUrls::class, 'id_domain', 'id');
    }

    public function manualTags()
    {
        return $this->belongsToMany(Tags::class,'manual_tags_domain');
    }

    public function manualChannel()
    {
        return $this->belongsToMany(Channels::class,'manual_domains_channels');
    }
}
