<?php

namespace App\Traits;
/**
 * Created by PhpStorm.
 * User: Smm3
 * Date: 16.05.2018
 * Time: 14:59
 */

use Sunra\PhpSimple\HtmlDomParser;

trait BustGetUrl
{
    protected $expansion = "|png|ics|jpg|doc|docx|rar|mp4|jpeg|mailto|ts|json|javascript|MOV|MTS|3gp|gif|zip|7z|tar.gz|tar.gz2|pkg|bmp|ico|svg|dmg|xml|rss|mp3|mp4|wav|arj|bz2|cab|gz|iso|lz|lzh|tar|uue|xz|z|zipx|001|h|hpp|cpp|c|xls|xlsx|avi|ppt|pptx|txt|PDF|pdf";

    /**
     * Приводим ссылку в надлежащий вид.
     *
     * @param $host
     * @param $link
     * @param null $port
     * @param $level
     *
     * @return bool|string
     */
    public function relateDomainUrl($host, $link, $port = null, $level)
    {
        // Исключаем любой файл в ссылке
        $pattern_bad = preg_match("/\.(?:z(?:ip|[0-9]{2})|r(?:ar|[0-9]{2})" . $this->expansion . ")$/i", $link);

        $link = trim($link, '/');

        // Если ссылка чистая, а чистота, как известно - залог здоровья
        if (!parse_url($link, PHP_URL_QUERY) && !parse_url($link, PHP_URL_FRAGMENT) && strlen($link) > 1 && $pattern_bad != 1 && $link != 'javascript:void(0)')
        {
            $link = $this->makeAbsoluteLink($link, $level, $port, $host);

            return $this->makeRelateDomainUrl($link, $host, $port);
        }
    }

    /**
     * При некоторых стечениях обстоятельств делаем относительную ссылку.
     *
     * @param $link
     * @param $host
     * @param $port
     *
     * @return null|string
     */
    private function makeRelateDomainUrl($link, $host, $port) :?string
    {
        if ($this->checkWww($link) || $this->checkHostOwnedLink($link, $host))
          return $this->makeProtocol($link, $host, $port);

        return null;
    }

    /**
     * Проверим есть ли WWW в сслыке
     *
     * @param $link
     *
     * @return bool
     */
    private function checkWww($link) :bool
    {
        return preg_match('~www\.~', $link);
    }

    /**
     * Проверим принадлежность ссылки домену
     *
     * @param $link
     * @param $host
     *
     * @return bool
     */
    private function checkHostOwnedLink($link, $host) :bool
    {
        $hostWithoutWww = parse_url(str_replace('www.', '', $link), PHP_URL_HOST);

        return parse_url($link, PHP_URL_HOST) && $host == $hostWithoutWww;
    }

    /**
     * Добавлем протокол к ссылке.
     *
     * @param $link
     * @param $host
     * @param $port
     *
     * @return string
     */
    private function makeProtocol($link, $host, $port) :?string
    {
        if ($port == 443 && parse_url($link, PHP_URL_SCHEME) == 'http')
            $linkWithProtocol =  str_replace('http', 'https', $link);

        elseif ($port == 80 && parse_url($link, PHP_URL_SCHEME))
            $linkWithProtocol =  str_replace(parse_url($link, PHP_URL_HOST), $host, $link);

        elseif ($port == 443 && parse_url($link, PHP_URL_SCHEME))
            $linkWithProtocol = str_replace(parse_url($link, PHP_URL_HOST), $host, $link);

        else
            $linkWithProtocol = null;

        return $linkWithProtocol;
    }

    /**
     * Приводим ссылку к абсолютной
     *
     * P.S. "Только ситхи всё возводят в абсолют" (с) Оби-Ван Кеноби
     *
     * @param $link
     * @param $level
     * @param $port
     * @param $host
     *
     * @return null|string
     */
    private function makeAbsoluteLink($link, $level, $port, $host) :?string
    {
        if (!parse_url($link, PHP_URL_HOST))
        {
            if (!parse_url($link, PHP_URL_QUERY) && !parse_url($link, PHP_URL_FRAGMENT) && strlen($link) > 1 && parse_url($link, PHP_URL_PATH))
            {
                if (count(explode('/', $link)) <= $level)
                {
                    if ($port == 80)
                        $link = 'http://' . $host . '/' . $link;

                    elseif ($port == 443)
                        $link = 'https://' . $host . '/' . $link;

                    else
                        $link = 'http://' . $host . '/' . $link;


                    return $link;
                }
            }
        }
        else
        {
            return $link;
        }

        return null;
    }

    protected function remote_domain($domain, $link)
    {
        if ($domain !== parse_url(str_replace('www.', '', $link), PHP_URL_HOST)) {
            return parse_url(str_replace('www.', '', $link), PHP_URL_HOST);
        }
    }

    function exec_script($url, $params = array())
    {
        $parts = parse_url($url);

        if (!$fp = fsockopen($parts['host'], isset($parts['port']) ? $parts['port'] : 80)) {
            return false;
        }

        $data = http_build_query($params, '', '&');

        fwrite($fp, "POST " . (!empty($parts['path']) ? $parts['path'] : '/') . " HTTP/1.1\r\n");
        fwrite($fp, "Host: " . $parts['host'] . "\r\n");
        fwrite($fp, "Content-Type: application/x-www-form-urlencoded\r\n");
        fwrite($fp, "Content-Length: " . strlen($data) . "\r\n");
        fwrite($fp, "Connection: Close\r\n\r\n");
        fwrite($fp, $data);
        fclose($fp);

        return true;
    }

    public function contentGet($url)
    {
//        dump($url);
        // identify regex
//        $proxy = '190.60.4.210:3128';
        $loginpassw = 'JkaCDVWp:itNTmQuF'; //your proxy login and password here
        $proxy_ip = '138.197.157.68'; //proxy IP here
        $proxy_port = 8080; //proxy port that you use with the proxies
        $user_agent = 'Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';

        $options = array(
//            CURLOPT_PROXYPORT => $proxy_port,
//            CURLOPT_PROXYTYPE => 'HTTP',
//            CURLOPT_PROXY => $proxy_ip,
//            CURLOPT_PROXYUSERPWD => $loginpassw,
            CURLOPT_CUSTOMREQUEST => "GET",        //set request type post or get
            CURLOPT_POST => false,        //set to GET
            CURLOPT_USERAGENT => $user_agent, //set user agent
            CURLOPT_COOKIEFILE => "cookie.txt", //set cookie file
            CURLOPT_COOKIEJAR => "cookie.txt", //set cookie jar
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING => "",       // handle all encodings
            CURLOPT_AUTOREFERER => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT => 120,      // timeout on response
            CURLOPT_MAXREDIRS => 10,       // stop after 10 redirects
            CURLINFO_CONTENT_TYPE => '',
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        $err = curl_errno($ch);
        $errmsg = curl_error($ch);
        $header = curl_getinfo($ch);
        curl_close($ch);

        $header['errno'] = $err;
        $header['errmsg'] = $errmsg;
        $header['content'] = $content;
//        dump($header);
        if (strpos($header['content_type'], 'text/html') !== false && $header['http_code'] != '403') {
            return $header['content'];
        } elseif ($header['http_code'] == '403') {
            return $header['http_code'];
        } else {
            $header['content'] = false;
            return $header['content'];
        }

    }

    public function get_http_response_code($url)
    {
        if (preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url)) {
            $headers = get_headers($url);
            return substr($headers[0], 9, 3);
        } else {
            return;
        }

    }

    public function ping($url)
    {
        $ch = curl_init();    // инициализация
        curl_setopt($ch, CURLOPT_URL, $url); // устанавливаем URL
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);// разрешаем редирект
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // указывает, что функция curl_exec должна вернуть полученный ответ, а не отправить его сразу браузеру
        $result = curl_exec($ch); // запуск
        curl_close($ch);
        echo $result;
    }

    public function linkExpansion($url)
    {
        foreach ($this->expansion as $item) {
            $result = strpos($url, $item);
            if ($result === false) {
                $url = true;
            } else {
                $url = false;
            }
            return $url;
        }

//        $pos = stristr($url, '.png');
//        $ics = stristr($url, '.ics');
//        $jpg = stristr($url, '.jpg');
//        $doc = stristr($url, '.doc');
//        $rar = stristr($url, '.rar');
//        $mp4 = stristr($url, '.mp4');
//        $jpeg = stristr($url, '.jpeg');
//        $mailto = stristr($url, 'mailto');
//        $ts = stristr($url, '.ts');
//        $json = stristr($url, '.json');
//        $javascript = stristr($url, 'javascript');
//        if ($jpg === false && $pos === false && $ics === false && $doc === false && $rar === false && $mp4 === false && $jpeg === false && $mailto === false && $javascript === false && $ts === false && $json === false){
//            $url = true;
//        }else{
//            $url = false;
//        }

    }

    public function objUrlGet($url)
    {
        // identify regex
        $proxy = '190.60.4.210:3128';
        $regex = '/(<a\s*'; // Start of anchor tag
        $regex .= '(.*?)\s*'; // Any attributes or spaces that may or may not exist
        $regex .= 'href=[\'"]+?\s*(?P<link>\S+)\s*[\'"]+?'; // Grab the link
        $regex .= '\s*(.*?)\s*>\s*'; // Any attributes or spaces that may or may not exist before closing tag
        $regex .= '(?P<name>\S+)'; // Grab the name
        $regex .= '\s*<\/a>)/i'; // Any number of spaces between the closing anchor tag (case insensitive)
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_PROXY => $proxy,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_URL => $url));

        //
//        $fOut = fopen($_SERVER["DOCUMENT_ROOT"].'/'.'curl_out.txt', "w" );
//        curl_setopt ($ch, CURLOPT_VERBOSE, 1);
//        curl_setopt ($ch, CURLOPT_STDERR, $fOut );
        $PaserUrl = curl_exec($ch);
        preg_match_all($regex, $PaserUrl, $url);
//        $html = new HtmlDomParser();
        $objUrlGet = HtmlDomParser::str_get_html($PaserUrl);
        return $objUrlGet;
//        $html->clear();
//        unset($html);
    }

    public function clearOperait()
    {
        $html = new HtmlDomParser();
        $html->dom = null;
        $html->nodes = null;
        $html->parent = null;
        $html->children = null;
        unset($html);
    }
}