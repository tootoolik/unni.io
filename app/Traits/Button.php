<?php
/**
 * Created by PhpStorm.
 * User: Smm3
 * Date: 13.08.2018
 * Time: 10:34
 */

namespace App\Traits;

trait Button
{
    public $page;
    public $text;
    public $isActive;

    public function __construct($page, $isActive = true, $text = null)
    {
        $this->page = $page;
        $this->text = is_null($text) ? $page : $text;
        $this->isActive = $isActive;
    }

    public function activate()
    {
        $this->isActive = true;
    }

    public function deactivate()
    {
        $this->isActive = false;
    }
}