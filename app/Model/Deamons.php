<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Deamons extends Model
{
    protected $table = 'deamons_list';
    protected $fillable = ['id_pid', 'active', 'work', 'title', 'command'];
}
