<?php

namespace App\Model;

use App\Model\Bots\Auditor\AuditorDomains;
use Illuminate\Database\Eloquent\Model;

class Sitemap extends Model
{
    protected $table = 'sitemaps';

    protected $fillable = ['domain_id', 'url'];

    public function domain()
    {
        return $this->belongsTo(AuditorDomains::class);
    }

    public function urls()
    {
        return $this->hasMany(SitemapUrl::class,'sitemap_id','id');
    }
}
