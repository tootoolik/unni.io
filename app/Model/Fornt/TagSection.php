<?php

namespace App\Model\Fornt;

use Illuminate\Database\Eloquent\Model;

class TagSection extends Model
{
    protected $table = 'tags_section';

    protected $fillable = ['name','url_id','domain_id'];

    public $timestamps = false;
}
