<?php

namespace App\Model\Fornt;

use Illuminate\Database\Eloquent\Model;

class UserProvider extends Model
{
    protected $table = 'user_providers';
    protected $fillable = ['provider', 'provider_id', 'user_id'];
    public $timestamps = false;
}
