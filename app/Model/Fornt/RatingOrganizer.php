<?php

namespace App\Model\Fornt;

use Illuminate\Database\Eloquent\Model;

class RatingOrganizer extends Model
{
    protected $table = 'rating_organizer';

    protected $fillable = ['organizer_id', 'user_id', 'votes'];
}
