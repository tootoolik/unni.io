<?php

namespace App\Model\Fornt;

use App\Model\Fornt\Tag;
use App\Model\Bots\Auditor\AuditorUrls;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Feed extends Model
{
//    protected $table = 'jeweler_content';
    protected $table = 'cache_feed';
    protected $fillable = ['id_url', 'title', 'description', 'created_at', 'updated_at', 'image', 'created_page'];
    public function linksContent()
    {
        return $this->hasOne(AuditorUrls::class, 'id', 'id_url');
    }
    public function linksNew()
    {
        return $this->hasOne(AuditorUrls::class, 'id', 'id_url')->where('status_news', 1);
    }
    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'tags_urls','url_id', 'tag_id','id_url')->select('tags.tag', 'tags.slug', 'tags.id');
    }


    public function tagsSection($url_id)
    {
        $tags = DB::table('tags_urls')
            ->join('tags', 'tags.id', '=', 'tags_urls.tag_id')
            ->distinct()
            ->where('url_id', $url_id)
            ->select('tags.tag');

        return DB::table('tags_section')
            ->distinct()
            ->where('url_id', $url_id)
            ->unionAll($tags)
            ->select('name')
            ->groupBy('name')
            ->get();
//        return $this->belongsToMany(Tag::class, 'tags_urls', 'url_id')->limit(10);
    }
}
