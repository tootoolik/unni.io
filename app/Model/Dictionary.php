<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Dictionary extends Model
{
    protected $table = 'dictionarys';
    protected $fillable = ['title'];
}
