<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SearchHistory extends Model
{
    protected $table = 'search_history';
    protected $fillable = ['title', 'typeSearch'];
}
