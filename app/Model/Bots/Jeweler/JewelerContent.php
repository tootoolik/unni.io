<?php

namespace App\Model\Bots\Jeweler;

use App\Model\Bots\Auditor\AuditorUrls;
use App\Model\Tags;
use Illuminate\Database\Eloquent\Model;

class JewelerContent extends Model
{
    protected $table = 'jeweler_content';

    protected $fillable = ['id_url', 'title', 'description', 'created_at', 'updated_at', 'image', 'created_page'];

    public function linksContent()
    {
        return $this->hasOne(AuditorUrls::class, 'id', 'id_url');
    }

    public function tags()
    {
        return $this->belongsToMany(Tags::class, 'tags_urls', 'url_id','tag_id','id_url','id');

    }
}
