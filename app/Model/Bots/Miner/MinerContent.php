<?php

namespace App\Model\Bots\Miner;

use App\Model\Bots\Auditor\AuditorDomains;
use App\Model\Bots\Auditor\AuditorUrls;
use Illuminate\Database\Eloquent\Model;

class MinerContent extends Model
{
    protected $table = 'miner_content';

    protected $fillable = ['id_url','title','description','cron_start'];

    public function countContentsAdmin()
    {
        return $this->count();
    }
}
