<?php

namespace App\Model\Bots\Crons;

use Illuminate\Database\Eloquent\Model;

class UrlsCron extends Model
{
    protected $table = 'auditor_cron_urls';
}
