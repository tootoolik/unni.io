<?php

namespace App\Model\Bots\Auditor;

use App\Model\Bots\Jeweler\JewelerContent;
use App\Model\Bots\Miner\MinerContent;
use App\Model\Tags;
use App\Model\TagsUrls;
use Illuminate\Database\Eloquent\Model;

class AuditorUrls extends Model
{
    protected $table = 'auditor_url_children';

    protected $fillable = ['id', 'url', 'id_domain', 'http_code', 'parser', 'home', 'speed_download', 'callback_server_time', 'size_download', 'status_news'];
    
    public function auditorContents()
    {
        return $this->hasMany(MinerContent::class, 'id_url', 'id');
    }
    public function url_text()
    {
        return $this->hasOne(JewelerContent::class, 'id_url', 'id')->select('id_url', 'description', 'title');
    }
}
