<?php

namespace App\Model;

use App\Admin\Domains;
use App\Model\Bots\Auditor\AuditorUrls;
use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    protected $table = 'tags';

    protected $fillable = ['tag', 'type', 'url_id','slug'];
    public function url(){
        return $this->belongsToMany(AuditorUrls::class, 'tags_urls', 'tag_id', 'url_id')->groupBy('url_id')->limit(10);
    }

    public function manualDomains()
    {
        return $this->belongsToMany(Domains::class,'manual_tags_domain');
    }

    public function domains()
    {
        return $this->hasMany(TagsDomain::class,'tag_id','id');
    }

    public function tagsUrls()
    {
        return $this->hasMany(TagsUrls::class,'tag_id','id');
    }
}
