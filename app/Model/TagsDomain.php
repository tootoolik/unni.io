<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TagsDomain extends Model
{
    protected $table = 'tags_domain';

    protected $fillable = ['domain_id', 'tag_id', 'count'];

    public $timestamps = false;
}
