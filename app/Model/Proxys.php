<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Proxys extends Model
{
    protected $fillable = ['address'];
}
