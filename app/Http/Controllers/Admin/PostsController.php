<?php

namespace App\Http\Controllers\Admin;

use App\Front\Posts;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Admin.Posts.index', ['posts' => Posts::paginate(20)]);
    }

    public function moderationPosts()
    {
        return view('Admin.Posts.index', ['posts' => Posts::where('active', '0')->paginate(20)]);
    }

    public function approvedPosts()
    {
        return view('Admin.Posts.index', ['posts' => Posts::where('active', '1')->paginate(20)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = 0;
        if (Auth::check()) {
            $user = Auth::user()->id;
        }
        return view('Admin.Posts.create', ['user' => $user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $params = [];
        $params['name'] = $request->name;
        $params['body'] = $request->post_content;
        $params['active'] = $request->public;
        if ($request->hasFile('post_img'))
        {
            $params['post_img'] = $this->saveImage($request->file('post_img'));
        }
        $post = Posts::create($params);

        $this->storeManualTags($post, $request);
        $this->storeManualAuthor($post, $request);

        return back()->with('status', 'Публикация успешно создана');
    }

    /**
     * Display the specified resource.
     *
     * @param Posts $posts
     * @return \Illuminate\Http\Response
     */
    public function show(Posts $posts)
    {
        //return view('Admin.Posts.show', ['posts' => $post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @return void
     */
    public function edit($id)
    {
        $post = Posts::find($id);
        $tags = $post->manualTags;
        $authors = $post->postAuthor;
        return view('Admin.Posts.edit', ['post' => $post, 'tags' => $tags, 'authors' => $authors]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updatePost = Posts::find($id);
        $params = [];
        $params['name'] = $request->name;
        $params['body'] = $request->post_content;
        $params['active'] = $request->public;
        if ($request->hasFile('post_img'))
        {
            $this->deleteImage($updatePost);
            $params['post_img'] = $this->saveImage($request->file('post_img'));
        }
        $updatePost->update($params);

        $this->storeManualTags($updatePost, $request);
        $this->storeManualAuthor($updatePost, $request);

        return back()->with('status', 'Публикация успешно обновленна');
    }

    public function storeManualTags($post, $request)
    {
        $post->manualTags()->detach();

        if ($request->has('tags'))
        {
            foreach ($request->tags as $tagId)
            {
                $post->manualTags()->attach([
                    'tags_id' => $tagId
                ]);
            }
        }
    }

    public function storeManualAuthor($post, $request)
    {
        $post->postAuthor()->detach();

        if ($request->has('post_author'))
        {
            foreach ($request->post_author as $authorId)
            {
                $post->postAuthor()->attach([
                    'user_id' => $authorId
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $post_destroy = Posts::find($request->id);
        $post_destroy->manualTags()->detach();
        $post_destroy->postAuthor()->detach();
        $post_destroy->delete();
        return back();
    }

    private function saveImage($file) :string
    {
        $path = $file->store('posts', ['disk' => 'public']);
        $image = Image::make(public_path('storage/' . $path));
        $image->save();
        return $path;
    }

    private function deleteImage($post)
    {
        Storage::disk('public')->delete($post->post_img);
    }

}
