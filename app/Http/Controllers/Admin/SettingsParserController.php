<?php

namespace App\Http\Controllers\Admin;

use App\Model\Bots\Auditor\AuditorDomains;
use App\Model\Bots\Auditor\AuditorUrlAnhor;
use App\Model\Bots\Crons\DomainsCron;
use App\Model\Bots\Crons\UrlsCron;
use App\Model\Elastic;
use App\Model\Setting;
use App\Model\Tags;
use App\Notifications\StopParser;
use App\User;
use Elasticsearch\ClientBuilder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;

class SettingsParserController extends Controller
{
    public function index()
    {
        $elastics = DB::table('elastic')->get();
        $mainParser = DB::table('main_parser')->orderBy('id', 'desc')->first();
//        exec ('sh /home/unni/public_html/test.sh');
//        dump($outputsass);
        return view('Admin.Settings.Parser.index', compact('elastics'), ['mainParser' => $mainParser]);
    }

    public function stopstart(Request $request)
    {
        if ($request->stop) {
            DB::table('main_parser')->insert(['level' => 1]);
            $status = 'Парсер завершаеться, после завершения выведится уведомление';
        } elseif ($request->start) {
            DB::table('main_parser')->insert(['level' => 0]);
            $status = 'Парсер запущен';
        } else {
            $status = 'Непредвиденная ошибка';
        }
        return back()->with('status', $status);
    }

    public function ajax_status_parser()
    {
        $files = Storage::allFiles('domains');
        $mainParser = DB::table('main_parser')->orderBy('id', 'desc')->first();
        if (!$files && $mainParser->level == 1) {
            return 0;
        }
        if (!$files && $mainParser->level == 0) {
            return 2;
        }
        return 1;
    }

    public function create()
    {
        return view('Admin.Settings.Parser.create');
    }

    public function store(Request $request)
    {
        $client = ClientBuilder::create()->build();
        $validation = $this->validate($request, [
            'index_elastic' => 'required|unique:elastic|max:255'
        ]);
        $elastic = new Elastic();
        $elastic->index_elastic = $request->index_elastic;
        $elastic->save();
        $params = [
            'index' => $request->index_elastic
        ];
        $client->indices()->create($params);
        return back()->with_errors($validation);
    }

    public function delete(Request $request)
    {
        $client = ClientBuilder::create()->build();
        $dictionary = [
            'AuditorUrls' => 'App\Model\Bots\Auditor\AuditorUrls',
            'AuditorContent' => 'App\Model\Bots\Auditor\AuditorContent',
            'CountParser' => 'App\Model\Bots\Auditor\CountParser',
            'AuditorErrorUrls' => 'App\Model\Bots\Auditor\AuditorErrorUrls',
            'JewelerContent' => 'App\Model\Bots\Jeweler\JewelerContent',
//            'Tags' => 'App\Model\Tags',
        ];

        if ($request->all()['models']) {
            foreach ($request->all()['models'] as $nameModel => $value) {
                $className = $dictionary[$value];
                $models = null;
                if (class_exists($className)) {
                    $className::truncate();
                }
            }
            $models = implode(',', $request->all()['models']);
            AuditorDomains::where('count', '>', 0)->update(['count' => 0]);
        }

        AuditorUrlAnhor::truncate();
//        Tags::where('type', 'parser')->delete();
        DB::table('tags_domain')->truncate();
        DB::table('tags_count')->truncate();
        DB::table('tags_section')->truncate();
        DB::table('tags_urls')->truncate();
        if (!empty($request->all()['elasticIndexs'])) {
            foreach ($request->all()['elasticIndexs'] as $elasticIndex) {
                $deleteParams = [
                    'index' => $elasticIndex
                ];
                $client = ClientBuilder::create()->build();
                $client->indices()->delete($deleteParams);
            }
            DB::table('elastic')->where('index_elastic', $elasticIndex)->delete();
        }
        $listDomains = DB::connection('mysqlTmpUrl')->select('SHOW TABLES');
        if ($listDomains) {
            foreach ($listDomains as $listDomain) {
                Schema::connection('mysqlTmpUrl')->dropIfExists($listDomain->Tables_in_unni_tmp_url);
            }
        }
        return back()->with('status', 'Очищены таблицы ' . $models . '. Удалены временные таблицы ссылками');
    }

    public function setting(Request $request)
    {
        $request->validate([
            'count_links' => 'bail|required|max:255',
            'time_timeout' => 'required',
        ]);
        $request->session()->flash('status', 'Настройки сохранены!');
        return back();
    }

    public function tags()
    {
        $params = Setting::where('type', 'tags')->get();
        return view('Admin.Settings.tags', ['params' => $params]);
    }

    public function tagsPost(Request $request)
    {
        $params = array_combine($request->name, $request->value);
        foreach ($params as $param => $key) {
            Setting::updateOrCreate(
                ['type' => 'tags', 'name' => $param, 'value' => $key]
            );
        }
        return back();
    }
}
