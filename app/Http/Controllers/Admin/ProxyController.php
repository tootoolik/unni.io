<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Proxys;

class ProxyController extends Controller
{
   public function index(){
       $allProxys = Proxys::get();
       return view('Admin.Proxys.index', ['allProxys' => $allProxys]);
   }
   public function forms(){
       return view('Admin.Proxys.Forms.addListProxy');
   }
   public function formsSend(Request $request){
       $request->listProxy = isset($request->listProxy)?$request->listProxy:"";
       if (strlen($request->listProxy)==0) {exit;}
       $request->listProxy = explode("\n", str_replace("\r", "", $request->listProxy));

       foreach ($request->listProxy as $listProxy){
           Proxys::create(['address' => $listProxy]);
       }

       unset($request);
//       return redirect('/test/proxy')->with('status','Proxy добавлены');
   }
}
