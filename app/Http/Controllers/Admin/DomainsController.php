<?php

namespace App\Http\Controllers\Admin;

use App\Admin\Domains;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreDomainRequest;
use App\Http\Requests\Admin\UpdateDomainRequest;
use App\Model\Bots\Auditor\AuditorContent;
use App\Model\Bots\Auditor\AuditorErrorUrls;
use App\Model\Bots\Auditor\AuditorUrls;
use App\Model\Bots\Auditor\CountParser;
use App\Model\Bots\Jeweler\JewelerContent;
use App\Model\Tags;
use App\Front\Channels;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class DomainsController extends Controller
{
    public function index()
    {
//        $categories = Domains::with('countContent')->where('type', $type)->orderBy('parser_content', 'desc')->paginate(15);
//        $countIndexisUrls = CountParser::sum('count_link');
//        $countIndexis = CountParser::sum('count_content');
//        return view('Admin.Domains.index', ['categories' => $categories, 'countIndexis' => $countIndexis, 'countIndexisUrls' => $countIndexisUrls]);
    }

    public function type($type)
    {
        $categories = Domains::with('countContent','manualChannel')->where('type', $type)->orderBy('created_at', 'desc')->paginate(15);
        $countIndexisUrls = CountParser::sum('count_link');
        $countIndexis = CountParser::sum('count_content');
        return view('Admin.Domains.index', ['categories' => $categories, 'countIndexis' => $countIndexis, 'countIndexisUrls' => $countIndexisUrls]);
    }

    public function create()
    {
        return view('Admin.Domains.create');
    }

    public function store(Domains $domain, StoreDomainRequest $request)
    {
        $domain->domain = mb_strtolower($request->domain);
        $domain->priority = $request->priority;
        $domain->parser_content = $this->getParseContentValue($request);
        $domain->status_delete = $this->getStatusDeleteValue($request);
        $domain->type = 'admin';
        $domain->level = 4;
        $domain->save();

        $this->storeManualTags($domain, $request);

        return redirect()->route('admin.domains.type', ['type' => 'admin'])->with('status', 'Домен добавлен');
    }

    public function storeManualTags($domain, $request)
    {
        $domain->manualTags()->detach();

        if ($request->has('tags'))
        {
            foreach ($request->tags as $tagId)
            {
                $domain->manualTags()->attach([
                    'tags_id' => $tagId
                ]);
            }
        }
    }

    private function getParseContentValue($request)
    {
        return $request->has('enableDomain') ? 1 : 0;
    }

    private function getStatusDeleteValue($request)
    {
        return $request->has('status_delete') ? 1 : 0;
    }

    public function edit($id)
    {
        $domain = Domains::find($id);
        $tags = $domain->manualTags;

        return view('Admin.Domains.edit', compact('domain','tags'));
    }

    public function update(UpdateDomainRequest $request, $id)
    {
        $domain = Domains::find($id);

        $domain->domain = mb_strtolower($request->name);
        $domain->priority = $request->priority;
        $domain->parser_content = $this->getParseContentValue($request);
        $domain->status_delete = $this->getStatusDeleteValue($request);
        $domain->seo_title = $request->seo_title;
        $domain->seo_description = $request->seo_description;
        $domain->description = $request->description;
        $domain->level = $request->level;

        if ($request->hasFile('logo'))
        {
            $this->deleteImage($domain);
            $domain->logo = $this->saveImage($request->file('logo'));
        }

        $domain->save();

        $this->storeManualTags($domain, $request);

        return redirect()->back()->with('status', 'Данные сохранены');
    }

    private function saveImage($file) :string
    {
        $path = $file->store('domain_logo', ['disk' => 'public']);

        $image = Image::make(public_path('storage/' . $path));

        $image->resize(400, 400, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        $image->save();

        return $path;
    }

    private function deleteImage($domain)
    {
        Storage::disk('public')->delete($domain->logo);
    }

    public function show($id)
    {
        $domain = Domains::find($id);
        $errorsDomains = AuditorErrorUrls::where('id_domain', $id)->paginate(15);
        $errorsCodies = AuditorErrorUrls::where('id_domain', $id)->groupBy('error_code')->get();
        return view('Admin.Domains.store', compact('domain', 'errorsDomains', 'errorsCodies'));
    }

    public function destroy($id)
    {
        $deleteDomain = Domains::find($id);
        $deleteDomain->manualTags()->detach();
        $deleteDomain->delete();

        return redirect()->route('admin.domains.type', ['type' => 'admin'])->with('status', 'Домен удален');
    }

    public function tagDelete(Request $request){
        Tags::where('tag', $request->tag)->delete();
        return response()->json(array('status' => true));
    }

    public function onOffDomains(Request $request){
        $domain = Domains::find($request->id_domain);
        if ($request->has('enableDomain') && $request->enableDomain == 'on') {
            $domain->update(['parser_content' => '1']);
            if($request->has('channelOnOff') && $request->channelOnOff === 'true'){
                if( $domain->manualChannel && count($domain->manualChannel) > 0 ){
                    foreach ($domain->manualChannel as $item){
                        $channel = Channels::find($item['id']);
                        if(!empty($channel)){
                            $channel->update(['active' => '1']);
                        }
                    }
                }
                else
                {
                    $new_сhannel = Channels::create([
                        'name' => $domain->domain,
                        'website' => $domain->domain,
                        'fb_group_id' => 0,
                        'body' => NULL,
                        'active' => '1',
                    ]);
                    $this->generateStoreManualDomains($domain, $new_сhannel);
                }
            }
            return 'domain enable';
        }elseif($request->enableDomain == null){
            $domain->update(['parser_content' => '0']);
            if($request->has('channelOnOff') && $request->channelOnOff === 'true'){
                if( $domain->manualChannel && count($domain->manualChannel) > 0 ){
                    foreach ($domain->manualChannel as $item) {
                        $channel = Channels::find($item['id']);
                        if (!empty($channel)) {
                            $channel->update(['active' => '0']);
                        }
                    }
                }
                else
                {
                    $new_сhannel = Channels::create([
                        'name' => $domain->domain,
                        'website' => $domain->domain,
                        'fb_group_id' => 0,
                        'body' => NULL,
                        'active' => '0',
                    ]);
                    $this->generateStoreManualDomains($domain, $new_сhannel);
                }
            }
            return 'domain disable';
        }
    }

    public function generateStoreManualDomains($domain, $сhannel)
    {
        $domain->manualChannel()->detach();

        if ($сhannel->id)
        {
            $domain->manualChannel()->attach([
                'channels_id' => $сhannel->id
            ]);
        }
    }

    public function error_parser(Request $request)
    {
        if ($request->errors){
            $idUrls = AuditorErrorUrls::whereIn('error_code', $request->errors)->where('id_domain', $request->domain)->limit(500)->get();
            foreach ($idUrls as $idUrl) {
                $auditorUrls = AuditorUrls::find($idUrl->url_id);
                $auditorUrls->parser = '0';
                $auditorUrls->save();

                AuditorContent::where('url_id', $idUrl->url_id)->delete();
                JewelerContent::where('id_url', $idUrl->url_id)->delete();
                $url_ids = DB::table('tags_urls')->where('url_id', $idUrl->url_id)->get();
                foreach ($url_ids as $url_id) {
                    DB::table('tags_urls')->where('url_id', $url_id->url_id)->delete();
                }
                AuditorErrorUrls::where('url_id', $idUrl->url_id)->delete();
            }
            return back()->with('status','Ссылки успешно отправлены на перепарсинг');
        }elseif($request->search_text){
            $searchTexts = JewelerContent::where('description', 'LIKE', '%'.$request->search_text.'%')->limit(500)->get();
            foreach ($searchTexts as $searchText) {
                $auditorUrls = AuditorUrls::find($searchText->id_url);
                $auditorUrls->parser = 0;
                $auditorUrls->save();
                JewelerContent::where('id_url', $searchText->id_url)->delete();
                Tags::where('url_id', $searchText->id_url)->delete();
                AuditorContent::where('url_id', $searchText->id_url)->delete();
            }
            return back()->with('status','Ссылки успешно отправлены на перепарсинг');
        }elseif($request->all_content){
            $searchTexts = AuditorUrls::where('id_domain', $request->domain)->where('parser', '1')->limit(1000)->get();
            foreach ($searchTexts as $searchText) {
                $auditorUrls = AuditorUrls::find($searchText->id);
                $auditorUrls->parser = 0;
                $auditorUrls->save();
                JewelerContent::where('id_url', $searchText->id)->delete();
                Tags::where('url_id', $searchText->id)->delete();
                AuditorContent::where('url_id', $searchText->id)->delete();
            }
            return back()->with('status','Контент удален');
        }else{
            return back()->with('status','Нет данных на перепарсинг');
        }
//        dump(implode(',', $request->errors));
    }

    public function getDomains(Request $request){
        if($request->ajax())
        {
            $domainsCollection = Domains::where('domain', 'LIKE', '%'.$request->text.'%')
                ->limit(10)
                ->get();

            $domains = $this->transformDataForTokenize2Domain($domainsCollection);

            return response()->json($domains);
        }
    }

    private function transformDataForTokenize2Domain($domains)
    {
        $result = [];

        foreach ($domains as $domain)
        {
            $result[] = [
                'text' => $domain->domain,
                'value' => $domain->id
            ];
        }

        return $result;
    }

}
