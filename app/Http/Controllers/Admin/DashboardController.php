<?php

namespace App\Http\Controllers\Admin;

use App\Model\Bots\Auditor\AuditorUrls;
use App\Model\Bots\Jeweler\JewelerContent;
use App\Model\Bots\Miner\MinerContent;
use App\Model\Tags;
use Carbon\Carbon;
use DateTime;
use DateTimeZone;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
//        $freshContents =  MinerContent::orderBy('id', 'desc')->limit(10)->get();
//        $countStatContent = MinerContent::get();
//        $countStatContent = $countStatContent->groupBy('cron_start');
//        dump($countStatContent->toArray());
////        $countStatContent = $countStatContent->countContentsAdmin();
///
        $countStatContent = DB::table('jeweler_content')
            ->select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as views'))
            ->groupBy('date')
            ->where('created_at', '>', Carbon::now()->subDays(90))
            ->get();
        $countDateTagsUrls = Tags::select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as views'))
            ->groupBy('date')
            ->where('created_at', '>', Carbon::now()->subDays(90))
            ->get();
        $freshContents =  JewelerContent::orderBy('id', 'desc')->limit(5)->get();
        return view('Admin.Dashboard.index', ['countStatContent' => $countStatContent, 'countDateTagsUrls' => $countDateTagsUrls, 'freshContents' => $freshContents]);
    }
    public function statistics()
    {
        $countDateTagsUrls = Tags::select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as views'))
            ->groupBy('date')
            ->get();
        $result[] = ['Year','Viewer'];
        foreach ($countDateTagsUrls as $key => $value) {
            $result[++$key] = [$value->date, (int)$value->views];
        }
        return json_encode($result);
    }
    public function freshContent(){
//        return DB::table('miner_content')
//            ->leftJoin('auditor_url_children', 'miner_content.id_url', '=', 'auditor_url_children.id')
//            ->leftJoin('auditor_domain', 'auditor_domain.id', '=', 'auditor_url_children.id_domain')
//            ->select('miner_content.title', 'miner_content.description', 'auditor_domain.domain', 'miner_content.cron_start')
//            ->limit(5)
//            ->orderBy('miner_content.id', 'desc')
//            ->get();
//
        $freshContents =  JewelerContent::orderBy('id', 'desc')->limit(5)->get();
        return view('Admin.Dashboard.freshContent', ['freshContents' => $freshContents]);
    }
    public function testing()
    {

    }
}
