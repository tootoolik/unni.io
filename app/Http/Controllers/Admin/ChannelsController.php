<?php

namespace App\Http\Controllers\Admin;

use App\Front\Channels;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin\Domains;

class ChannelsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Admin.Channels.index', ['channels' => Channels::paginate(20)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = $request;
        return view('Admin.Channels.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $сhannel = Channels::create([
            'name' => $request->name,
            'website' => $request->website,
            'fb_group_id' => $request->fb_group_id,
            'body' => $request->chanel_content,
            'active' => $request->public,
        ]);

        $this->storeManualTags($сhannel, $request);
        $this->storeManualDomains($сhannel, $request);

        return back()->with('status', 'Новый канал успешно создан');
    }

    /**
     * Display the specified resource.
     *
     * @param Channels $сhannels
     * @return \Illuminate\Http\Response
     */
    public function show(Channels $сhannels)
    {
        //return view('Admin.Channels.show', ['сhannels' => $сhannels]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Channels $сhannels
     * @return void
     */
    public function edit($id)
    {
        $сhannel = Channels::find($id);
        $tags = $сhannel->manualTags;
        $domains = $сhannel->manualDomains;
        return view('Admin.Channels.edit', ['channel' => $сhannel, 'tags' => $tags, 'domains' => $domains]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Channels $сhannels
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateChannel = Channels::find($id);
        $updateChannel->update([
            'name' => $request->name,
            'website' => $request->website,
            'fb_group_id' => $request->fb_group_id,
            'body' => $request->chanel_content,
            'active' => $request->public,
        ]);

        $this->storeManualTags($updateChannel, $request);
        $this->storeManualDomains($updateChannel, $request);

        return back()->with('status', 'Запись успешно обновленна');
    }

    public function storeManualTags($сhannel, $request)
    {
        $сhannel->manualTags()->detach();

        if ($request->has('tags'))
        {
            foreach ($request->tags as $tagId)
            {
                $сhannel->manualTags()->attach([
                    'tags_id' => $tagId
                ]);
            }
        }
    }

    public function storeManualDomains($сhannel, $request)
    {
        $сhannel->manualDomains()->detach();

        if ($request->has('domains'))
        {
            foreach ($request->domains as $domainId)
            {
                $сhannel->manualDomains()->attach([
                    'domains_id' => $domainId
                ]);
            }
        }
    }

    public function generateStoreManualDomains($сhannel, $domain)
    {
        $сhannel->manualDomains()->detach();

        if ($domain->id)
        {
            $сhannel->manualDomains()->attach([
                'domains_id' => $domain->id
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $channel_destroy = Channels::find($request->id);
        $channel_destroy->manualTags()->detach();
        $channel_destroy->manualDomains()->detach();
        $channel_destroy->delete();
        return back();
    }


    public function generate()
    {
        $domains = Domains::select('id', 'domain')
            ->where([
                ['type','admin'],
                ['parser_content','1'],
            ])
            ->orWhere([
                ['type','parser'],
                ['parser_content','1'],
            ])
            ->get();
        $channels = Channels::get();
        $channels_array = array();
        if(count($channels) > 1){
            foreach ($channels as $channel){
                $channels_array[] = $channel->name;
            }
        }
        foreach ($domains as $domain){
            if(!in_array($domain->domain, $channels_array)){
                $сhannel = Channels::create([
                    'name' => $domain->domain,
                    'website' => $domain->domain,
                    'fb_group_id' => 0,
                    'body' => NULL,
                    'active' => '1',
                ]);

                $this->generateStoreManualDomains($сhannel, $domain);
            }

        }
        return back()->with('status', 'Каналы успешно сгенерированы');
    }

}
