<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\StoreSeoRequest;
use App\Http\Requests\Admin\UpdateSeoRequest;
use App\Model\Seo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SeoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $seoItems = Seo::orderBy('id', 'desc')->get();

        return view('Admin.Seo.index', compact('seoItems'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.Seo.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param StoreSeoRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreSeoRequest $request)
    {
        Seo::create($request->all());

        return redirect()->route('admin.seo.index')->with('success', 'Запись успешно добавлена!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $seoItem = Seo::find($id);

        return view('Admin.Seo.edit', compact('seoItem'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateSeoRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateSeoRequest $request, $id)
    {
        $seoItem = Seo::find($id);

        $seoItem->update($request->all());

        return redirect()->route('admin.seo.edit', $seoItem->id)->with('success', 'Запись успешно обновлена!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $seoItem = Seo::find($id);
        $seoItem->delete();

        return redirect()->route('admin.seo.index')->with('success', 'Запись успешно удалена!');
    }


    /**
     * Change active of seo item
     *
     * @param Request $request
     * @return string
     * @throws \Throwable
     */
    public function switchActive(Request $request)
    {
        $seoItem = Seo::find($request->seoItemId);
        $seoItem->active = ($seoItem->active) ? 0 : 1;
        $seoItem->save();

        $seoItems = Seo::orderBy('id', 'desc')->get();

        return view('Admin.Seo.table_items', compact('seoItems'))->render();
    }

}
