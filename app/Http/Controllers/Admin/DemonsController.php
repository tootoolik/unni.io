<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Demons\Demons;
use App\Model\Deamons;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DemonsController extends Controller
{
    public function index(){

//        $demon = new Demons('http://unni.io/demonsss');
//        $processId = $demon->getPid();
//        DB::table('deamons_list')->insert(['id_pid' => $processId]);



//           $demon = new Demons();
//        $demon->setPid('19972');
//        $status = $demon->status();
//        dump($status);
//        $status = $demon->stop('32754');


//        $demon->start('http://unni.io/demonsss');
//        $demon->stop('14209');

        $allDeamons = Deamons::all();
        return view('Admin.Demons.index', compact('allDeamons'));
    }

    public function create(){
        return view('Admin.Demons.create');
    }

    public function store(Request $request){
        if ($request->work == '1'){
            $demon = new Demons($request->command);
            $processId = $demon->getPid();
            Deamons::updateOrCreate([
                'id_pid' => $processId,
                'active' => $request->work,
                'work' => $request->work,
                'command' => $request->command]);
        }else{
            Deamons::updateOrCreate([
                'active' => $request->work,
                'work' => $request->work,
                'command' => $request->command]);
        }

        return back()->with('status','Демон создан');
    }

    public function destroy(Request $request)
    {
        $demon = new Demons();
        $demon->stop($request->id_pid);
        $demon->stop($request->id_pid+1);
        Deamons::destroy($request->id);
        $request->id_pidW = $request->id_pid + 1;
        return back()->with('status','процессы '.$request->id_pid.' и '.$request->id_pidW.' были уничтожены и удалены. Действие критическое, выполняется сразу');
    }
}
