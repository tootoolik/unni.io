<?php

namespace App\Http\Controllers\Admin;

use App\Model\Dictionary;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class DictionaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dictionary = Dictionary::paginate(15);
        return view('Admin.Dictionary.index', compact('dictionary'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->addDictionary){
            $request->addDictionary = explode("\r\n", $request->addDictionary);
            foreach ($request->addDictionary as $addDictionary) {
                $answer[] = Dictionary::updateOrCreate(['title' => $addDictionary]);
            }
            $cool = 'Тег добавлен';
            return redirect('/test/hint_with_dictionary/')->with('status', $cool);

        }else{
            return view('Admin.Dictionary.addDictionary');
        }
    }
    public function create_file(Request $request)
    {
        if (\request()->ajax()){
            $fileSize = $request->file('files')->getSize();
            $file = $request->file('files');
            $file_cont = $file->openFile()->fread($fileSize);
            $rows        = explode("\n", $file_cont);
            array_shift($rows);
            foreach($rows as $row => $data)
            {
                $row_data = explode('^', $data);
                $dfsdf = Dictionary::updateOrCreate(['title' => $row_data[0]]);
                    $countNew[] = $dfsdf->wasRecentlyCreated;
            }
            return count($countNew);
        }else{
            return view('Admin.Dictionary.addDictionaryFile');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
