<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreTagRequest;
use App\Model\Tags;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Traits\Tag\TagsTrait;

class TagsController extends Controller
{
    use TagsTrait;

    public function index(Request $request)
    {
        if($request->sort){
            $tags = \App\Model\Admin\Tag::orderBy($request->sort, 'desc')->paginate(50);
        }else{
            $tags = \App\Model\Admin\Tag::orderBy('count', 'desc')->paginate(50);
        }


        return view('Admin.Tags.index', compact('tags'));
    }

    public function tagForm()
    {
        return view('Admin.Tags.Forms.addTag');
    }

    /*
     * Добавление тегов/тега
     */
    public function addTag(Request $request)
    {
        if (!empty($request->addTag)) {
            $existingTags = [];
            $addedTags = [];

            foreach ($request->addTag as $addTag) {
                $addTag = mb_strtolower($addTag);
                $existTag = Tags::where('tag', $addTag)->first();
                if (!$existTag) {
                    //генерируем slug и проверяем его уникальность
                    $newSlug = $this->generateSlug($addTag);
                    //сохраняем новый Тег
                    Tags::create(['tag' => $addTag, 'slug' => $newSlug, 'type' => 'hand']);
                    $addedTags[] = $addTag;
                } else {
                    $existingTags[] = $addTag;
                }
            }

            $status = '';
            if ($existingTags) {
                $status .= 'Данные теги уже существуют: ' . implode(', ', $existingTags) . ' ';
            }

            if ($addedTags) {
                $status .= 'Данные теги были добавлены: ' . implode(', ', $addedTags) . ' ';
            }

            return redirect('/test/tags/add_tags')->with('status', $status);
        } else {
            return view('Admin.Tags.addTags');
        }
    }

    public function delete($id)
    {
        $deleteTag = Tags::find($id);
        $deleteTag->delete();
        return redirect('/test/tags')->with('status', 'Тег удален');
    }

    /*
     * Редактируем карточку тега
     */
    public function edit(Request $request)
    {
        $tag = Tags::find($request->id);
        if ($tag) {
            //получаем тег
            if ($request->isMethod('get')) {
                return view('Admin.Tags.editTags', compact('tag'));
            } //сохраняем тег
            elseif ($request->isMethod('post')) {
                $this->validate($request, [
                    'tag' => 'required|max:128',
                    'slug' => 'required|max:128'
                ]);
                $existSlug = Tags::where([['slug', '=', $request->slug], ['id', '!=', $tag->id]])
                                    ->orWhere([['tag', '=', $request->tag], ['id', '!=', $tag->id]])->first();
                if ($existSlug) {
                    //не пропускаем дубли
                    return redirect()->back()->with('status', 'Tag или Slug совпадает с другим уже существующим тегом.');

                } else {
                    $tag->tag = $request->tag;
                    $tag->slug = $request->slug;
                    $tag->save();
                    //сообщение что успешно сохранен
                    return redirect()->route('admin.tags.index')->with('status', 'Tag успешно сохранен.');
                }
            }
        } else {
            abort(404);
        }
    }

    /*
     * обработка - генерация slug для
     */
    public function slugsTagsGen()
    {
        $tags = Tags::whereNull('slug')->get();
        foreach ($tags as $tag) {
            //генерируем slug и проверяем его уникальность
            $tag->slug = $this->generateSlug($tag->tag,$tag->id);
            $tag->save();
        }
        return redirect('/test/tags')->with('status', 'Обработка завершена');
    }


    public function getTags(Request $request)
    {
        if ($request->ajax()) {
            $tagsCollection = Tags::where('tag', 'LIKE', '%' . $request->text . '%')
                ->orderByDesc('count')
                ->limit(10)
                ->get();

            $tags = $this->transformDataForTokenize2($tagsCollection);

            return response()->json($tags);
        }
    }

    private function transformDataForTokenize2($tags)
    {
        $result = [];

        foreach ($tags as $tag) {
            $result[] = [
                'text' => $tag->tag,
                'value' => $tag->id
            ];
        }

        return $result;
    }

    public function showSwitchActive(Request $request)
    {
        if ($request->tagId) {
            $tag = Tags::where('id', $request->tagId)->first();
            $tag->show = $tag->show ? 0 : 1;
            $tag->save();
        }
    }

    public function search(Request $request)
    {
        if ($request->ajax()) {
            $search = $request->search;

            $tags = Tags::where('tag', 'LIKE', $search . '%')
                ->orderBy('count', 'desc')
                ->limit(50)
                ->get();

            return view('Admin.Tags.content', compact('tags'))->render();
        }
    }
}
