<?php

namespace App\Http\Controllers\AuditorBot;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Zend\EventManager\Event;

use App\Auditor\Auditor;

use App\Traits\BustGetUrl;

class AuditorController extends Controller
{
    use BustGetUrl;
    const SLEEP = 3;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){
        $startCron = DB::table('auditor_cron')->orderBy('id', 'desc')->first();
//        dd($startCron->level);
        if (!isset($startCron->level) || $startCron->level == 0){
            DB::table('auditor_cron')->insert(['level' => '1']);
            $this->listDomain();
            DB::table('auditor_cron')->insert(['level' => '0']);
        }
        $startCronUrls = DB::table('auditor_cron_urls')->orderBy('id', 'desc')->first();
        if (!isset($startCronUrls->level) || $startCronUrls->level == 0){
            DB::table('auditor_cron_urls')->insert(['level' => '1']);
            $this->listUrl();
            DB::table('auditor_cron_urls')->insert(['level' => '0']);
        }
    }

    /**
     * @return mixed
     * parser home site
     */
    public function listDomain(){
        $auditors = DB::table('auditor_domain')->select('domain', 'id')->orderBy('id', 'DESC')->get();
//        dd($auditors->all());
        foreach ($auditors->all() as $item => $key) {
            if (!DB::table('auditor_domain_cron')->select('id_domain', 'id')->where('id_domain', '=', $key->id)->first()) {
//            echo $key->domain;
                $objUrlGet = $this->objUrlGet($key->domain);
                foreach ($objUrlGet->find('a') as $elem) {
                    echo $elem->href . '<br>';
                    $testUrl = strpos($elem->href, $key->domain);
                    if (!DB::table('auditor_url_children')->where('url', $elem->href)->first() &&
                        $testUrl !== false && strlen($elem->href) < 700) {
                        DB::table('auditor_url_children')->insert([
                            'id_domain' => $key->id,
                            'url' => $elem->href,
                            'updated_at' => date('Y-m-d G:i:s'),
                            'created_at' => date('Y-m-d G:i:s')
                        ]);
                    }
                }
                DB::table('auditor_domain_cron')->insert([
                    'id_domain' => $key->id,
                    'status' => '1',
                    'updated_at' => date('Y-m-d G:i:s'),
                    'created_at' => date('Y-m-d G:i:s')
                ]);
            }else{
//                DB::table('auditor_domain_cron')->insert([
//                    'id_domain' => $key->id,
//                    'status' => '1',
//                    'updated_at' => date('Y-m-d G:i:s'),
//                    'created_at' => date('Y-m-d G:i:s')
//                ]);
            }
        }
        return $auditors;
    }
    public function listUrl(){
//        $urlList = $this->getAllSite();
//        dd($urlList);
        Auditor::join('auditor_url_children', 'auditor_url_children.id_domain', '=', 'auditor_domain.id')->select('auditor_url_children.url', 'auditor_domain.id', 'auditor_domain.domain')->chunk(10, function($urlList)
        {
            foreach ($urlList as $list)
            {
                if (strpos($list->url, 'http') !== false){
                    if (strpos($list->url, $list->domain) !== false){
                        $url = $list->url;
                        $domain = $list->id;
                    }
                }else{
                    $url = $list->domain.$list->url;
                    $domain = $list->id;
                }
                $objUrlGet = $this->objUrlGet($url)->find('a');
                $this->clearOperait();
            }
            foreach ($objUrlGet as $item) {
//                echo $item->href;
//                        $testUrlHttp = strpos($item->href, 'http');
                        if (!DB::table('auditor_url_children')->where('url', $item->href)->first() && strlen($item->href) < 700) {
                            DB::table('auditor_url_children')->insert([
                                'id_domain' => $domain,
                                'url' => $item->href,
                                'updated_at' => date('Y-m-d G:i:s'),
                                'created_at' => date('Y-m-d G:i:s')
                            ]);
                        }
//                    elseif (!DB::table('auditor_url_children')->where('url', $item->href)->first() && $testUrlHttp === false){
//                        DB::table('auditor_url_children')->insert([
//                            'id_domain' => $item['id'],
//                            'url' => $item->href,
//                            'updated_at' => date('Y-m-d G:i:s'),
//                            'created_at' => date('Y-m-d G:i:s')
//                        ]);
//                    }
            }
            return false;

//            for($url; $url<= 10; $url++){
//                echo $url.'<br>';
//            }
//            echo $url.'<br>';
//            return false;
//            foreach ($objUrlGet as $item) {
//                echo $item.'<br>';
//            }
        });
//        foreach ($urlList as $item) {
//            if (strpos($item->url, 'http') !== false){
//                if (strpos($item->url, $item->domain) !== false){
//                    $url = $item->url;
//                }
//            }else{
//                $url = $item->domain.$item->url;
//            }
//                $objUrlGet = $this->objUrlGet($url)->find('a');
//                if (!empty($objUrlGet)) {
//                    foreach ($objUrlGet as $elem) {
//                    echo $elem->href . '<br>';
////                        $testUrl = strpos($elem->href, $item->domain);
////                        $testUrlHttp = strpos($elem->href, 'http');
////                        if (!DB::table('auditor_url_children')->where('url', $elem->href)->first() &&
////                            $testUrl !== false && strlen($elem->href) < 700) {
////                            DB::table('auditor_url_children')->insert([
////                                'id_domain' => $item['id'],
////                                'url' => $elem->href,
////                                'updated_at' => date('Y-m-d G:i:s'),
////                                'created_at' => date('Y-m-d G:i:s')
////                            ]);
////                        }
//////                    elseif (!DB::table('auditor_url_children')->where('url', $elem->href)->first() && $testUrlHttp === false){
//////                        DB::table('auditor_url_children')->insert([
//////                            'id_domain' => $item['id'],
//////                            'url' => $elem->href,
//////                            'updated_at' => date('Y-m-d G:i:s'),
//////                            'created_at' => date('Y-m-d G:i:s')
//////                        ]);
//////                    }
////                        $this->clearOperait();
//                    }
//                }
//        }
    }

    /**
     * clear links garbage (url with #)
     * очистка ссылки от якорей
     */
    public function clearLinks(){
        $allLinks = DB::table('auditor_url_children')->get();
        foreach ($allLinks as $allLink) {
            if (strpos($allLink->url, '#') !== false){
                DB::table('auditor_url_children')->where('url', '=', $allLink->url)->delete();
            }

        }
    }

    public function indexaaaa()
    {
        $startCron = DB::table('auditor_cron')->latest()->limit(1)->get();
//        dd($startCron['0']);
        if ($startCron['0']->level == 0){
            $startCron = DB::table('auditor_cron')->insert(['level' => '1']);
            $auditor = Auditor::pluck('domain', 'id');
            foreach ($auditor->all() as $item => $key) {
//                sleep(self::SLEEP);

                $proxys = array(
                    0 => '198.27.66.158:1080',
                    1 => '180.210.205.110:3129',
                    2 => '203.126.218.186:80',
                    3 => '139.59.223.115:8118',
                    4 => '74.118.81.18:8080'
                );
                foreach ($proxys as $proxy) {
                    $objUrlGet = $this->objUrlGet($key, $proxy);


                    if (!empty($objUrlGet)) {
                        foreach ($objUrlGet->find('a') as $elem) {
                            $testUrl = strpos($elem->href, $key);
                            $testUrlHttp = strpos($elem->href, 'http');
                            if (!DB::table('auditor_url_children')->where('url', $elem->href)->first() &&
                                $testUrl !== false && strlen($elem->href) < 700) {
//                        echo $elem->href.'<br>';
                                DB::table('auditor_url_children')->insert([
                                    'id_domain' => $item,
                                    'url' => $elem->href,
                                    'updated_at' => date('Y-m-d G:i:s'),
                                    'created_at' => date('Y-m-d G:i:s')
                                ]);
                            } elseif (!DB::table('auditor_url_children')->where('url', $elem->href)->first() &&
                                $testUrlHttp === false && strlen($elem->href) < 700) {
//                        echo $elem->href.'<br>';
                                DB::table('auditor_url_children')->insert([
                                    'id_domain' => $item,
                                    'url' => $elem->href,
                                    'updated_at' => date('Y-m-d G:i:s'),
                                    'created_at' => date('Y-m-d G:i:s')
                                ]);
                            }
                        }
                    }
                }
            }
            $allUrlSite = $this->getAllSite();
            foreach ($allUrlSite as $item) {
//                sleep(self::SLEEP);
                if (strpos($item->url, 'http') !== false){
                    if (strpos($item->url, $item->domain) !== false){
                        $url = $item->url;
                    }
                }else{
                    $url = $item->domain.$item->url;
                }
//            echo '<pre>';
//            var_dump($url);
//            echo '<pre>';
//            echo gettype($url), "\n";
//            echo $url.'<br>';
                $proxys = array(
                    0 => '198.27.66.158:1080',
                    1 => '180.210.205.110:3129',
                    2 => '203.126.218.186:80',
                    3 => '139.59.223.115:8118',
                    4 => '74.118.81.18:8080'
                );
                foreach ($proxys as $proxy) {
                    $objUrlGet = $this->objUrlGet($url, $proxy);
                    if (!empty($objUrlGet)) {
                        foreach ($objUrlGet->find('a') as $elem) {
//                    echo $elem->href . '<br>';
                            $testUrl = strpos($elem->href, $item->domain);
                            $testUrlHttp = strpos($elem->href, 'http');
                            if (!DB::table('auditor_url_children')->where('url', $elem->href)->first() &&
                                $testUrl !== false && strlen($elem->href) < 700) {
                                DB::table('auditor_url_children')->insert([
                                    'id_domain' => $item['id'],
                                    'url' => $elem->href,
                                    'updated_at' => date('Y-m-d G:i:s'),
                                    'created_at' => date('Y-m-d G:i:s')
                                ]);
                            }
//                    elseif (!DB::table('auditor_url_children')->where('url', $elem->href)->first() && $testUrlHttp === false){
//                        DB::table('auditor_url_children')->insert([
//                            'id_domain' => $item['id'],
//                            'url' => $elem->href,
//                            'updated_at' => date('Y-m-d G:i:s'),
//                            'created_at' => date('Y-m-d G:i:s')
//                        ]);
//                    }
                        }
                    }
                }
            }
            DB::table('auditor_cron')->insert(['level' => '0']);
//            sleep(self::SLEEP);
            return 'end';
        }
        DB::table('auditor_cron')->insert(['level' => '0']);
//        sleep(self::SLEEP);
        return 'enda';
    }

    public function getAllSite(){
//        return Auditor::join('auditor_url_children', 'auditor_url_children.id_domain', '=', 'auditor_domain.id')->select('auditor_url_children.url', 'auditor_domain.id', 'auditor_domain.domain')->get();
        Auditor::join('auditor_url_children', 'auditor_url_children.id_domain', '=', 'auditor_domain.id')->select('auditor_url_children.url', 'auditor_domain.id', 'auditor_domain.domain')->chunk(50, function($urlList)
        {
            foreach ($urlList as $list)
            {
                $list->url;
            }
            return false;
        });

    }

//    private function objUrlGet($url){
//        // identify regex
//        $regex  = '/(<a\s*'; // Start of anchor tag
//        $regex .= '(.*?)\s*'; // Any attributes or spaces that may or may not exist
//        $regex .= 'href=[\'"]+?\s*(?P<link>\S+)\s*[\'"]+?'; // Grab the link
//        $regex .= '\s*(.*?)\s*>\s*'; // Any attributes or spaces that may or may not exist before closing tag
//        $regex .= '(?P<name>\S+)'; // Grab the name
//        $regex .= '\s*<\/a>)/i'; // Any number of spaces between the closing anchor tag (case insensitive)
//        $ch = curl_init();
//        curl_setopt_array( $ch, array(
//            CURLOPT_FOLLOWLOCATION => true,
//            CURLOPT_MAXREDIRS => 10,
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_TIMEOUT => 10,
//            CURLOPT_URL => $url));
//
//        $PaserUrl = curl_exec($ch);
//        preg_match_all($regex,$PaserUrl,$url);
////        $html = new HtmlDomParser();
//        $objUrlGet= HtmlDomParser::str_get_html($PaserUrl);
//        return $objUrlGet;
//        $html->clear();
//        unset($html);
//    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Get content from html.
     *
     * @param $parser object parser settings
     * @param $link string link to html page
     *
     * @return array with parsing data
     * @throws \Exception
     */
//    public function getContent($parser, $link)
//    {
////// Get html remote text.
////        $html = file_get_contents($link);
////
////// Create new instance for parser.
////        $crawler = new Crawler(null, $link);
////        $crawler->addHtmlContent($html, 'UTF-8');
////
////// Get title text.
////        $title = $crawler->filter($parser->settings->title)->text();
////
////// If exist settings for teaser.
////        if (!empty(trim($parser->settings->teaser))) {
////            $teaser = $crawler->filter($parser->settings->teaser)->text();
////        }
////
////// Get images from page.
////        $images = $crawler->filter($parser->settings->image)->each(function (Crawler $node, $i) {
////            return $node->image()->getUri();
////        });
////
////// Get body text.
////        $bodies = $crawler->filter($parser->settings->body)->each(function (Crawler $node, $i) {
////            return $node->html();
////        });
////
////        $content = [
////            'link' => $link,
////            'title' => $title,
////            'images' => $images,
////            'teaser' => strip_tags($teaser),
////            'body' => $body
////        ];
////
////        return $content;
//    }


}
