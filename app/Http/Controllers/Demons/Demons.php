<?php
/**
 * Created by PhpStorm.
 * User: Pafas
 * Date: 26.06.2018
 * Time: 10:56
 */

namespace App\Http\Controllers\Demons;


use Illuminate\Support\Facades\DB;
use function simplehtmldom_1_5\file_get_html;

class Demons
{
//    private $command;
//
//    public function start($command){
//        // Форкаем процесс
//        $pid = pcntl_fork();
//        if ($pid == -1) {
//            // Ошибка
//            die('could not fork'.PHP_EOL);
//        } else if ($pid) {
//            // Родительский процесс, убиваем
//            die('die parent process'.PHP_EOL);
//        } else {
//            // Отцепляемся от терминала
//            posix_setsid();
//
//            //Получаем id процесса в unix
//            $pid = getmypid();
//            DB::table('deamons_list')->insert(['id_pid' => $pid]);
//            // Новый процесс, запускаем главный цикл
//            system('wget '.$command.' &');
//        }
//        return $pid;
//    }
//
//    public function stop($pid){
//        exec("kill -9 $pid");
//    }
    private $pid;
    private $command;

    public function __construct($cl=false){
        if ($cl != false){
            $this->command = $cl;
            $this->runCom();
        }
    }
    private function runCom(){
        $command = 'wget -P '.$this->command.' > /dev/null 2>&1 & echo $!';
        exec($command ,$op);
        $this->pid = (int)$op[0];
    }

    public function setPid($pid){
        $this->pid = $pid;
    }

    public function getPid(){
        return $this->pid;
    }

    public function status(){
        $command = 'ps -p '.$this->pid;
        exec($command,$op);
        if (!isset($op[1]))return false;
        else return true;
    }

    public function start(){
        if ($this->command != '')$this->runCom();
        else return true;
    }

    public function stop($command){
//        $command = $this->pid;
        exec('kill -9 '.$command);
        if ($this->status() == false)return true;
        else return false;
    }
}