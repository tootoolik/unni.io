<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Model\Bots\Auditor\CountParser;
use App\Model\Bots\Jeweler\JewelerContent;
use App\Model\Tags;
use App\Traits\Pagination;
use Elasticsearch\ClientBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use JasonGrimes;

class SearchContentController extends Controller
{
    use Pagination;

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * In $request->search srting search
     * @throws \Throwable
     */
    public function search(Request $request)
    {
        $searchWord = '';
        $allTags = Tags::where('show', 1)->orderBy('count','desc')->get();
        if (!empty($request->search) && $request->typeSearch) {
            $client = ClientBuilder::create()->build();
            $params['index'] = 'jeweler_content';
            $params['type'] = 'my_type';
            $params['from'] = $request->page - 1;
            $params['size'] = 10;
            if ($request->typeSearch == 'search_content') {
                $params['body'] = ['query' => [
                    'bool' => [
                        'should' => [
                            ['match_phrase' => ['description' => $request->search]],
                            ['match_phrase' => ['title' => $request->search]],
                            ['match' => ['description' => $request->search]],
                            ['match' => ['title' => $request->search]],
                        ]
                    ]
                ]
                ];
            } else {
                $params['body'] = ['query' => [
                    'bool' => [
                        'should' => [
                            ['match_phrase' => ['title' => $request->search]],
                            ['match' => ['title' => $request->search]],
                        ]
                    ]
                ]];
            }
            $accounts = $client->search($params);
            $resultSearch = $accounts['hits']['hits'];
            $paginateTotal = $accounts['hits']['total'];

            $urlPattern = '?' . http_build_query(array_merge($request->query(), ["page" => ''])) . '(:num)';
            if ($paginateTotal > 0) {
                $paginates = new JasonGrimes\Paginator($paginateTotal, $params['size'], $request->page ?? '1', $urlPattern);
            } else {
                $paginates = 0;
            }

            $tags = $this->getTagsFromSearchResult($resultSearch)->groupBy('url_id');

            $searchWord = $request->search;

        } else {
            $paginates = 0;
        }


        if ($request->ajax()) {

            return view('loadSearch', compact(
                'tags',
                'resultSearch',
                'paginates',
                'searchWord'
            ))->render();
        }
        $countIndexisUrls = CountParser::sum('count_link');
        $countIndexis = CountParser::sum('count_content');

        $freshContents = JewelerContent::orderBy('id', 'desc')->with('linksContent')->limit(50)->get();

        $meta_title_page='';
        if($request->page){
            $meta_title_page ='страница '.$request->page;
        }

        return view('welcome', compact(
            'tags',
            'resultSearch',
            'countIndexis',
            'countIndexisUrls',
            'allTags',
            'paginates',
            'freshContents',
            'meta_title_page',
            'searchWord'
        ));
    }

    private function getTagsFromSearchResult($res)
    {
        $ids = array_map(function ($v) {
            return $v['_source']['id_url'];
        }, $res);

        return DB::table('tags_urls')->whereIn('url_id', $ids)->leftJoin('tags', 'tags_urls.tag_id', 'tags.id')->limit(5)->get();
    }

    public function getFreshContent()
    {
        $freshContents =  JewelerContent::orderBy('id', 'desc')->with('tags')->limit(50)->get();

        return view('fresh_content', compact('freshContents'))->render();
    }
}
