<?php

namespace App\Http\Controllers\Front;

use App\Front\FollowLink;
use App\Model\Fornt\AuditorUrl;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UserFixedController extends Controller
{
    public function clickRemoteLinks(Request $request)
    {
        $urlId = AuditorUrl::where('url', $request->link)->first();

        if($urlId) {
            $newLink = FollowLink::where('cookie', $request->imprint)
                ->where('ip', \request()->ip())
                ->where('link', $request->link)
                ->first();
            if (!$newLink){
                FollowLink::create(
                    [
                        'ip' => \request()->ip(),
                        'url_id' => $urlId->id,
                        'source_page' => $request->source_page,
                        'link' => $request->link,
                        'cookie' => $request->imprint,
                    ]
                );
                return true;
            }
        }
    }
    public function followChannel()
    {
        $statusVisits = DB::table('follow_channel')
            ->where('imprint', \request()->imprint)
            ->where('domain_id', \request()->domain)
            ->first();
        if (!$statusVisits){
            DB::table('follow_channel')->insert(
                ['ip' => \request()->ip(), 'domain_id' => \request()->domain, 'imprint' => \request()->imprint]
            );
        }
    }
}
