<?php

namespace App\Http\Controllers\Front;

use App\Model\Fornt\AuditorUrl;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $news = AuditorUrl::where('status_news', 1)
                ->leftJoin('jeweler_content', 'auditor_url_children.id', '=', 'jeweler_content.id_url')
                ->orderBy('created_page', 'desc')
                ->paginate(25);
            return view('News.load', ['news' => $news])->render();
        }
        $news = AuditorUrl::where('status_news', 1)
            ->leftJoin('jeweler_content', 'auditor_url_children.id', '=', 'jeweler_content.id_url')
            ->orderBy('created_page', 'desc')
            ->paginate(25);
        return view('News.index', ['news' => $news]);
    }
}
