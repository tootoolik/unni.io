<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Model\Fornt\Feed;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FeedsController extends Controller
{
    public function index(Request $request)
    {

        $feeds = Feed::with(['linksContent'])->orderBy('id', 'desc')->paginate(25);
        if(count($feeds)===0) abort(404);
        $title_page = $request->page ? ', страница ' . $request->page : '';
        //dd($feeds);
        if ($request->ajax())
            return view('Feeds.load', compact('feeds'))->render();

        return view('Feeds.index', ['feeds'=>$feeds,'title_page'=>$title_page]);

    }

    public function archive_feeds(Request $request)
    {
        $feeds = Feed::orderBy('id', 'desc')->with(['linksContent', 'tags'])->paginate(25);
        if(count($feeds)===0) abort(404);
        if ($request->ajax())
            return view('Archive.load', compact('feeds'))->render();

        return view('Archive.index', compact('feeds'));
    }
}
