<?php

namespace App\Http\Controllers\Front;

use App\Front\Domains;
use App\Http\Controllers\Controller;
use App\Model\Fornt\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DomainsController extends Controller
{
    public function domain(Request $request, $name_domains)
    {
        $domain = Domains::where('domain', $name_domains)->with(['domainTags'])->first();

        if ($domain)
        {
            $follow = DB::table('follow_channel')->where('domain_id', $domain->id)->count();

            $domainContents = Domains::find($domain->id)
                ->domainContent(true)
                ->select(
                    'jeweler_content.id',
                    'jeweler_content.id_url',
                    'auditor_url_children.id_domain',
                    'jeweler_content.title',
                    'jeweler_content.description',
                    'jeweler_content.image',
                    'jeweler_content.created_page'
                )
//                ->orderBy('jeweler_content.id', 'desc')
//                ->orderBy('jeweler_content.id_url', 'asc')
//                ->orderBy('jeweler_content.title', 'asc')
                ->orderBy('jeweler_content.id', 'desc')
                ->paginate(25,[]);

            $tags = $domain->domainTags->load('domains')->sortByDesc('count');
            //dd($tags);
            $similarChannels = $this->getSimilarChannels($domain, $tags);
        }
        else
        {
            abort(404);
        }
        if(count($domainContents)===0) abort(404);


        if ($request->ajax()){
            return view('Domains.load', compact('domainContents', 'tagsDomain', 'name_domains', 'similarChannels'))->render();
        }

        if ($domainContents[0]){
            return view('Domains.index', compact('domainContents', 'domain', 'tags', 'follow', 'tagsDomain', 'name_domains', 'similarChannels'));
        } else {
            return view('Domains.noResults', compact('domainContents', 'domain', 'tags', 'follow', 'tagsDomain', 'name_domains', 'similarChannels'));
        }
    }

    private function getSimilarChannels($domain, $tags)
    {
        if($tags->isEmpty())
            $tags = Tag::orderBy('count')->with('domains')->limit(100)->get();

        $similarChannels = $this->getChannelsByTags($tags, $domain, 5);

        $similarChannelsCount = $similarChannels->count();

        if($similarChannelsCount < 5)
        {
            // TODO realise this
        }

        return $similarChannels;
    }


    public function getChannelsByTags($tags, $domain, $count)
    {
        $similarChannels = collect([]);

        foreach ($tags as $tag)
        {
            $bestDomain = $tag->domains->where('domain', '!=', $domain->domain)->first();

            if(!$bestDomain)
                continue;

            $similarChannels->push($bestDomain);

            $similarChannels = $similarChannels->unique('domain');

            if($similarChannels->count() === $count)
                break;
        }

        return $similarChannels;
    }

    public function domainTags(Request $request, $name_domains)
    {
        $domain = Domains::where('domain', $name_domains)->with(['domainTags'])->first();

        $follow = DB::table('follow_channel')->where('domain_id', $domain->id)->count();

        $tags = $domain->domainTags('all')->orderBy('tags_domain.count', 'desc')->paginate(500);

        $similarChannels = $this->getSimilarChannels($domain, $tags);

        if ($request->ajax())
            return view('Domains.loadTags', compact('tags', 'similarChannels'))->render();

        return view('Domains.tags', compact('tags', 'domain', 'follow', 'similarChannels'));
    }
}
