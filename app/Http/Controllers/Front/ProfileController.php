<?php

namespace App\Http\Controllers\Front;

use App\Front\Domains;
use App\Front\Posts;
use Curl\Curl;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use App\Mail\FeedbackMail;
use Illuminate\Support\Facades\Mail;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function siteAdd()
    {
        return view('Profile.siteAdd');
    }

    public function store(Domains $domain)
    {
        $validator = Validator::make(\request()->all(), [
            'domain' => 'required|regex:^(?!\-)(?:[a-zA-Z\d\-]{0,62}[a-zA-Z\d]\.){1,126}(?!\d+)[a-zA-Z\d]{1,63}$^|unique:auditor_domain'
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        $curl = new Curl();
        if (!$curl->get('http://'.\request()->domain)){
            return redirect()->back()->withErrors('Домен не доступен')->withInput();
        }
        if (parse_url(\request()->domain, PHP_URL_HOST)){
            $url = parse_url(\request()->domain, PHP_URL_HOST);
        }else{
            $url = \request()->domain;
        }
        $count = $domain->count();

        $domain->domain = mb_strtolower($url);
        $domain->priority = ++$count;
        $domain->type = 'user';
        $domain->save();
        return redirect()->route('front.profile.site.add')->with('status', 'Домен добавлен');
    }

    public function myPosts()
    {
        $posts = $this->getUserPosts()->latest()->get();
        return view('Profile.myPosts', ['posts' => $posts]);
    }

    public function myModerationPosts()
    {
        $posts = $this->getUserPosts()->where('active', '0')->latest()->get();
        return view('Profile.myPosts', ['posts' => $posts ]);
    }

    public function myApprovedPosts()
    {
        $posts = $this->getUserPosts()->where('active', '1')->latest()->get();
        return view('Profile.myPosts', ['posts' => $posts]);
    }

    protected function getUserPosts()
    {
        $user_id = $this->getUserId();
        return Posts::whereHas('postAuthor', function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
        });
    }

    public function postsAdd()
    {
        $user_id = $this->getUserId();
        return view('Profile.postsAdd', ['user' => $user_id]);
    }

    protected function getUserId()
    {
        if (Auth::check()) {
            return Auth::user()->id;
        } else {
            return 0;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function postsStore(Request $request)
    {
        if ($request->hasFile('post_img'))
        {
            $post_img = $this->saveImage($request->file('post_img'));
        }
        else
        {
            $post_img = '';
        }
        $post = Posts::create([
            'name' => $request->post_name,
            'body' => $request->post_content,
            'post_img' => $post_img,
        ]);

        $this->storeManualTags($post, $request);
        $this->storeManualAuthor($post, $request);
        $this->adminNewPostSend();

        return back()->with('status', 'Публикация успешно создана');
    }

    public function storeManualTags($post, $request)
    {
        $post->manualTags()->detach();

        if ($request->has('tags'))
        {
            foreach ($request->tags as $tagId)
            {
                $post->manualTags()->attach([
                    'tags_id' => $tagId
                ]);
            }
        }
    }

    public function storeManualAuthor($post, $request)
    {
        $post->postAuthor()->detach();

        if ($request->has('post_author'))
        {
            foreach ($request->post_author as $authorId)
            {
                $post->postAuthor()->attach([
                    'user_id' => $authorId
                ]);
            }
        }
    }

    private function saveImage($file) :string
    {
        $path = $file->store('posts', ['disk' => 'public']);
        $image = Image::make(public_path('storage/' . $path));
        $image->save();
        return $path;
    }

    public function adminNewPostSend() {
        $comment = 'На сайте unni.io добавленна новая публикация ожидающая модерации';
        Mail::to(env('MAIL_ADMIN_ADDRESS'))->send(new FeedbackMail($comment));
    }

}

