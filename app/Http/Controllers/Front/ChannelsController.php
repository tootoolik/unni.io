<?php

namespace App\Http\Controllers\Front;

use App\Front\Channels;
use App\Front\ChannelsCross;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ChannelsController extends Controller
{

    const FILTER_DATE_KEY = 'channels_filter_date';
    const FILTER_OTHER_KEY = 'channels_filter_other';
    const FILTER_TAG_KEY = 'channels_filter_tag';

    public function index(Request $request)
    {
        $categories = $this->getCategories();

        $page = 0;
        if ($request->has('page')){
            $page = $request->page;
        }

        $channelsBuilder = $this->getChannelsBySort();
        $channels = $channelsBuilder->where('active', '1')
                                    ->offset($page * 50)
                                    ->limit(50)
                                    ->withCount('usersLike')
                                    ->get();

        if ($request->ajax()){
            return view('Channels.list_channels', compact('channels', 'categories'))->render();
        }

        return view('Channels.index', compact('channels', 'categories'));
    }

    public function getCategories()
    {
        return ChannelsCross::with(['tags', 'channels'])
            ->select('tags_id', \DB::raw('count(*) as count_tags'))
            ->groupBy('tags_id')
            ->orderBy('count_tags', 'desc')
            ->limit(50)
            ->get();
    }

    public function getChannelsByDate($from = '-30 years', $to = '1 days')
    {
        $dateFrom = date("Y-m-d", strtotime($from));
        $dateTo = date('Y-m-d', strtotime($to));

        return Channels::whereBetween('created_at', [$dateFrom, $dateTo]);
    }

    public function getChannelsBySort()
    {
        $channels = $this->getSortChannelsByDate();

        $orderedChannels = $this->orderByWithFilter($channels);

        $orderedChannelsByTag = $this->orderedChannelsByTag($orderedChannels);

        return $orderedChannelsByTag;
    }

    public function orderedChannelsByTag($channels)
    {
        if($this->isTagsAll())
        {
            return $channels;
        }
        else
        {
            $tagId = session(self::FILTER_TAG_KEY);

            return $channels->whereHas('manualTags', function ($query) use ($tagId) {
                $query->where('tags_id', $tagId);
            });
        }
    }

    private function isTagsAll()
    {
        return (!session(self::FILTER_TAG_KEY) || session(self::FILTER_TAG_KEY) === 'all');
    }

    public function getSortChannelsByDate()
    {
        $filterDateFrom = session(self::FILTER_DATE_KEY) ?? '-30 years';

        $filterDateTo = $this->isYesterday() ? '-1 days' : '1 days';

        return $this->getChannelsByDate($filterDateFrom, $filterDateTo);
    }

    private function isYesterday(): bool
    {
        return session(self::FILTER_DATE_KEY) && session(self::FILTER_DATE_KEY) === '-1 days';
    }

    public function orderByWithFilter($channels)
    {
        if (session(self::FILTER_OTHER_KEY) && session(self::FILTER_OTHER_KEY) === 'popularity')
            $orderedChannels = $channels->withCount('usersLike')->orderBy('users_like_count', 'desc');
        else
            $orderedChannels = $channels->orderBy('created_at', 'desc');

        return $orderedChannels;
    }

    public function getLikeUsers(Request $request)
    {
        $users = Channels::find($request->channelId)
            ->usersLike()
            ->orderBy('created_at')
            ->limit(20)
            ->get();

        $usersContent = view('Channels.modal_content', compact('users'))->render();

        return response()->json($usersContent);
    }

    public function userSave(Request $request)
    {
        if (Auth::check())
        {
            $channel = Channels::find($request->channelId);

            if (Auth::user()->checkChannelsSaved($channel->id))
                Auth::user()->channelsSave()->detach($channel->id);
            else
                Auth::user()->channelsSave()->attach([
                    'channel_id' => $channel->id
                ]);

            $response = view('Channels.user_save_content', compact('channel'))->render();

            return response()->json($response);
        }
    }

    public function userSubscribe(Request $request)
    {
        if (Auth::check())
        {
            $channelId = $request->channelId;

            if (Auth::user()->checkChannelsSubscribe($channelId))
                Auth::user()->channelsSubscribe()->detach($channelId);
            else
                Auth::user()->channelsSubscribe()->attach([
                    'channel_id' => $channelId
                ]);
        }
    }

    public function markAsLiked(Request $request)
    {
        if (Auth::check())
        {
            $channelId = $request->channelId;

            if (Auth::user()->checkChannelsLike($channelId))
                Auth::user()->channelsLike()->detach($channelId);
            else
                Auth::user()->channelsLike()->attach([
                    'channel_id' => $channelId
                ]);
        }
    }

    public function savingStateSortingInSession(Request $request)
    {
        switch ($request->type)
        {
            case 'date':
                session()->put(self::FILTER_DATE_KEY, $request->sort);
                break;
            case 'filter':
                session()->put(self::FILTER_OTHER_KEY, $request->sort);
                break;
            case 'tag':
                session()->put(self::FILTER_TAG_KEY, $request->sort);
                break;
        }
    }

    public function updateChanelsCounts($id, $params)
    {
        Channels::find($id)->update($params);
    }

}
