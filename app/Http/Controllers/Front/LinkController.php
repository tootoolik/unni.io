<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class LinkController extends Controller
{
    public function redirectToLinks(Request $request)
    {
        if ($request->i){
            return redirect($request->i);
        }else{
           return abort(404);
        }

    }
}
