<?php

namespace App\Http\Controllers\Front;

use App\Auditor\Auditor;
use App\Model\Bots\Auditor\AuditorUrls;
use App\Model\Fornt\AuditorUrl;
use App\Model\Fornt\Tag;
use App\Model\Tags;
use Elasticsearch\ClientBuilder;
use http\Env;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JasonGrimes;
use App\Traits\Tag\TagsTrait;

class TagsController extends Controller
{
    use TagsTrait;

    protected  $tags;
    protected  $pageN=1;

    public function __construct(Tags $tags)
    {
        $this->tags = $tags;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if (strlen($request->name_tags) > 2){
            $allTags = Tag::where('tag', 'LIKE', '%'.$request->name_tags.'%')
                ->orderBy('count', 'desc')
                ->paginate(100);
            if ($request->ajax()) {
                return view('Tags.load', ['allTags' => $allTags])->render();
            }
            return view('Tags.index', ['allTags' => $allTags]);
        }
        $allTags = $this->tags->orderBy('count', 'desc')->paginate(200);
        if ($request->tab && $request->tab == 'name'){
            $allTags = $this->tags->orderBy('tag', 'asc')->paginate(100);
        }
        if ($request->tab && $request->tab == 'new'){
            $allTags = $this->tags->orderBy('id', 'desc')->paginate(100);
        }
        if ($request->ajax()) {
            return view('Tags.load', ['allTags' => $allTags])->render();
        }
        return view('Tags.index', compact('allTags'));
    }
    public function scrollTags(Request $request){
        if (strlen($request->search) > 2){
            $resultSearch = Tag::where('tag', 'LIKE', '%'.$request->search.'%')
                ->orderBy('count', 'desc')
                ->paginate(100);
            return Response::json(view('Tags.search', ['resultSearch' => $resultSearch])->render());
        }
        return view('Tags.search');
    }
    public function searchTags(Request $request){
        return Tags::where('tag', 'LIKE', '%'.$request->search.'%')->paginate(10);
    }
    public function addUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tag' => 'required|unique:tags|max:128',
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        $slug = $this->generateSlug($request->tag);

        $addStatus = Tags::Create(
            ['url_id' => $request->id, 'tag' => $request->tag,'slug' => $slug, 'type' => 'add_user']
        );

        if (empty($addStatus->wasRecentlyCreated)){
            return back()->with('status', 'Тег ранее был предложен');
        }
        return back()->with('status', 'Тег отправлен на модерацию');
    }
    public function urls(Request $request, $tagId)
    {
        $tagsName = Tag::where('slug', $tagId)->first();
        $tagId = $tagsName->id;
        if ($request->page){
            $this->pageN=$request->page;
        }

        $tags = DB::table('tags_urls')
            ->leftJoin('auditor_url_children', 'tags_urls.url_id', '=', 'auditor_url_children.id')
            ->leftJoin('jeweler_content', 'jeweler_content.id_url', '=', 'tags_urls.url_id')
            ->leftJoin('tags', 'tags.id', '=', 'tags_urls.url_id')
            ->where('tag_id', $tagId)
            ->groupBy('tags_urls.url_id')
            ->orderBy('tags_urls.url_id', 'desc')
            ->paginate(25);

        if ($request->ajax()) {
            return view('Tags.loadUrls', ['tags' => $tags, 'tagsName' => $tagsName])->render();
        }
        //dd($tags);
        return view('Tags.urls', ['tags' => $tags, 'tagsName' => $tagsName]);
    }
}
