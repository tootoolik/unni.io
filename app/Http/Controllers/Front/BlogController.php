<?php

namespace App\Http\Controllers\Front;

use App\Front\Posts;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    public function index(Request $request)
    {
        $posts = Posts::with(['tags'])
            ->where('active', '=', '1')
            ->orderBy('id', 'desc')
            ->paginate(10);
        if (count($posts) === 0) {
            abort(404);
        }
        $title_page = $request->page ? ', страница ' . $request->page : '';
        if ($request->ajax())
            return view('Feeds.load', compact('posts'))->render();
        //todo надо вывести еще и теги

        return view('Blog.index', ['posts' => $posts, 'title_page' => $title_page]);
    }

    public function post(Request $request)
    {
        $post = Posts::where('id', '=', $request->id)
                        ->where('active', '=', '1')
                        ->with(['tags'])->first();

        if (!$post) {
            abort(404);
        } else {
            $title_page = $post->name;
        }
        return view('Blog.post', compact('post', 'title_page'));
    }
}
