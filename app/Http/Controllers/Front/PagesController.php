<?php

namespace App\Http\Controllers\Front;

use App\Model\Pages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    public function page($slug){
        $page = Pages::where('slug', $slug)->first();
        return view('Pages.page', compact('page'));
    }
}
