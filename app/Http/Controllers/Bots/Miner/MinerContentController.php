<?php

namespace App\Http\Controllers\Bots\Miner;

use App\Model\Bots\Auditor\AuditorDomains;
use App\Model\Bots\Auditor\AuditorUrls;
use App\Model\Bots\Crons\MinerCron;
use App\Model\Bots\Miner\MinerContent;
use App\Model\Tags;
use Curl\MultiCurl;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Traits\BustGetUrl;
use Illuminate\Support\Facades\DB;

use RollingCurl\RollingCurl;
use SplFileInfo;
use Symfony\Component\DomCrawler\Crawler;
use App\Traits\Tag\TagsTrait;

use Sunra\PhpSimple\HtmlDomParser;
class MinerContentController extends Controller
{
    use BustGetUrl, TagsTrait;
    const SLEEP = 3;

    public function parseMetaTagsNew(){

//           while (true){
               $allLinks = AuditorUrls::join('auditor_domain', 'auditor_url_children.id_domain', '=', 'auditor_domain.id')
                   ->select('auditor_url_children.url', 'auditor_url_children.id as url_id', 'auditor_domain.id',
                       'auditor_domain.domain')
                   ->where('auditor_domain.ping', '<>', '403')
                   ->where('auditor_domain.parser_content', '1')
                   ->where('auditor_url_children.parser_content', '0')
                   ->orderBy('auditor_url_children.id_domain', 'asc')
                   ->groupBy('auditor_url_children.id_domain')
                   ->get();
               $this->parseCron($allLinks);
//           }
    }

    public function circleParser($id){
        $allLinks = AuditorUrls::select('id_domain')
            ->where('id', $id)
            ->get();
        $allLinks = AuditorUrls::select('id', 'url')
            ->where('id_domain', $allLinks[0]->id_domain)
            ->where('parser_content', '0')
            ->whereNotIn('id', [$id])
            ->limit(1)
            ->get();
//                $this->metaAnalysis($allLinks);
        $multi_curl = new MultiCurl();

        $multi_curl->setReferer(true);
        $multi_curl->setTimeout(10);
        $multi_curl->setAutoReferrer(true);
        $multi_curl->setFollowLocation(true);
        $multi_curl->setUserAgent('Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0');

        $multi_curl->success(function($instance) {
            $this->metaAnalysis($instance);
        });
        $multi_curl->error(function($instance) {
//            $fixedLink = AuditorUrls::find($instance->id);
//            $fixedLink->http_code = $instance->httpStatusCode;
//            $fixedLink->save();
        });
        $multi_curl->complete(function($instance) {
//            echo 'call completed' . "\n";
        });
//        dump($allLinks[0]->url);
        $multi_curl->addGet($allLinks[0]->url, $allLinks[0]->id);
        $multi_curl->start();
    }
    /**
     * @param $allLinks
     */
    public function parseCron($allLinks){
        // Requests in parallel with callback functions.
        $multi_curl = new MultiCurl();

        $multi_curl->setReferer(true);
        $multi_curl->setTimeout(10);
        $multi_curl->setAutoReferrer(true);
        $multi_curl->setFollowLocation(true);
        $multi_curl->setConcurrency(1000);
        $multi_curl->setUserAgent('Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0');

        $multi_curl->success(function($instance) {
//            echo 'success';
            $this->metaAnalysis($instance);
//            if (count($instance) > 0){
//                while (true){
//                    $this->circleParser($instance->id);
//                }
//            }
        });
        $multi_curl->error(function($instance) {
//            echo 'error';
//            dump($instance->id);
            $fixedLink = AuditorUrls::find($instance->id);
            $fixedLink->http_code = $instance->httpStatusCode;
            $fixedLink->parser_content = '1';
            $fixedLink->save();
        });
        $multi_curl->complete(function($instance) {
//            echo 'complete';
//            echo 'call completed' . "\n";
        });

        foreach ($allLinks as $allLink) {
            if (strpos($allLink->url, $allLink->domain) == false){
                $allLink->url = 'http://'.$allLink->domain.$allLink->url;
            }else{
                $allLink->url;
            }
//            $allLink->url = str_replace(" ","",$allLink->url);
//            dump($allLink->url);
            $multi_curl->addGet($allLink->url, $allLink->url_id);
//            $this->circleParser($allLink->url_id);
        }

        $multi_curl->start(); // Blocks until all items in the queue have been processed.
    }




    /**
     * @param $instance
     * Получаем массив с контентом и начинаем разбор
     */
    public function metaAnalysis($instance){
        if (!empty($instance->response)) {
            if (strpos($instance->responseHeaders['content-type'], 'text/html') !== false) {
                $crawler = new Crawler(null, $instance->url);
                $crawler->addHtmlContent($instance->response, 'UTF-8');
                if ($crawler->filter('title')->count() > 0) {
                    $title = $crawler->filter('title')->text();
                    $meta = $crawler->filterXpath('//meta[@name="description"]')->each(function ($node) {
                        return [
                            'content' => $node->attr('content'),
                        ];
                    });
                    if (empty($meta)) {
                        $meta = $crawler->filterXpath('//meta[@name="description"]')->each(function ($node) {
                            return [
                                'content' => $node->attr('content'),
                            ];
                        });
                    }
                    $metaDescription = array();
                    foreach ($meta as $subArr) {
                        foreach ($subArr as $key => $val) {
                            if (isset($metaDescription[$key]) && $metaDescription[$key] > $val) continue;
                            $metaDescription[$key] = $val;
                        }
                    }
                    $metaDescription = implode('', $metaDescription);
                    MinerContent::updateOrCreate([
                        'title' => trim($title),
                        'description' => trim($metaDescription),
                        'id_url' => $instance->id
                    ]);


                    $metaKeywords = $crawler->filterXpath('//meta[@name="keywords"]')->each(function ($node) {
                        return [
                            'content' => $node->attr('content'),
                        ];
                    });
                    if ($metaKeywords){
                        $metaKeywords = explode(',', $metaKeywords[0]['content']);
                        foreach ($metaKeywords as $metaKeyword) {
                            $kw = trim($metaKeyword);
                            $tag = Tags::where('tag',$kw)->first();
                            if(!$tag){
                                Tags::Create([
                                    'tag' => $metaKeyword,
                                    'slug' => $this->generateSlug($kw),
                                    'url_id' => $instance->id,
                                    'type' => 'parser',
                                ]);
                            }
                        }
                    }
                    $fixDomain = AuditorUrls::select('id_domain')->where('id', $instance->id)->first();

                    $count = DB::table('miner_content_count')
                        ->select('count_content', 'id_domains')
                        ->where('id_domains', $fixDomain->id_domain)
                        ->first();
                    if (isset($count->id_domains)){
                        ++$count->count_content;
                        DB::table('miner_content_count')
                            ->where('id_domains', $count->id_domains)
                            ->update(['count_content' => $count->count_content]);
                    }else{
                        $count = 1;
                        DB::table('miner_content_count')->insert(
                            ['count_content' => $count, 'id_domains' => $fixDomain->id_domains]
                        );
                    }


                    $fixedLink = AuditorUrls::find($instance->id);
                    $fixedLink->parser_content = '1';
                    $fixedLink->save();
                }
            }else{
                AuditorUrls::destroy($instance->id);
            }
        }
    }

    public function metaReceivingOLD(){
        return AuditorUrls::join('auditor_domain', 'auditor_url_children.id_domain', '=', 'auditor_domain.id')
            ->select('auditor_url_children.url', 'auditor_url_children.id as url_id', 'auditor_domain.id',
                'auditor_domain.domain')
            ->where('auditor_domain.ping', '<>', '403')
            ->where('auditor_url_children.parser_content', '0')
            ->orderBy('auditor_url_children.id_domain', 'asc')
            ->groupBy('auditor_url_children.id_domain')
            ->get();
    }

    public function parseMetaTagsNewOLD(){
            $allLinks = $this->metaReceiving();
            // Requests in parallel with callback functions.
            $multi_curl = new MultiCurl();
//            $multi_curl->setConcurrency(10);
            $multi_curl->setReferer(true);
            $multi_curl->setTimeout(10);
            $multi_curl->setAutoReferrer(true);
            $multi_curl->setFollowLocation(true);
            $multi_curl->setUserAgent('Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0');
        $multi_curl->success(function($instance) use (&$success){
//                echo 'call to "' . $instance->url . '" was successful.' . "<br>";
//                echo 'response:' . "\n";
//                dump($instance);
                $this->metaAnalysis($instance);

            });
            $multi_curl->error(function($instance) use (&$error) {
//                echo 'call to "' . $instance->url . '" was unsuccessful.' . "<br>";
//                echo 'error code: ' . $instance->errorCode . "<br>";
//                echo 'error message: ' . $instance->errorMessage . "<br>";
                $fixedLink = AuditorUrls::find($instance->id);
                $fixedLink->http_code = $instance->httpStatusCode;
                $fixedLink->save();
            });

            $multi_curl->complete(function($instance) use (&$complete) {
//                echo 'call completed' . "<br>";
//                dump($instance->url);
//                dump($instance->httpStatusCode);
            });
            foreach ($allLinks as $allLink) {
                if (strpos($allLink->url, $allLink->domain) == false){
                    $allLink->url = 'http://'.$allLink->domain.$allLink->url;
                }else{
                    $allLink->url;
                }
//                dump($allLink->url);
                $multi_curl->addGet($allLink->url, $allLink->url_id);
            }
        // Blocks until all items in the queue have been processed.
        $multi_curl->start();
    }






    //OLD CODE
    public function parseMetaTags(){
        /**
         * We get the status of a cron, and if 0, go cron
         */
        $startCron = MinerCron::orderBy('id', 'desc')->first();

        if (!isset($startCron->level) || $startCron->level == 0) {
            $cron = new MinerCron();
            $cron->level = '1';
            $cron->save();

//            $noUrl= MinerContent::latest('id_url')->select('id_url')->get()->chunk(1000);
//
            $allLinks = AuditorUrls::join('auditor_domain', 'auditor_url_children.id_domain', '=', 'auditor_domain.id')
                ->select('auditor_url_children.url', 'auditor_url_children.id as url_id', 'auditor_domain.id',
                    'auditor_domain.domain')
//                ->where('auditor_domain.ping', '<>', '403')
                ->where('auditor_url_children.parser_content', '0')
                ->orderBy('auditor_url_children.id_domain', 'asc')
                ->groupBy('auditor_url_children.id_domain')
                ->get();
//            dump($allLinks);
//            $allLinks = 0;
            foreach ($allLinks as $link) {
                $fixedLink = AuditorUrls::find($link->url_id);
                $fixedLink->parser_content = '1';
                $fixedLink->save();
                    if (strpos($link->url, $link->domain) !== false) {
                        if (strpos($link->url, $link->domain) !== false) {
                            $url = $link->url;
                            $domain = $link->id_domain;
                        }
                    } else {
                        $url = $link->domain.'' . $link->url;
                        $domain = $link->id_domain;
                    }
//                    dump($link->url_id);
                $html = $this->contentGet(trim($url));
//                    dump($html);
                    if ($html != false && $html != '403') {
//                        time_sleep_until(microtime(true) + self::SLEEP);
                        if (strripos($url, 'http')){
                            $crawler = new Crawler(null, $url);
                            $crawler->addHtmlContent($html, 'UTF-8');
                        }else{
                            $crawler = new Crawler(null, 'http://'.$url);
                            $crawler->addHtmlContent($html, 'UTF-8');
                        }
                        if ($crawler->filter('title')->count() > 0) {
                            $title = $crawler->filter('title')->text();
                            $meta = $crawler->filterXpath('//meta[@name="description"]')->each(function ($node) {
                                return [
                                    'content' => $node->attr('content'),
                                ];
                            });
                            if (empty($meta)){
                                $meta = $crawler->filterXpath('//meta[@name="description"]')->each(function ($node) {
                                    return [
                                        'content' => $node->attr('content'),
                                    ];
                                });
                            }
                            $metaDescription = array();
                            foreach ($meta as $subArr) {
                                foreach ($subArr as $key => $val) {
                                    if (isset($metaDescription[$key]) && $metaDescription[$key] > $val) continue;
                                    $metaDescription[$key] = $val;
                                }
                            }
                            $metaDescription = implode('', $metaDescription);



                            $metaKeywords = $crawler->filterXpath('//meta[@name="keywords"]')->each(function ($node) {
                                return [
                                    'content' => $node->attr('content'),
                                ];
                            });
                            if ($metaKeywords){
                                $metaKeywords = explode(',', $metaKeywords[0]['content']);
                                foreach ($metaKeywords as $metaKeyword) {
                                    $kw = trim($metaKeyword);
                                    $tag = Tags::where('tag',$kw)->first();
                                    if(!$tag){
                                        Tags::Create([
                                            'tag' => $metaKeyword,
                                            'slug' => $this->generateSlug($kw),
                                            'type' => 'parser',
                                        ]);
                                    }

                                }
                            }
                            $cronStart = MinerCron::orderby('id', 'desc')->first();

                            MinerContent::updateOrCreate([
                                    'id_url' => $link->url_id,
                                    'title' => $title,
                                    'description' => $metaDescription,
                                    'cron_start' => $cronStart
                            ]);
                        }
                    } elseif ($html == '403'){
//                        dump($link->url_id);
//                        dd($html);
                    }else{
                            AuditorUrls::destroy($link->url_id);
                    }

            }
            $cron->level = '0';
            $cron->save();
        }
    }
}
