<?php

namespace App\Http\Controllers\Bots\Auditor;

use App\Components\ParserLogger\ParserLogger;
use App\Components\ParserLogger\ParserLoggerFacade;
use App\Facades\Analyzer;
use App\Front\Domains;
use App\Http\Controllers\Controller;
use App\Model\Bots\Auditor\AuditorContent;
use App\Model\Bots\Auditor\AuditorDomains;
use App\Model\Bots\Auditor\AuditorErrorUrls;
use App\Model\Bots\Auditor\AuditorExceptionUrl;
use App\Model\Bots\Auditor\AuditorUrls;
use App\Model\Bots\Auditor\CountParser;
use App\Traits\BustGetUrl;
use App\Traits\HtmlDomParsing;
use Curl\MultiCurl;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;

class AuditorUrlsController extends Controller
{
    use HtmlDomParsing;
    use BustGetUrl;

    const LIMIT_URLS_TO_TMP_TABLE = 200;
    const LIMIT_URLS_FROM_TMP_TABLE = 200;
    const LIMIT_DOMAINS = 5 ;
    const LIMIT_SYMBOL_OF_TEXT = 1000000;

    private $multiCurl;

    // Logger data parse()
    private $internalPageCount = 0;
    private $homePageCount = 0;
    private $analysisLinksData = '<span>---- MultiCurl не был вызван ----</span><br>';
    private $countAllLinks = 0;

    // Logger data for urlAnalysisLinks()
    private $analysisLinksLoggerId = '';

    // Logger data for tableTmp()
    private $tableTmpLoggerId = '';
    private $countListDomains = 0;
    private $countListDomainsWriteToTmpTable = 0;
    private $countCreatedTables = 0;
    private $countDropTables = 0;
    private $countUpdateTables = 0;

    // Logger data for startParseUrls()
    private $startParseUrlsLoggerId = '';
    private $parseCronLoggerData = false;

    /**
     * Запускает парсинг "главных" доменов.
     *
     * @return void
     */
    public function parseDomainHomepage() :void
    {
        $allDomains = AuditorDomains::where('parser_content', 1)
                                    ->where('ping', '!=', 403)
                                    ->orderBy('count', 'asc')
                                    ->limit(self::LIMIT_DOMAINS)
                                    ->get();

        $this->parse($allDomains, 1);
    }

    /**
     * Основной парсер. Запускает всё необходимое для парсинга ссылок.
     * HTML записывается в БД (auditor_content)
     *
     * @param $domains
     * @param null $home
     * @return string
     */
    protected function parse($domains, $home = null) :string
    {
        $this->initializeMultiCurl();

        // Запросы отправляются параллельно с коллбэками.
        // Обрабатываем результат работы метода runMultiCurlForDomains()
        $this->multiCurlParseSuccess();
        $this->multiCurlParseError();

        $this->runMultiCurlForDomains($domains, $home);

        // Блокирует до тех пор, пока все элементы в очереди не будут обработаны.
        if(isset($this->multiCurl)) $this->multiCurl->start();

        return $this->parseLogging($domains);
    }

    /**
     * Запускаем MultiCurl для выбранных доменов.
     *
     * Результат выполнения метода addGet() обрабатывается
     * в методах $this->multiCurlParseSuccess() или $this->multiCurlParseError()
     *
     * @param $domains
     * @param $home
     *
     * @return void
     */
    private function runMultiCurlForDomains($domains, $home) :void
    {
        try
        {
            if ($home === null && isset($domains[0]))
            {
                // Достаем все ссылки с меткой disallow из robots.txt
                // чтобы исключить их из парсинга
                $domainId = $domains[0]->id_domain;
                $disallow = $this->getDisallowLinksFromRobotsTxt($domainId);
            }

            foreach ($domains as $domain)
            {
                ++$this->countAllLinks;
                if ($home === null)
                {
                    $parsedAllLink = parse_url($domain->url);

                    if($path = $parsedAllLink['path'] ?? null)
                        if(in_array($path, $disallow)) continue;

                    $mk = $this->multiCurl->addGet($domain->url);
                    $mk->url_id = $domain->url_id;
                    $mk->urlsss = $domain->urlsss;
                    $mk->id_domain = $domain->id_domain;
                }
                else
                {
                    $domain->url = 'http://' . $domain->domain;
                    $domain->url_id = 'home';
                    $mk = $this->multiCurl->addGet($domain->url);
                    $mk->url_id = $domain->url_id;
                    $mk->urlsss = $domain->urlsss;
                    $mk->id_domain = $domain->id;
                }
            }
        }
        catch(\Exception $e)
        {
            unset($this->multiCurl);
            Log::debug($e->getMessage());
            Log::debug($e->getLine());
            Log::debug('MultiCurl упал при вызове метода addGet() в цикле');

        }
    }

    /**
     * Попадаем сюда в случае успеха выполнения функции addGet() у multiCurl.
     *
     * @return void
     */
    private function multiCurlParseSuccess() :void
    {
        //todo Обновить тайт и описанине для канала из тайтла и дескрипшена страницы
        try
        {
            $this->multiCurl->success(function ($instance) {
                if ($instance->url_id !== 'home')
                {
                    ++$this->internalPageCount;
                    $instance->httpPort = $instance->getInfo()['primary_port'];
                    $this->analysisLinksData = $this->urlAnalysisLinks($instance);
                    DB::connection('mysqlTmpUrl')->table($instance->urlsss)->where('url_id', $instance->url_id)->delete();
                }
                else
                {
                    ++$this->homePageCount;
                    $instance->httpPort = $instance->getInfo()['primary_port'];
                    $this->analysisLinksData = $this->urlAnalysisLinks($instance);
                }

                unset($instance);
            });
        }
        catch (\Exception $e)
        {
            unset($this->multiCurl);
            Log::debug($e->getMessage());
            Log::debug('MultiCurl упал при success');
        }
    }

    /**
     * Попадаем сюда в случае провала выполнения функции addGet() у multiCurl.
     *
     * @return void
     */
    private function multiCurlParseError() :void
    {
        try
        {
            $this->multiCurl->error(function ($instance)
            {
                if ($instance->url_id !== 'home')
                {
                    AuditorErrorUrls::create(['url_id' => $instance->url_id,
                        'id_domain' => $instance->id_domain,
                        'error_code' => $instance->errorCode,
                        'message' => $instance->errorMessage
                    ]);

                    DB::connection('mysqlTmpUrl')
                        ->table($instance->urlsss)
                        ->where('url_id', $instance->url_id)
                        ->delete();

                    AuditorUrls::where('id', $instance->url_id)->update(['parser' => 1]);
                }
                else
                {
                    AuditorDomains::where('id', $instance->id_domain)->update(['ping' => $instance->httpStatusCode]);

                    AuditorErrorUrls::create([
                        'url_id' => $instance->url_id,
                        'id_domain' => $instance->id_domain,
                        'error_code' => $instance->errorCode,
                        'message' => $instance->errorMessage
                    ]);
                }

                $this->analysisLinksData = '<hr><strong>MultiCurl Error: </strong></br>message: ' . $instance->errorMessage . '<hr>';

                unset($instance);
            });
        }
        catch (\Exception $e)
        {
            unset($this->multiCurl);
            Log::debug($e->getMessage());
            Log::debug('MultiCurl упал при error');
        }
    }

    /**
     * Инициализурем асинхронную обработку множества cURL-дескрипторов.
     *
     * @return void
     */
    private function initializeMultiCurl() :void
    {
        try
        {
            $multiCurl = new MultiCurl();
            $multiCurl->setReferer(true);
            $multiCurl->setTimeout(30);
            $multiCurl->setOpt(CURLINFO_SSL_ENGINES, 1);
            $multiCurl->setConnectTimeout(30);
            $multiCurl->setOpt(CURLOPT_FOLLOWLOCATION, true);
            $multiCurl->setOpt(CURLOPT_ENCODING, '');
            $multiCurl->setOpt(CURLOPT_SSL_VERIFYPEER, false);
            $multiCurl->setHeader('Content-Type', 'text/html');
            $multiCurl->setUserAgent('Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0');

            $this->multiCurl = $multiCurl;
        }
        catch (\Exception $e)
        {
            Log::debug($e->getMessage());
            Log::debug('MultiCurl упал при установке параметров');
        }
    }

    /**
     * Выбираем все ссылки с пометкой disallow из robots.txt
     * Кэшируем результат выполенения на 1 мес.
     *
     * @param $domainId
     * @return array
     */
    private function getDisallowLinksFromRobotsTxt($domainId) :array
    {
        $domain = AuditorDomains::find($domainId);

        $keyName = 'domain_robot_txt.' . $domain->id;
        $lifeTime = 60*24*30; // 30 days

        return Cache::remember($keyName, $lifeTime, function() use ($domain) {
            return Analyzer::robotTxt()->getDisallow($domain->domain);
        });
    }

    /**
     * Анализируем URL.
     * Результат пишем в auditor_content для дальнейшего парсинга HTML.
     *
     * @param $instance
     * @return string
     */
    protected function urlAnalysisLinks($instance) :string
    {
        $domain = Domains::where('id', $instance->id_domain)->first();

        if (!empty($instance->rawResponse) && $domain->domain == str_replace('www.', '', $instance->requestHeaders['host']))
        {
            $this->analysisLinksLoggerId = ParserLoggerFacade::startProcess('Анализ контента');

            $text = $this->normalizeText($instance);

            if($instance->url_id !== 'home' && strlen($text) > self::LIMIT_SYMBOL_OF_TEXT)
                return $this->makeAuditorException($instance);

            $instance = $this->domainIncrement($instance);

            // TODO сделать проверку существует ли контент
            $content = $this->createAuditorContent($instance, $text, $domain);

            //Плюсуем в счетчик контент
            $this->countsLinks($content->wasRecentlyCreated, $instance->id_domain, $type = 'count_content');

            return $this->urlAnalysisLinksLogging($text, $domain, $instance, $content);
        }

        return '<br>Анализ ссылок: не прошли условия для анализа<br>';
    }

    /**
     * Добавляем HTML для дальнейшего разбора в таблицу auditor_content
     *
     * @param $instance
     * @param $text
     * @param $domain
     * @return mixed
     */
    private function createAuditorContent($instance, $text, $domain)
    {
        $idProcess = ParserLoggerFacade::startProcess('Запись контента в базу');

        try
        {
            $content =  AuditorContent::create([
                    'url_id' => $instance->url_id,
                    'content' => $text,
                    'domain_id' => $instance->id_domain,
                ]);

            return $content;
        }
        catch (\Exception $e)
        {
            Log::debug('Рухнул при записи в auditor_content');
            Storage::delete('domains/' . $domain->domain);
        }

        ParserLoggerFacade::stopProcess($idProcess);
    }

    /**
     * Меняем ответ сервера у домена и увеличиваем счетчик.
     *
     * @param $instance
     * @return mixed
     */
    private function domainIncrement($instance)
    {
        if ($instance->url_id == 'home')
        {
            AuditorDomains::where('id', $instance->id_domain)->update([
                'httpPort' => $instance->httpPort,
                'ping' => $instance->httpStatusCode
            ]);

            AuditorDomains::where('id', $instance->id_domain)->increment('count');
            $instance->url_id = 0;
        }

        return $instance;
    }

    /**
     * Создаем исключение для нежелательных URL
     *
     * @param $instance
     * @return string
     */
    private function makeAuditorException($instance) :string
    {
        AuditorExceptionUrl::firstOrCreate(['url' => $instance->url]);

        $analysisContentData = '<br>Контент привысил миллион символов<br>';

        ParserLoggerFacade::stopProcess($this->analysisLinksLoggerId, $analysisContentData);

        return $analysisContentData;
    }

    /**
     * Приводим к UTF-8
     * Вырезаем куски кода.
     *
     * @param $instance
     * @return string
     */
    private function normalizeText($instance) :string
    {
        $text = $instance->rawResponse;

        if ($instance->responseHeaders['content-type'] == 'text/html; charset=windows-1251')
            $text = @iconv('CP1251', 'UTF-8', $text);

        $text = preg_replace('/<script.*?>.*?\/script>|<svg.*?\/svg>|<!--.*?-->|<style.*?>.*?\/style>/mis', '', $text);

        return preg_replace('/\s{2,}/', ' ', $text);
    }

    /**
     * Одновременный парсинг урлов доменов из временных таблиц.
     * Для каждого домена запустим отдельный процесс.
     *
     * @return void
     */
    public function parseUrlsOfDomain() :void
    {
        $mainParser = DB::table('main_parser')->orderBy('id', 'desc')->first();

        if ($mainParser->level == 0)
        {
            // Собираем список доменов из временных таблиц
            $listDomains = DB::connection('mysqlTmpUrl')->select('SHOW TABLES');

            foreach ($listDomains as $domain)
            {
                // Необходимо зафиксировать, что процесс для каждого домена запущен,
                // для этого создаем временный файл на диске
                if (!Storage::disk('local')->exists('domains/' . $domain->Tables_in_unni_tmp_url))
                {
                    Storage::put('domains/' . $domain->Tables_in_unni_tmp_url, '1');

                    // По идее будет ожидать конца процесса, я почти в этом уверен
                    exec('sudo curl https://unni.io/startparsurls/' . $domain->Tables_in_unni_tmp_url . '?domain=' . $domain->Tables_in_unni_tmp_url. ' > /dev/null &');
                }
            }
        }
    }

    /**
     * Парсинг URL
     *
     * @param Request $request
     */
    protected function startParseUrls(Request $request) :void
    {
        $this->startParseUrlsBeginLogging();

        //Пустые таблицы удаляются в другом процессе(method parse_cron()), проверим если таблица в базе
        if (Schema::connection('mysqlTmpUrl')->hasTable($request->domain))
        {
            //Выберем все ссылки по каждой таблице для каждого домена
            $linksDomain = DB::connection('mysqlTmpUrl')
                ->table($request->domain)
                ->orderByDesc('id')
                ->limit(self::LIMIT_URLS_TO_TMP_TABLE)
                ->get()->reverse();

            //Отдаем curl все ссылки в виде массива для каждого домена
            $this->parseCronLoggerData = $this->parse($linksDomain);
        }

        //Цикл для парсинга в указанные лимиты прошел, удалим временный файл, для повторного цикла
        Storage::delete('domains/' . $request->domain);

        $this->startParseUrlsEndLogging($request);
    }

    /**
     * Регистрируем кастомный лог для startParseUrls()
     *
     * @return void
     */
    private function startParseUrlsBeginLogging() :void
    {
        app()->singleton('parser_logger', function () {
            return new ParserLogger('start_parser_urls');
        });
        $this->startParseUrlsLoggerId = ParserLoggerFacade::startProcess('Парсим ссылки домена из временнных таблиц');
        $this->parseCronLoggerData = false;
    }

    /**
     * Пишем в кастомный лог результат работы startParseUrls()
     *
     * @param $request
     *
     * @return void
     */
    private function startParseUrlsEndLogging($request) :void
    {
        $startParseloggerData = '<strong>Имя домена:</strong> ' . $request->domain . '<br>';
        $loggerData = $startParseloggerData . $this->parseCronLoggerData;
        ParserLoggerFacade::stopProcess($this->startParseUrlsLoggerId, $loggerData);
    }

    /**
     * Записываем URL во временные таблицы доменов для дальнейшего парсинга
     *
     * @return void
     */
    public function tableTmp() :void
    {
        $mainParser = DB::table('main_parser')->orderBy('id', 'desc')->first();

        if ($mainParser->level === '0')
        {
            $this->tableTmpLoggerId = ParserLoggerFacade::startProcess('Анализ контента');

            $listDomains = AuditorDomains::where('parser_content', 1)->orderBy('priority', 'desc')->get();

            $this->workWithTmpTables($listDomains);

            $this->tableTmpLogging($listDomains);
        }
    }

    /**
     * Работа с временными таблицами.
     * Создаем/обновляем/удаляем
     *
     * @param $listDomains
     *
     * @return void
     */
    private function workWithTmpTables($listDomains) :void
    {
        $exceptions = AuditorExceptionUrl::all();

        foreach ($listDomains as $domain)
        {
            $listUrls = $this->getAllLinksOfDomain($domain, $exceptions);

            $domainName = $this->getDomainNameForTmpTable($domain);

            $domainTable = $this->checkTmpTable($domainName);

            if (!$domainTable && $listUrls->count() > 0)
            {
                ++$this->countCreatedTables;
                $this->createTmpTable($domainName,$listUrls);
            }
            elseif ($domainTable && DB::connection('mysqlTmpUrl')->table($domainName)->count() === 0)
            {
                ++$this->countDropTables;
                Schema::connection('mysqlTmpUrl')->dropIfExists($domainName);
            }
            elseif ($domainTable)
            {
                ++$this->countUpdateTables;
                $this->writeToTmpTable($domainName, $listUrls);
            }
        }
    }

    /**
     * Проверяем на существование таблицы с именем домена
     *
     * @param $name
     *
     * @return bool
     */
    private function checkTmpTable(string $name) :bool
    {
        return Schema::connection('mysqlTmpUrl')->hasTable($name);
    }

    /**
     * Подготвливаем доменное имя для работы с временными таблицами
     *
     * @param $domain
     *
     * @return string
     */
    private function getDomainNameForTmpTable($domain) :string
    {
        return str_replace([".", ",", ":", "_", "-"], "", $domain->domain);
    }

    /**
     * Отдает не спарсенные ссылки домена
     *
     * @param $domain
     * @param $exceptions
     *
     * @return mixed
     */
    private function getAllLinksOfDomain($domain, $exceptions)
    {
        $listUrls = AuditorUrls::select('id', 'url', 'parser', 'id_domain')
                               ->where('id_domain', $domain->id)
                               ->where('parser', 0)
                               ->limit(self::LIMIT_URLS_FROM_TMP_TABLE)
                               ->get();

        $this->countListDomains += $listUrls->count();

        return $listUrls->whereNotIn('url', $exceptions);
    }

    /**
     * Записываем сслыки во временную таблицу.
     *
     * @param $domainName
     * @param $listUrls
     *
     * @return void
     */
    private function writeToTmpTable($domainName, $listUrls) :void
    {
        foreach ($listUrls as $listUrl)
        {
            ++$this->countListDomainsWriteToTmpTable;

            $sql = 'insert into ' . $domainName . '(url_id, url, urlsss, id_domain) values (?, ?, ?, ?)';
            DB::connection('mysqlTmpUrl')->insert($sql, [$listUrl->id, $listUrl->url, $domainName, $listUrl->id_domain]);

            AuditorUrls::where('id', $listUrl->id)->update(['parser' => 1]);
        }
    }

    /**
     * Создаем временную таблицу для домена.
     *
     * @param $domainName
     * @param $listUrls
     *
     * @return void
     */
    private function createTmpTable($domainName, $listUrls) :void
    {
        Schema::connection('mysqlTmpUrl')->create($domainName, function ($table) {
            $table->increments('id');
            $table->integer('url_id');
            $table->integer('geted');
            $table->string('url', 255);
            $table->string('urlsss', 255);
            $table->string('id_domain', 255);
        });

        foreach ($listUrls as $listUrl)
        {
            ++$this->countListDomainsWriteToTmpTable;

            DB::connection('mysqlTmpUrl')->insert('insert into ' . $domainName .
                '(url_id, url, urlsss, id_domain) values (?, ?, ?, ?)',
                [$listUrl->id, $listUrl->url, $domainName, $listUrl->id_domain]);

            AuditorUrls::where('id', $listUrl->id)->update(['parser' => 1]);
        }
    }

    /**
     * Пишем в кастомный лог результат работы tmpTable()
     *
     * @param $listDomains
     */
    private function tableTmpLogging($listDomains) :void
    {
        if ($this->countListDomains > 0)
        {
            $loggerContent = '<strong>Кол-во выбранных доменов:</strong> ' . $listDomains->count() . '<br>' .
                '<strong>Общее кол-во URL у выбранных доменов:</strong> ' . $this->countListDomains . '<br>' .
                '<strong>Общее кол-во URL записанных во временные таблицы:</strong> ' . $this->countListDomainsWriteToTmpTable . '<br>' .
                '<strong>Общее кол-во созданых временных таблиц:</strong> ' . $this->countCreatedTables . '<br>' .
                '<strong>Общее кол-во удаленных временных таблиц:</strong> ' . $this->countDropTables . '<br>' .
                '<strong>Общее кол-во обновленных временных таблиц:</strong> ' . $this->countUpdateTables . '<br>';

            ParserLoggerFacade::stopProcess($this->tableTmpLoggerId, $loggerContent);
        }
        else
        {
            ParserLoggerFacade::offAndRemove();
        }
    }

    /**
     * Пишем в кастомный лог результат работы urlAnalysisLinks()
     *
     * @param $text
     * @param $domain
     * @param $instance
     * @param $content
     *
     * @return string
     */
    private function urlAnalysisLinksLogging($text, $domain, $instance, $content) :string
    {
        $checkContent = (strlen($text) > 0) ? 'Да' : 'Нет';
        $analysisContentData = '<strong>Имя домена:</strong> ' . $domain->domain . '<br>' .
            '<strong>Наличие контента:</strong> ' . $checkContent . '<br>' .
            '<hr>' .
            '<strong>Кол-во символов в контенте: </strong> ' . strlen($content->content) . '<br>' .
            '<hr>' .
            '<strong>url_id:</strong> ' . $instance->url_id . '<br>' .
            '<strong>domain_id:</strong> ' . $instance->id_domain . '<br>' .
            '<strong>ping:</strong> ' . $instance->httpStatusCode . '<br>';

        ParserLoggerFacade::stopProcess($this->analysisLinksLoggerId, $analysisContentData);

        return $analysisContentData;
    }

    /**
     * Пишем в кастомный лог выполенения parse()
     *
     * @param $domains
     * @return string
     */
    private function parseLogging($domains) :string
    {
        $loggerData = '<strong>Кол-во "Главных" страниц:</strong> ' . $this->homePageCount . '<br>' .
            '<strong>Кол-во "Внутренних" страниц:</strong> ' . $this->internalPageCount . '<br>';

        $loggerDataCountLinks = '<strong>Кол-во ссылок для парсинга: </strong> ' . count($domains) . '<br>' .
            '<strong>Кол-во ссылок поппавшие в цикл: </strong> ' . $this->countAllLinks . '<br>';

        return $loggerData . $this->analysisLinksData . $loggerDataCountLinks ;
    }

    /**
     * Основной счетчик
     *
     * TODO дублируется с @see JewelerController::countsLinks(), убрать дубликат
     *
     * @param $wasRecentlyCreated
     * @param $domainId
     * @param $type
     *
     * @return void
     */
    protected function countsLinks($wasRecentlyCreated, $domainId, $type) :void
    {
        if ($wasRecentlyCreated == true)
        {
            $count = CountParser::where('id_domain', $domainId)->increment($type);

            if ($count == 0)
                CountParser::create(['id_domain' => $domainId, $type => 1]);

        }
    }
}
