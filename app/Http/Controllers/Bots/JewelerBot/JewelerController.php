<?php

namespace App\Http\Controllers\Bots\JewelerBot;

use App\Components\ParserLogger\ParserLoggerFacade;
use App\Http\Controllers\Controller;
use App\Model\Bots\Auditor\AuditorContent;
use App\Model\Bots\Auditor\AuditorDomains;
use App\Model\Bots\Auditor\AuditorUrls;
use App\Model\Bots\Auditor\CountParser;
use App\Model\Bots\Jeweler\JewelerContent;
use App\Model\Fornt\Feed;
use App\Model\Fornt\TagSection;
use App\Model\Tags;
use App\Model\TagsDomain;
use App\Model\TagsUrls;
use App\Traits\BustGetUrl;
use App\Traits\HtmlDomParsing;
use App\Traits\Tag\TagsTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class JewelerController extends Controller
{
    const ANALYSIS_LINK_LIMIT = 700;
    const ANALYSIS_CONTENT_LIMIT = 700;
    const ANALYSIS_TAG_LIMIT = 600;

    public $urls = [];

    // Logger data for getCreatedPage()
    private $checkDateFromOG = 0;
    private $checkDateFromSchema = 0;
    private $checkDateFromDB = 0;

    // Logger data for analysisLinks()
    private $analysisLinksLoggerId = '';
    private $secondsStoreLink = 0;
    protected $isSitemap = false;
    protected $analysisContentCount = 0;
    protected $allLinksCount = 0;
    protected $allLinksNewsCount = 0;
    protected $allLinksApprovedCount = 0;

    // Logger data for analysisContent()
    private $analysisContentLoggerId = 0;
    private $countWriteContent = 0;

    // Logger data for analysisTags()
    private $analysisTagsLoggerId = '';
    private $keywordsCount = 0;
    private $tagsCountIterator = 0;

    use HtmlDomParsing, BustGetUrl, TagsTrait;

    /**
     * Анализируем ссылки и записываем полезные в базу.
     *
     * @return void
     */
    public function analysisLinks() :void
    {
        $this->analysisLinksLoggerId = ParserLoggerFacade::startProcess('Собираем и анализируем ссылки');

        $contents = AuditorContent::where('analysis_link', 0)->limit(self::ANALYSIS_LINK_LIMIT)->get();

        foreach ($contents as $content)
        {
            $domain = AuditorDomains::where('id', $content->domain_id)->first();

            if ($domain->httpPort !== '' || $domain->httpPort == '403')
            {
                ++$this->analysisContentCount;
                $links = $this->getLinks($content->content);

                if ($links)
                {
                    $start = time();
                    foreach ($links as $link => $anchor)
                    {
                        ++$this->allLinksCount;
                        if (!empty($link))
                        {
                            $relate_domain_links = $this->relateDomainUrl($domain->domain, $link, $domain->httpPort, $domain->level);
                            if ($relate_domain_links)
                                $this->storeLink($relate_domain_links, $domain);
                        }
                    }

                    AuditorUrls::insert($this->urls);
                    $this->urls = [];

                    $end = time();
                    $this->secondsStoreLink += $end - $start;
                }

                AuditorContent::where('id', $content->id)->update(['analysis_link' => 1]);
            }
            else
            {
                AuditorContent::where('url_id', $content->url_id)->delete();
            }
        }

        $this->analysisLinksLogger($contents);
    }

    /**
     * Пишем в кастомный лог результат работы analysisLinks()
     *
     * @param $contents
     *
     * @return void
     */
    private function analysisLinksLogger($contents) :void
    {
        $sitemapLogStr = $this->isSitemap ? ' (Sitemap) ' : '';
        $loggerData = '<strong>Кол-во выбранного контента' . $sitemapLogStr . ': </strong> ' . $contents->count() . '<br>' .
            '<strong>Кол-во анализируемого контента' . $sitemapLogStr . ': </strong> ' . $this->analysisContentCount . '<br>' .
            '<strong>Кол-во ссылок' . $sitemapLogStr . ': </strong> ' . $this->allLinksCount . '<br>' .
            '<strong>$this->storeLink()' . $sitemapLogStr . ': </strong> ' . $this->secondsStoreLink . 'сек<br>' .
            '<strong>Кол-во ссылок для анализа' . $sitemapLogStr . ': </strong> ' . $this->allLinksApprovedCount . '<br>' .
            '<strong>Кол-во ссылок cо статусом "news"' . $sitemapLogStr . ': </strong> ' . $this->allLinksNewsCount . '<br>';

        ParserLoggerFacade::stopProcess($this->analysisLinksLoggerId, $loggerData);
    }

    /**
     * Сохраняем ссылку в auditor_url_children для дальнейшего парсинга
     *
     * @param $link
     * @param $domain
     *
     * @return void
     */
    public function storeLink($link, $domain) :void
    {
        ++$this->allLinksApprovedCount;

        if(!AuditorUrls::where('url', mb_strtolower($link))->exists())
        {
            $this->urls[] = [
                'url' => $link,
                'id_domain' => $domain->id,
                'status_news' => ($domain->url_id == 0 && $domain->count > 2) ? 1 : 0,
                'created_at' => now(),
                'updated_at' => now()
            ];

            $this->countsLinks($domain->id, $type = 'count_link');
        }

        $this->saveDomain($link, $domain);
    }

    /**
     * Создаем новый домен или увеличивем счетчик.
     *
     * @param $link
     * @param $domain
     *
     * @return void
     */
    public function saveDomain($link, $domain) :void
    {
        if(preg_match('/(https?):\/\/.*\..{1,}/', $link))
        {
            if (filter_var($link, FILTER_VALIDATE_URL))
            {
                $host = parse_url($link, PHP_URL_HOST);
                $replaceLink = mb_strtolower(str_replace('www.','', $host));

                if(!empty($replaceLink) && $replaceLink != $domain->domain)
                {
                    AuditorDomains::updateOrCreate(
                        ['domain' => $replaceLink, 'type' => 'parser'],
                        ['parser_content' => 0]
                    )->increment('found_count');
                }
            }
        }
    }

    /**
     * Забираем контент для публички и собираем ссылки для дальнейшего парсинга.
     *
     * @return void
     */
    public function analysisContent() :void
    {
        $this->analysisContentLoggerId = ParserLoggerFacade::startProcess('Собираем контент');

        $contents = AuditorContent::where('analysis_content', 0)->limit(self::ANALYSIS_CONTENT_LIMIT)->get();

        foreach ($contents as $content)
        {
            if (strlen($content->content) > 0)
               $this->storeToJeweler($content);

            AuditorContent::where('id', $content->id)->update(['analysis_content' => 1]);

            $this->sectionTags($content->url_id, $content->domain_id);
        }

        $this->jewelerContentLogging($contents);

        unset($this->dom);

        // TODO Можно вынести этот метод в отдельный процесс, так как жинреннький
        $this->analysisLinks();

        $this->deleteContent();

        if(!$contents->count()) ParserLoggerFacade::offAndRemove();
    }

    /**
     * Записываем новый контент в jeweler_content
     *
     * @param $content
     *
     * @return void
     */
    private function storeToJeweler($content) :void
    {
        ++$this->countWriteContent;

        $this->domLoadHtml($content->content);

        JewelerContent::updateOrCreate(
            ['id_url' => $content->url_id],
            [
                'description' => $this->description(),
                'title' => $this->title(),
                'image' => $this->getImage(),
                'created_page' => $this->getCreatedPage($content)
            ]
        );

        $this->cachedContent($content);
    }

    /**
     * Пишем в кастомный лог выполенения записи в jeweler_content
     *
     * @param $contents
     *
     * @return void
     */
    private function jewelerContentLogging($contents) :void
    {
        $analysisContentData = '<strong>Кол-во записей с контентом: </strong> ' . $contents->count() . '<br>' .
            '<strong>Дата из: </strong>OpenGraph - ' . $this->checkDateFromOG . '; Schema - ' . $this->checkDateFromSchema . '; База - ' . $this->checkDateFromDB . ';<br>' .
            '<strong>Кол-во созданных/обновленных записей с контентом в таблицу jeweler_content: </strong> ' . $this->countWriteContent . '<br>';

        ParserLoggerFacade::stopProcess($this->analysisContentLoggerId, $analysisContentData);
    }

    /**
     * Забираем дату "статьи".
     *
     * @param $content
     *
     * @return object Carbon()
     */
    private function getCreatedPage($content, $parse_createdPage=false)
    {
        // OpenGraph
        $createdPage = $this->publishedDate();
        if($createdPage){
            ++$this->checkDateFromOG;
        }

        if (!$createdPage)
        {
            // Schema
            $createdPage = $this->publishedDateTwo();
            if($createdPage){
                ++$this->checkDateFromSchema;
            }
        }

        if($createdPage && $parse_createdPage == true)
        {
            return $createdPage[0];
        }
        else if($parse_createdPage == true)
        {
            return false;
        }

        if (!$createdPage)
        {
            // DataBase
            $createdPage = AuditorUrls::where('id', $content->url_id)->first();
            if ($createdPage)
            {
                ++$this->checkDateFromDB;
                $createdAt = $createdPage->created_at;
                unset($createdPage);
                $createdPage[] = $createdAt;
            }
        }

        return $createdPage[0] ?? Carbon::now();
    }

    /**
     * Собираем тэги из keywords
     *
     * @return void
     */
    public function analysisTags() :void
    {
        $this->analysisTagsLoggerId = ParserLoggerFacade::startProcess('Анализ');

        $contents = AuditorContent::where('analysis_tags', 0)->limit(self::ANALYSIS_TAG_LIMIT)->get();

        foreach ($contents as $content)
        {
            $keywords = $this->keywords($content->content);

            if ($keywords)
            {
                DB::transaction(function () use ($keywords, $content)
                {
                    $tags = [];
                    foreach ($keywords as $keyword)
                    {
                        ++$this->keywordsCount;

                        $keyword = $this->deleteSpecialСharacters($keyword);

                        if ($this->mbStrWordCount($keyword) < 7 && $keyword !== '' && $keyword !== ' ')
                        {
                            ++$this->tagsCountIterator;

                            $tags[] = $this->prepareTagsForWriting($keyword, $content);
                        }
                    }

                    TagsUrls::insert($tags);
                });
            }

            AuditorContent::where('id', $content->id)->update(['analysis_tags' => 1]);
        }

        $this->deleteContent();

        unset($this->dom);

        $this->analysisTagsLogging($contents);
    }

    /**
     * Считаем количество слов включая русские
     *
     * @param $str
     * @return mixed
     */
    private function mbStrWordCount($str)
    {
        $alphabets = 'АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯя';
        return str_word_count($str, 0, $alphabets);
    }

    /**
     * Увеличиваем счетчики и собираем массив для записи в БД
     *
     * @param $keyword
     * @param $content
     *
     * @return array
     */
    private function prepareTagsForWriting($keyword, $content)
    {
        $kw = trim($keyword);
        $tag = Tags::where('tag',$kw)->first();
        if(!$tag) {
            $tag = Tags::Create([
                'tag' => $kw,
                'slug' => $this->generateSlug($kw),
                'type' => 'parser'
            ]);
        }

        $tag->increment('count');


        TagsDomain::firstOrCreate(
            ['domain_id' => $content->domain_id, 'tag_id' => $tag->id],
            ['type' => 'parser']
        )->increment('count');

        return [
            'url_id' => $content->url_id,
            'domain_id' => $content->domain_id,
            'tag_id' => $tag->id
        ];
    }

    /**
     * Пишем в кастомный лог результат работы analysisTags()
     *
     * @param $contents
     *
     * @return void
     */
    private function analysisTagsLogging($contents) :void
    {
        if($contents->count() > 0)
        {
            $loggerData = '<strong>Кол-во выбранного контента: </strong> ' . $contents->count() . '<br>' .
                '<strong>Кол-во keywords: </strong> ' . $this->keywordsCount . '<br>' .
                '<strong>Кол-во тэгов: </strong> ' . $this->tagsCountIterator . '<br>';

            ParserLoggerFacade::stopProcess($this->analysisTagsLoggerId, $loggerData);
        }
        else
        {
            ParserLoggerFacade::offAndRemove();
        }
    }

    /**
     * Записываем в таблицу tags_section если на сайте есть OpenGraph разметка.
     *
     * Данные из этой таблицы ипользуются для объеденинения с таблцей tags_urls.
     *
     * @see Feed::tagsSection()
     *
     * @param $urlId
     * @param $domainId
     *
     * @return void
     */
    protected function sectionTags($urlId, $domainId) :void
    {
        $keywords = $this->section();
        if ($keywords)
        {
            foreach ($keywords as $keyword)
            {
                if (strlen($keyword) > 2)
                {
                    TagSection::firstOrCreate(
                        ['name' => mb_strtolower($keyword), 'url_id' => $urlId],
                        ['domain_id' => $domainId]
                    );
                }
            }
        }
    }

    /**
     * Удаляем проанализированный контент
     *
     * @return void
     */
    protected function deleteContent() :void
    {
        AuditorContent::where('analysis_content', 1)->where('analysis_tags', 1)->where('analysis_link', 1)->delete();
    }

    /**
     * Основной счетчик
     *
     * TODO дублируется с @see AuditorUrlsController::countsLinks(), убрать дубликат
     *
     * @param $domainId
     * @param $type
     *
     * @return void
     */
    protected function countsLinks($domainId, $type) :void
    {
        $count = CountParser::where('id_domain', $domainId)->increment($type);

        if ($count == 0)
            CountParser::create(['id_domain' => $domainId, $type => 1]);

    }

    protected function cachedContent($content)
    {
        $count = Feed::count();
        if( $count < (int)env('FEED_TABLE_LIMIT') )
        {
            $this->setCachedContent($content);
        }
        else
        {
            $deleted = $this->deleteOldContent($content);
            if($deleted === true)
            {
                $this->setCachedContent($content);
            }
        }
    }

    protected function setCachedContent($content)
    {
        $date_create = $this->getCreatedPage($content, true);
        if($date_create !== false)
        {
            Feed::updateOrCreate(
                ['id_url' => $content->url_id],
                [
                    'description' => $this->description(),
                    'title' => $this->title(),
                    'image' => $this->getImage(),
                    'created_page' => $date_create
                ]
            );
        }
    }

    protected function deleteOldContent($content)
    {
        $odl = Feed::orderBy('created_page', 'asc')->first();
        if( $odl->created_page < $this->getCreatedPage($content, true) )
        {
            $odl->delete();
            return true;
        }
        return false;
    }

}
