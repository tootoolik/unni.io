<?php

namespace App\Http\Controllers\Bots\SitemapParser;

use App\Jobs\AnalysisSitemapUrls;
use App\Jobs\CollectSitemaps;
use App\Model\Bots\Auditor\AuditorDomains;

class SitemapParser
{
    private $domains;

    /**
     * SitemapParser constructor.
     *
     * Select all available domains.
     *
     */
    public function __construct()
    {
        $this->domains = AuditorDomains::with('sitemaps')
                                       ->where('parser_content', 1)
                                       ->where('ping', '!=', 403)
                                       ->get();
    }

    /**
     * Start to parse sitemaps of all domains.
     *
     * @return void
     */
    public function parse(): void
    {
        foreach ($this->domains as $domain)
            dispatch(new CollectSitemaps($domain));
    }

    /**
     * Start to analysis sitemaps of all domains.
     *
     * @return void
     */
    public function analysis(): void
    {
        foreach ($this->domains as $domain)
        {
            $sitemapsUrls = $domain->sitemapsUrls()->where('parsing',0)->limit(10000)->get();

            if($sitemapsUrls->count() > 0)
                dispatch(new AnalysisSitemapUrls($sitemapsUrls, $domain));
        }
    }
}