<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;

class AuthController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware($this->guestMiddleware(), ['except' => 'getLogout']);
//    }
    public function getLogout()
    {
        Auth::logout();
        return redirect()->back();
    }

    public function saveRefererToSession(Request $request)
    {
        if($request->ajax() && $request->has('referer'))
        {
            $url = $request->referer;
        }
        else
        {
            $url = '/';
        }

        session()->put('refererURL', $url);
    }

    public function getUsers(Request $request)
    {
        if($request->ajax())
        {
            $userCollection = User::where('name', 'LIKE', '%'.$request->text.'%')
                ->orderByDesc('id')
                ->limit(10)
                ->get();

            $users = $this->transformDataUsersForTokenize2($userCollection);

            return response()->json($users);
        }
    }

    private function transformDataUsersForTokenize2($users)
    {
        $result = [];

        foreach ($users as $user)
        {
            $result[] = [
                'text' => $user->name . ' (' . $user->email . ')',
                'value' => $user->id
            ];
        }

        return $result;
    }

}
