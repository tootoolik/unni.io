<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreSeoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url' => 'required|max:255|unique:seo,url',
            'title' => 'nullable|max:512',
            'description' => 'nullable|max:2048',
            'meta_title' => 'nullable|max:255',
            'meta_description' => 'nullable|max:255',
            'active' => 'required'
        ];
    }
}
