<?php

namespace App\Console\Commands;

use App\Http\Controllers\Bots\Auditor\AuditorUrlsController;

class ParserUrl extends ParserCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'start_parser_urls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command start parser all urls';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        parent::handle();

        (new AuditorUrlsController())->parseUrlsOfDomain();
    }
}
