<?php

namespace App\Console\Commands;

use App\Components\AmbulanceParser\AmbulanceParser;

class Ambulance extends ParserCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ambulance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Helper if parser is sleep';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        AmbulanceParser::run();
    }
}
