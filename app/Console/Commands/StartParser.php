<?php

namespace App\Console\Commands;

use App\Components\ParserLogger\ParserLoggerFacade;
use App\Http\Controllers\Bots\Auditor\AuditorUrlsController;
use Illuminate\Support\Facades\DB;

class StartParser extends ParserCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'start_parser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'start parser';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {
        parent::handle();

        /**
         * We get the status of a cron, and if 0, go cron for verification domain
         *
         * P.S. Не знаю что это за проверка, но она всегда возвращает true.
         * Логика осталась от бывшего разработчика
         */
        $mainParser = DB::table('main_parser')->orderBy('id', 'desc')->first();
        if (!isset($mainParser->level) || $mainParser->level == 0)
        {
            /** START LOGGER **/
            $id = ParserLoggerFacade::startProcess('Парсим главные страницы доменов');
            /** ************ **/

            (new AuditorUrlsController())->parseDomainHomepage();

            /** STOP LOGGER **/
            ParserLoggerFacade::stopProcess($id);
            /** ************ **/
        }
        $domains = DB::table('auditor_domain')
                ->where([
                    ['type','admin'],
                    ['parser_content','1'],
                ])->get();
        foreach ($domains as $domain){
            DB::table('auditor_domain')->where('id', $domain->id)->update(['ping' => 200]);
        }
    }
}