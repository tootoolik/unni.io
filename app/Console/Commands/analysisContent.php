<?php

namespace App\Console\Commands;

use App\Components\ParserLogger\ParserLoggerFacade;
use App\Http\Controllers\Bots\JewelerBot\JewelerController;

class analysisContent extends ParserCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'analysis_content';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Analysis content, start method analysisContent is controller App\Http\Controllers\Bots\JewelerBot\JewelerController';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        parent::handle();

        /** START LOGGER **/
        $id = ParserLoggerFacade::startProcess('Парсим контент');
        /** ************ **/

        (new JewelerController())->analysisContent();

        /** STOP LOGGER **/
        ParserLoggerFacade::stopProcess($id);
        /** ************ **/
    }
}
