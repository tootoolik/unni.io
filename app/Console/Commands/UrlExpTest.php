<?php

namespace App\Console\Commands;
use App\Traits\BustGetUrl;
use Illuminate\Console\Command;

class UrlExpTest extends Command
{
    use BustGetUrl;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'url_exp_test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $this->urlExpTest();
    }
}
