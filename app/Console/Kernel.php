<?php

namespace App\Console;

use App\Console\Commands\Ambulance;
use App\Console\Commands\analysisContent;
use App\Console\Commands\analysisTags;
use App\Console\Commands\ElasticIndex;
use App\Console\Commands\tmpTable;
use App\Console\Commands\UpdateChannelCounts;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\StartParser::class,
        ElasticIndex::class,
        tmpTable::class,
        analysisContent::class,
        analysisTags::class,
        Ambulance::class,
        UpdateChannelCounts::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /**
         * Собирает HTML с главных страниц доменов.
         */
        $schedule->command('start_parser')
                 ->everyThirtyMinutes();

        /**
         * Структурирует ссылки доменов во временные таблицы.
         */
        $schedule->command('tmp_table_parser')
                 ->withoutOverlapping();

        /**
         * Анализирует контент. Собирает ссылки, тайтл, описание и т.д.
         */
        $schedule->command('analysis_content')
                 ->withoutOverlapping();

        /**
         * Анализирует контент. Собираем тэги.
         */
        $schedule->command('analysis_tags')
                 ->withoutOverlapping();

        /**
         * Записывает в ElasticSearch. Index - "jeweler_content"
         */
        $schedule->command('elasticContent')
                 ->withoutOverlapping();

        /**
         * Парсит URL из временных таблиц.
         */
        $schedule->command('start_parser_urls')
                 ->everyMinute();

        /**
         * Запускает обновление счетчиков каналов
         */
        $schedule->command('update_channel_counts')
            ->everyTenMinutes();

        /**
         * Парсит Sitemaps, записываем урлы в БД
         */
        $schedule->command('parser_sitemap')
                 ->monthly()
                 ->withoutOverlapping();

        /**
         * Анализирует урлы из Sitemap и записывает незнакомые в БД для парсинга
         */
        $schedule->command('analysis_sitemap')
                 ->everyThirtyMinutes()
                 ->withoutOverlapping();

        /**
         * Запускает очереди
         */
        $schedule->command('queue:work')
                 ->everyThirtyMinutes()
                 ->withoutOverlapping();


        /**
         * В случае если парсер "засыпает" пытаемся его "разбудить"
         *
         * TODO рабоатет некоректно.
         * TODO Сделать возможность смотреть на любые изменения в таблице auditor_content, а не только когда в ней ес ть записи.
         * TODO Либо придумать новую логику=)
         */
//        $schedule->command('ambulance')
//                 ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
