<?php

namespace App\Providers\Elasticsearch;

use App\Elastic\ElasticSearch;
use Elasticsearch\ClientBuilder;
use Illuminate\Support\ServiceProvider;

class Client extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ElasticSearch::class, function ($app) {
            return new ElasticSearch(
                ClientBuilder::create()
                    ->setLogger(ClientBuilder::defaultLogger(storage_path('logs/elastic.log')))
                    ->build()
            );
        });
    }
}
