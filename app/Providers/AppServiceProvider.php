<?php

namespace App\Providers;

use App\Components\Analyzer\AnalyzerFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        $this->app->singleton('analyzerFactory',function (){
            return new AnalyzerFactory();
        });

//        DB::statement('SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
