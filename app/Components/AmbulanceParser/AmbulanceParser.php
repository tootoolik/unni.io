<?php

namespace App\Components\AmbulanceParser;

use App\Components\ParserLogger\ParserLogger;
use App\Components\ParserLogger\ParserLoggerFacade;
use App\Model\Bots\Auditor\AuditorContent;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class AmbulanceParser
{
    /**
     * AmbulanceParser constructor.
     *
     * Register the parser logger to work with it
     */
    public function __construct()
    {
        app()->singleton('parser_logger', function () {
            return new ParserLogger($this->signature);
        });
    }

    /**
     * The main method for running everything
     *
     * @return void
     */
    public static function run() :void
    {
        $checkTime = self::checkTimeForHelp();

        if($checkTime)
        {
            $idLoggerProcess = ParserLoggerFacade::startProcess('Лечение парсера');

            $count = self::heal();

            ParserLoggerFacade::stopProcess($idLoggerProcess, 'Кол-во удаленных файлов:' . $count);
        }
    }

    /**
     * Check how long there have been updates in the auditor_content table
     * and how long it has been working with temporary tables.
     *
     * @return bool
     */
    private static function checkTimeForHelp() :bool
    {
        return (self::checkAuditorContentTable() || self::checkTempTables()) ? true : false;
    }

    /**
     * Removes temporary domain files that are used to verify the start of the analysis
     * of a table in the database of temporary tables. In case of any server error,
     * there is a very high probability that the file will not be deleted, respectively,
     * the system will assume that it works with this domain, and the parser will fall asleep
     *
     * @return int
     */
    public static function heal() :int
    {
        $listDomains = Storage::disk('local')->files('/domains');

        $count = count($listDomains);

        Storage::delete($listDomains);

        return $count;
    }


    /**
     * Check how long it has been working with the auditor_content table
     *
     * @return bool
     */
    private static function checkAuditorContentTable() :bool
    {
        $latest = AuditorContent::latest("updated_at")->first();

        return $latest ? self::checkDiffBetweenDates($latest->updated_at) : true;

    }


    /**
     * Check how long there have been changes in the specified period of time
     *
     * @param $date
     * @param int $hours
     * @return bool
     */
    private static function checkDiffBetweenDates($date, int $hours = 1) :bool
    {
        $diff = Carbon::now()->diffInHours($date);

        return $diff >= $hours ? true : false;
    }

    /**
     * Check how long it took to work with temporary tables
     *
     * @return bool
     */
    private static function checkTempTables() :bool
    {
        $listDomains = DB::connection('mysqlTmpUrl')->select('SHOW TABLES');

        return count($listDomains) === 0 ? true : false;
    }
}