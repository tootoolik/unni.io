<?php
/**
 * Created by PhpStorm.
 * User: Des-2
 * Date: 06.06.2019
 * Time: 16:25
 */

namespace App\Components\ParserLogger;


use App\Components\ParserLogger\Models\ParserLoggerCycle;
use App\Components\ParserLogger\Models\ParserLoggerCycleProcess;
use App\Components\ParserLogger\Models\ParserProcessManager;
use Illuminate\Support\Carbon;

class ParserLogger
{
    const DATE_FORMAT = 'Y-m-d H:i:s:v';

    private $cycle;
    private $processes = [];
    private $isActive = false;

    /**
     * ParserLogger constructor.
     * @param string $processName
     * @throws \Exception
     */
    public function __construct(string $processName)
    {
        $processManager = ParserProcessManager::where('process_name', $processName)->first();
        if (!$processManager)
            throw new \Exception('Undefined process manager name');

        $this->isActive = (bool)$processManager->is_active_logger;

        if (!$this->isActive) return;

        $this->cycle = new ParserLoggerCycle();
        $this->cycle->processManager()->associate($processManager);
        $this->cycle->save();
    }

    public function startProcess(string $name): ?int
    {
        if (!$this->isActive) return null;

        $process = new ParserLoggerCycleProcess(compact('name'));
        $process->cycle()->associate($this->cycle);
        $process->started_at = Carbon::now()->format(self::DATE_FORMAT);
        $process->save();

        $this->processes[$process->id] = $process;

        return $process->id;
    }

    public function stopProcess($processId = null, string $comment = null): void
    {
        if (!$this->isActive) return;

        $process = $this->getProcess($processId);
        $process->comment = $comment;
        $process->ended_at = Carbon::now()->format(self::DATE_FORMAT);
        $process->save();

        if ($processId)
            unset($this->processes[$processId]);
        else
            array_pop($this->processes);
    }

    public function getProcess($processId = null): ?ParserLoggerCycleProcess
    {
        if (!$this->isActive) return null;

        if (!$processId)
            return array_last($this->processes);

        return $this->processes[$processId];
    }

    /**
     * @throws \Exception
     */
    public function offAndRemove()
    {
        $this->isActive = false;
        
        if($this->cycle)
        	$this->cycle->delete();
    }

    public function loadCycle($id)
    {
        $this->cycle = ParserLoggerCycle::find($id);
    }
}