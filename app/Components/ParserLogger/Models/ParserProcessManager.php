<?php
/**
 * Created by PhpStorm.
 * User: Des-2
 * Date: 06.06.2019
 * Time: 16:29
 */

namespace App\Components\ParserLogger\Models;


use Illuminate\Database\Eloquent\Model;

class ParserProcessManager extends Model
{
    protected $table = 'parser_processes_manager';

    public function cycle()
    {
        return $this->belongsTo(ParserLoggerCycle::class,'processes_id', 'id');
    }
}