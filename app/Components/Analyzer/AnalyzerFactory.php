<?php

namespace App\Components\Analyzer;

class AnalyzerFactory
{
    /**
     * @param $name
     * @param $arguments
     * @return object
     */
    public function __call(string $name, array $arguments)//: object
    {
        $name = 'App\Components\Analyzer\\'.ucfirst($name);
        return new $name();
    }
}