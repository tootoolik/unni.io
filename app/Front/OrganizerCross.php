<?php

namespace App\Front;

use App\Model\Fornt\Tag;
use App\Model\Tags;
use App\User;
use Illuminate\Database\Eloquent\Model;

class OrganizerCross extends Model
{
    protected $table = 'manual_tags_organizer';

    public function tags()
    {
        return $this->belongsTo(Tag::class, 'tags_id', 'id');
    }

    public function organizers()
    {
        return $this->belongsTo(Organizer::class, 'organizer_id', 'id');
    }
}
