<?php

namespace App\Front;

use Illuminate\Database\Eloquent\Model;

class FollowLink extends Model
{
    protected $table = "follow_links";
    protected $fillable = ['ip', 'url_id', 'source_page', 'link', 'cookie'];
}
