<?php

namespace App\Front;

use App\Model\Fornt\Tag;
use App\Model\Tags;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Organizer extends Model
{
    protected $fillable = ['website', 'type','name', 'fb_group_id','city_id', 'country_id'];

    public function getFormattedBody()
    {
        $body = str_replace("\\n", " ", $this->body_m);

        return trim($body, '"');
    }

    public function manualTags()
    {
        return $this->belongsToMany(Tags::class, 'manual_tags_organizer');
    }

    public function usersSave()
    {
        return $this->belongsToMany(User::class, 'organizer_saves');
    }

    public function usersLike()
    {
        return $this->belongsToMany(User::class, 'organizer_likes');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class,
            'manual_tags_organizer',
            'organizer_id',
            'tags_id',
            'id',
            'id');
    }
}
