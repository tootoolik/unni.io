<?php

namespace App\Front;

use App\Model\Fornt\Tag;
use App\Model\Tags;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $fillable = [
        'name',
        'body',
        'post_img',
        'active',
    ];

    public function manualTags()
    {
        return $this->belongsToMany(Tags::class, 'manual_tags_posts');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class,
            'manual_tags_posts',
            'posts_id',
            'tags_id',
            'id',
            'id');
    }

    public function postAuthor()
    {
        return $this->belongsToMany(User::class, 'posts_author');
    }

    public function Authors()
    {
        return $this->belongsToMany(User::class,
            'posts_author',
            'posts_id',
            'user_id',
            'id',
            'id');
    }

}
