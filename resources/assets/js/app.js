
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

Vue.component('list-domains', require('./components/admin/ListDomains.vue'));

Vue.component('live-search', require('./components/LiveSearch.vue'));
/*
Admin pages
 */
Vue.component('status-parser-settings', require('./components/admin/ParserStatus.vue'));
/*
Statistics pages
 */
Vue.component('statistics-domains', require('./components/admin/Statistics/domains.vue'));

/*
Front pages
 */
Vue.component('domains', require('./components/front/Domains.vue'));
Vue.component('tags', require('./components/front/Tags.vue'));
const app = new Vue({
    el: '#app'
});
