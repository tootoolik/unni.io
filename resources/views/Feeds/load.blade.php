<div id="load">
    <ul class="list_result_ul">
        @if(!empty($feeds))
            @foreach($feeds as $feed)

                @php
                    $url = '';
                    if(isset($feed->linksContent))
                        $url = $feed->linksContent->url;
                @endphp

                <li class="list-group-item">
                    <div class="col-8 tablet">
                        <div>
                            <h3 class="title-blok">{{ $feed->title }}</h3>
                            <noindex>
                                <a href="/link?i={{ $url }}" target="_blank" rel="nofollow">
                                    <i class="fas fa-external-link-alt"></i>
                                </a>
                            </noindex>
                        </div>
                        <div class="list_result_ul_chanel">
                            <a id="list_result_ul_chanel"
                               href="{{ route('front.domains', str_replace('www.', '', parse_url($url, PHP_URL_HOST))) }}"
                               target="_blank">
                                {{ '@ '.str_replace('www.', '', parse_url($url, PHP_URL_HOST)) }}
                            </a>
                        </div>
                        <div>@if($feed->description != 1){{ $feed->description }}@endif</div>
                        <div class="dataindex">Дата: {{ $feed->created_page }}</div>
                    </div>
                    <div class="col-4 tablet">
                        <a class="images_popup" href="{{ $feed->image }}">
                            <img src="{{ $feed->image  ?? asset('images/default-200x200.png') }}" alt="">
                        </a>
                    </div>
                    <div class="col-12" id="feed-tags">
                        @if(!empty($feed->tags))
                            @foreach ($feed->tags as $tag)
                                @if(!empty($tag->slug))
                                    <span>
                                        <a href="/tag/{{$tag->slug}}"
                                           class="tags-link domains-tag-link">{{$tag->tag}}</a>
                                    </span>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </li>
            @endforeach
        @endif
    </ul>
    {{ $feeds->links('paginate_one') }}
</div>
