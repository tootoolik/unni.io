@if ($paginator->hasPages())

    <ul class="pagination modal-5">
        @if ($paginator->onFirstPage())

            <li class="disabled"><span class="prev fa fa-arrow-left"></span></li>

        @else

            <li><a href="{{ $paginator->previousPageUrl() }}" rel="prev" class="prev fa fa-arrow-left"></a></li>

        @endif
        @foreach ($elements as $element)
            @if (is_string($element))

                {{--<li class="disabled"><span>{{ $element }}</span></li>--}}

            @endif
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())

                        <li><span><a href="#" class="active">{{ $page }}</a></span></li>

                    @else

                        <li><a href="{{ $url }}">{{ $page }}</a></li>

                    @endif
                @endforeach
            @endif
        @endforeach
        @if ($paginator->hasMorePages())

            <li><a href="{{ $paginator->nextPageUrl() }}" rel="next" class="next fa fa-arrow-right"></a></li>

        @else

            <li class="disabled"><span class="next fa fa-arrow-right"></span></li>

        @endif
    </ul>


@endif
