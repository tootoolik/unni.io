@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Привет, рады что ты с нами</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <ul class="profile-menu-list">
                        <li><a href="{{ route('front.profile.site.add') }}">Добавить сайт</a></li>
                        <li><a href="{{ route('front.profile.posts.my') }}">Мои публикации</a></li>
                        <li><a href="{{ route('front.profile.posts.add') }}">Добавить публикацию</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
