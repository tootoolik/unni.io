@extends('layouts.app')
@section('content')
    <h3 class="panel-heading textCenter"></h3>
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="mainBlocks">
        <div class="list_result">
            <h1>Архив</h1>
            <div class="searchPageAjax">
                @include('Feeds.load')
            </div>

        </div>
    </div>
@endsection