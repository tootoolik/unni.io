@extends('layouts.app')
@section('content')
    <div class="mainBlocks domains">
        <div class="col-4 tablet indent-up-down-10">
            <p>
                <strong class="channels-categories-main-title">Категории</strong>
            </p>

            <hr>

            <div class="channels-categories">
                <div class="row channels-categories-block">
                    <div class="col-lg-6 col-md-8 col-sm-9 col-xs-9">
                        <span class="channels-categories-title {{ (session('channels_filter_tag') === null || session('channels_filter_tag') === 'all') ? 'channels-categories-title-active' : '' }}" data-type="tag" data-sort="all">
                            Все
                        </span>
                    </div>
                </div>
                @foreach($categories as $category)
                    <div class="row channels-categories-block">
                        <div class="col-lg-6 col-md-8 col-sm-9 col-xs-9">
                            <span class="channels-categories-title {{ session('channels_filter_tag') == $category->tags_id ? 'channels-categories-title-active' : '' }}" data-type="tag" data-sort="{{ $category->tags_id }}">
                                {{ $category->tags->tag ?? '' }}
                            </span>
                        </div>
                        <div class="col-lg-5 col-md-4 col-sm-3 col-xs-3 text-right">
                            <span class="channels-categories-count">{{ $category->count_tags }}</span>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>

        <div class="col-8 searchPageAjax tablet">
            @include('Channels.load')
        </div>
        <div class="loader" id="loader" data-page="1"></div>
    </div>

@endsection
