@if(Auth::check() && Auth::user()->checkChannelsSaved($channel->id))
    <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
        <i class="fas fa-bookmark channel-bookmark-subscribe channel-bookmark-subscribe-saved"></i>
        <span><strong>Сохранено</strong></span>
        <span class="caret channel-caret-subscribe"></span>
    </button>
    <ul class="dropdown-menu channel-subscribe-dropdown">
        <li>
            <span class="channel-save channel-btn-unsave" data-channel-id="{{ $channel->id }}">Отменить сохранение</span>
        </li>
        <li><a href="#">Посмотреть сохраненные объекты</a></li>
    </ul>
@else
    <button type="button" class="btn btn-default btn-sm channel-save" data-channel-id="{{ $channel->id }}">
        <i class="fas fa-bookmark channel-bookmark-subscribe"></i>
        <span><strong>Сохранить</strong></span>
    </button>
@endif