@foreach($channels as $channel)
    @if($channel->manualDomains && count($channel->manualDomains) > 0)
        @foreach($channel->manualDomains as $domain)
            <li class="list-group-item list-domains-item">

                <div class="row organizer">

                    <div class="col-md-1 col-xs-1 text-center">
                        <div class="organizer-favicon">
                            @if($domain->domain)
                                <img src="https://www.google.com/s2/favicons?domain={{ $domain->domain }}" alt="favicon">
                            @else
                                <img src="{{ asset('images/default-favicon.png') }}" alt="favicon">
                            @endif
                        </div>
                    </div>

                    <div class="col-md-9 col-xs-8">

                        <p class="organizer-name"><a href="domains/{{ $domain->domain }}">@ {{ $channel->name }}</a></p>
                        @if($channel->fb_group_id && (int)$channel->fb_group_id !== 0)
                            <a class="btn btn-facebook-organizer" href="https://www.facebook.com/{{ $channel->fb_group_id }}/" target="_blank">
                                <span class="btn-facebook-organizer-text">Подробнее</span>
                                <i class="fab fa-facebook-f btn-facebook-organizer-icon"></i>
                            </a>
                        @endif
                        @if($channel->body)
                            <div class="organizer-body">{!! $channel->body !!}</div>
                        @endif
                        @if($channel->manualTags)
                            <div style="margin-top: 10px;margin-bottom: 8px">
                                @foreach($channel->manualTags as $tag)
                                <span class="tags-link">
                                    <a href="{{ route('tags.urls', $tag->slug) }}"><strong>{{ $tag->tag }}</strong></a>
                                </span>
                                @endforeach
                            </div>
                        @endif

                        @if( isset( $channel->all_links ) )
                            <span>Найдено: {{ $channel->all_links }}</span><br>
                        @endif
                        @if( isset( $channel->parse_content ) )
                            <span>Добавлено: {{ $channel->parse_content }}</span><br>
                        @endif
                        @if( isset( $channel->count_parse_today) )
                            <span>Обновлено сегодня: {{ $channel->count_parse_today }}</span><br>
                        @endif
                        @if( isset( $channel->updated_at) )
                            <span>Обновлено: {{ $channel->updated_at }}</span><br>
                        @endif

                        <div class="btn-group channel-subscribe-box">
                            <div class="btn-group channel-user-subscribe-content">
                                @php
                                    $checkSubscribe = Auth::check() && Auth::user()->checkChannelsSubscribe($channel->id);
                                @endphp
                                <button type="button" class="btn btn-default dropdown-toggle btn-sm channel-subscribe-btn" data-toggle="dropdown" data-channel-id="{{ $channel->id }}">
                                    <i class="fas fa-rss channel-bookmark-subscribe {{ $checkSubscribe ? 'channel-bookmark-subscribe-saved' : '' }}"></i>
                                    <span><strong class="channel-subscribe-btn-text">{{ $checkSubscribe ? 'Подписан' : 'Подписаться' }}</strong></span>
                                    <span class="caret channel-caret-subscribe"></span>
                                </button>
                                <ul class="dropdown-menu channel-subscribe-dropdown">
                                    <li>
                                        @if($checkSubscribe)
                                            <span class="channel-unsubscribe" data-channel-id="{{ $channel->id }}">Отменить подписку на эту страницу</span>
                                        @else
                                            <span class="channel-subscribe" data-channel-id="{{ $channel->id }}">Подписаться на эту страницу</span>
                                        @endif
                                    </li>
                                    <li><a href="#">Посмотреть все подписки</a></li>
                                </ul>
                            </div>
                            <div class="btn-group channel-user-save-content">
                                @include('Channels.user_save_content')
                            </div>
                            <button type="button" class="btn btn-default btn-sm">
                                <i class="fas fa-ellipsis-h channel-ellipsis-subscribe"></i>
                            </button>
                        </div>

                    </div>

                    <div class="col-md-2 col-xs-3 like-box like-box-channels" data-channel-id="{{ $channel->id }}">
                        <div class="panel panel-default text-center">
                            <div class="panel-body">
                                <div class="subscribe-box-body">
                                    <i class="fas fa-caret-up fa-lg caret-icon"></i>
                                    <p>
                                        <strong class="channel-likes-count">{{ $channel->users_like_count }}</strong>
                                    </p>
                                    <div class="heart-subscribe">
                                        @if(Auth::check() && Auth::user()->checkChannelsLike($channel->id))
                                            <i class="fas fa-heart filled-heart-icon"></i>
                                        @else
                                            <i class="far fa-heart empty-heart-icon"></i>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </li>
        @endforeach
    @endif
@endforeach
