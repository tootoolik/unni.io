@extends('layouts.app')

@if(!empty(Request::query()) && isset(Request::query()['search']))
    @section('meta_title')
        {{ Request::query()['search'] }} на Unni.io {{ $meta_title_page ? $meta_title_page:''}}
    @endsection

    @section('meta_description')
        {{ Request::query()['search'] }} на Unni.io - новости в Украине, поиск ИТ- компаний, поиск стартапов и новостей мира технологий
    @endsection
@endif

@section('content')

    <script>
        $(function () {
            $("#search_name").checkboxradio();
            $("#search_content").checkboxradio();
        });
    </script>
    <div class="flex-center position-ref full-height">
        <div class="mainBlocks">
            <div class="header__title textCenter header__statistics">
                <span>Страниц в поиске  {{$countIndexis}}</span>
                <span>Страниц найдено  {{$countIndexisUrls}}</span>
            </div>
            <div class="header__title textCenter">
                UNNI SEARCH SYSTEM
            </div>

            <div class="header__searchBlock">
                <form id="elasticScout" action="" method="get">
                    <div class="mysearchbar">
                        <div class="mysearchbar-search-input">
                            <input name="search"
                                   value="{{ app('request')->input('search') }}{{ app('request')->input('query') }}"
                                   class="header__search who" placeholder="Search..." autocomplete="off">
                            <button class="header__searchButton">Поиск</button>
                        </div>

                        <fieldset>
                            <label for="">Искать по:</label>
                            <label for="search_name">Названию</label>
                            <input type="radio" name="typeSearch" id="search_name" value="search_name" checked>
                            <label for="search_content">Всему тексту</label>
                            <input type="radio" name="typeSearch" id="search_content" value="search_content">
                        </fieldset>
                    </div>
                </form>

                <ul id="resSearch"></ul>

                <div class="searchPageAjax">
                    @if ($paginates)
                        <div>Результатов: примерно {{$paginates->getTotalItems()}}</div>
                    @endif
                    @include('loadSearch')
                </div>

            </div>

            @if(count($allTags) > 0 AND !$paginates)
                <div class="popular-search textCenter">
                    <h3>Популярные поисковые запросы</h3>
                    <a href="{{ route('tags') }}" class="tag-link-all">
                        <span class="tags-link">Смотреть все</span>
                    </a>
                    @foreach($allTags as $allTag)
                            <span class="tags-link">
                                <a href="{{ route('tags.urls', $allTag->slug) }}">{{$allTag->tag}}</a>
                            </span>
                    @endforeach
                </div>
            @endif

            <hr>
            @if(!$paginates)
                <ul class="list-group list-group-fresh-content" id="front-fresh-content">
                    @include('fresh_content')
                </ul>

                <script>
                    function mode() {
                        $.ajax({
                            url: '/front_fresh_content',
                            type: "POST",
                            success: function(data) {
                                $('#front-fresh-content').html(data);
                            }
                        });
                    };

                    setInterval(mode, 30000);
                </script>
            @endif
        </div>
    </div>
@endsection
