<div class="col-sm-7 col-md-7 col-xs-12 col-lg-4 panel panel-default modal-block-center" id="load">
    <div class="panel-heading text-center"><h3>Укажите ссылку на страницу в FB!</h3></div>
    <div class="panel-body">
        <form class="customForm" method="POST" action="{{ route('organizers.add') }}">
            {{ csrf_field() }}
            <div class="input {{ $errors->has('link') ? ' has-error' : '' }}">
                <label for="link">Пример: https://www.facebook.com/имя вашей страницы</label>
                <input type="text" name="link" required autocomplete="off">
                @if ($errors->has('link'))
                    <span class="help-block"><strong>{{ $errors->first('link') }}</strong></span>
                @endif
            </div>
            <div class="sendmail">
                <button type="submit">Отправить</button>
            </div>
        </form>
        <div class="alert alert-danger" style="display:none;"></div>
        <div class="alert alert-success" style="display:none;"></div>
    </div>
</div>
<link href="{{ asset('vendor/forms/form.css') }}" rel="stylesheet">
<script src="{{ asset('vendor/forms/form.js') }}"></script>
<script>
    $('form').submit(function(e) {
        var $form = $(this);
        $.ajax({
            type: "POST",
            url: "/organizers/add",
            data: {"name": name},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: false,
            success: function(data) {

            }
        });
    });
</script>
