<div id="load">
    <ul class="list-group" id="list-organizers">

        @include('Organizer.filters')

        @include('Organizer.list_organizers')

    </ul>

    <!-- Modal -->
    <div class="modal fade" id="modal-when-like-organizer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    <div class="text-center">
                        <img src="/image/unni_logo.png" alt="logo in modal" class="text-center" style="max-width:150px;">
                    </div>

                </div>

                <div class="modal-body text-center">
                    <p class="h4">Авторизуйтесь, чтобы проголосовать</p>
                    <br>
                    <div id="modal-organizer-likes">

                    </div>
                </div>

                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <a href="{!! route('socialite.auth', 'facebook') !!}" class="btn btn-md btn-block modal-facebook">
                                <i class="fab fa-facebook-f"></i>
                                &nbsp;
                                <span class="modal-facebook-text">Войти с помощью Facebook</span>
                            </a>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <a href="{!! route('socialite.auth', 'google') !!}" class="btn btn-md btn-block modal-google">
                                <i class="fab fa-google-plus-g"></i>
                                &nbsp;
                                <span class="modal-google-text">Войти с помощью Google</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>