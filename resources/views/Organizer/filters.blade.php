<li class="list-group-item list-domains-item organizer-filters-main-block">
    <div class="row organizer-lists">

        <div class="col-lg-1 col-md-0"></div>

        <div class="col-lg-9 col-md-12 organizer-lists-sort-box">

            <div class="organizer-sort-with-other">
                <ul class="list-inline row organizer-sort-with-other-list">
                    <li class="col-lg-2 col-md-3 col-sm-3 col-xs-3 organizer-sort-other-list-item">
                        <span class="organizer-sort-other-list-item-link {{(session('organizer_filter_other') === null || session('organizer_filter_other') == 'timeline') ? 'organizer-sort-other-active' : '' }}"
                              data-type="filter" data-sort="timeline">
                            TIMELINE
                        </span>
                    </li>
                    <li class="col-lg-2 col-md-3 col-sm-3 col-xs-3 organizer-sort-other-list-item">
                        <span class="organizer-sort-other-list-item-link {{ session('organizer_filter_other') == 'popularity' ? 'organizer-sort-other-active' : '' }}"
                              data-type="filter" data-sort="popularity">
                            POPULAR
                        </span>
                    </li>
                </ul>
            </div>

            <div class="organizer-sort-with-date">
                <ul class="list-inline text-center organizer-sort-with-date-row">
                    @foreach($countOrganizersByDate as $item)
                        @if( $item['count'] > 0)
                            <li class="organizer-sort-list-item {{ session('organizer_filter_date') == $item['sort'] ? 'organizer-sort-by-date-active' : '' }}">
                            <span class="organizer-sort-list-item-link" data-sort="{{$item['sort']}}"
                                  data-type="date">{{$item['name']}}</span>
                                <span class="organizer-sort-with-date-badge">{{ $item['count'] }}</span>
                            </li>
                        @endif
                    @endforeach

                    <li class="organizer-sort-list-item  organizer-sort-with-date-all {{ (session('organizer_filter_date') === null || session('organizer_filter_date') == '-30 years') ? 'organizer-sort-by-date-active' : '' }}">
                        <span class="organizer-sort-list-item-link organizer-sort-with-date-all-link"
                              data-sort="-30 years" data-type="date">Все</span>
                    </li>
                </ul>
            </div>

        </div>
        <div class="col-lg-2 col-md-0"></div>
    </div>
</li>
