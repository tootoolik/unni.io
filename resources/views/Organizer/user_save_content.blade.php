@if(Auth::check() && Auth::user()->checkOrganizerSaved($organizer->id))
    <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
        <i class="fas fa-bookmark organizer-bookmark-subscribe organizer-bookmark-subscribe-saved"></i>
        <span><strong>Сохранено</strong></span>
        <span class="caret organizer-caret-subscribe"></span>
    </button>
    <ul class="dropdown-menu organizer-subscribe-dropdown">
        <li>
            <span class="organizer-save organizer-btn-unsave" data-organizer-id="{{ $organizer->id }}">Отменить сохранение</span>
        </li>
        <li><a href="#">Посмотреть сохраненные объекты</a></li>
    </ul>
@else
    <button type="button" class="btn btn-default btn-sm organizer-save" data-organizer-id="{{ $organizer->id }}">
        <i class="fas fa-bookmark organizer-bookmark-subscribe"></i>
        <span><strong>Сохранить</strong></span>
    </button>
@endif