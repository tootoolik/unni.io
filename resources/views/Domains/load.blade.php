<div id="load">
    @foreach($domainContents as $domainContent)
        @php $parseURL = parse_url($domainContent->linksContent->url) @endphp
        <li class="domain-tags">
            @if($domainContent->image)
                <div class="col-8">
                    @else
                        <div class="col-8">
                            @endif
                            <div>
                                <h3 class="title-blok">{{ $domainContent->title }}</h3>
                                <noindex>
                                    <a href="/link?i={{ $parseURL ? $domainContent->linksContent->url : 'http://'.$name_domains }}"
                                       target="_blank" rel="nofollow">
                                        <i class="fas fa-external-link-alt"></i>
                                    </a>
                                </noindex>
                            </div>
                            <div>
                                @if($domainContent->description != 1)
                                    {{ $domainContent->description }}
                                @endif
                            </div>

                            <div>{{ $domainContent->created_page }}</div>

                        </div>
                        <div class="col-4 domains-image">
                            @if($domainContent->image)
                                <a class="images_popup" href="{{ $domainContent->image }}">
                                    <img src="{{ $domainContent->image }}" alt="">
                                </a>
                            @else
                                <img width="100px" src="/images/default-200x200.png" alt="">
                            @endif
                        </div>

                        <div class="col-12" id="feed-tags">
                            @foreach ($domainContent->tags as $tag)
                                <span>
                                    <a href="{{ route('tags.urls', $tag->slug) }}"
                                    class="tags-link domains-tag-link">{{$tag->tag}}</a>
                                </span>
                            @endforeach
                        </div>
        </li>
    @endforeach
    {{ $domainContents->links('paginate_one') }}
</div>
