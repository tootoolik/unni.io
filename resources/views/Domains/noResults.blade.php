@extends('layouts.app')

@section('meta_title')
    Канал {{ $domain->domain }} - Unni.io
@endsection

@section('meta_description')
    Канал {{ $domain->domain }} на Unni.io в Украине, поиск ИТ- компаний, поиск стартапов и новостей мира технологий
@endsection

@section('script')
    <script id="userFix">
        if (window.requestIdleCallback) {
            requestIdleCallback(function () {
                Fingerprint2.get(function (components) {
                    var imprint = Fingerprint2.x64hash128(components.map(function (pair) {
                        return pair.value
                    }).join(), 31);
                    $.ajax({
                        type: "POST",
                        url: "{{ route('follow.channel') }}",
                        data: {"imprint": imprint, "domain": "{{ $domain->id }}"},
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (data) {

                        }
                    });
                })
            })
        } else {
            setTimeout(function () {
                Fingerprint2.get(function (components) {
                    var imprint = Fingerprint2.x64hash128(components.map(function (pair) {
                        return pair.value
                    }).join(), 31);
                    // console.log(components) // an array of components: {key: ..., value: ...}
                })
            }, 500)
        }
        $('#userFix').remove();
    </script>
@stop

@section('content')
    <div class="container">

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <div class="list_result"><h1>Канал домена {{ $domain->domain }}</h1></div>

        <br>

        <div class="channel-box">
            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 channel-box-data">

                <div class="domain-preview">
                    <div class="domain-preview-image">
                        @if($domain->logo)
                            <img class="domain-preview-image-img" src="/storage/{{ $domain->logo }}" alt="img" width="150" height="150">
                        @else
                            <h1>{{str_replace('www.', '', $domain->domain)[0]}}</h1>
                        @endif
                    </div>

                    <div class="domain-preview-desc">
                        <div class="domain-preview-desc-name">
                            {{$domain->domain}}
                        </div>

                        <div class="domain-preview-desc-chanel">
                            {{ $domain->getPreviewChannel() }}
                        </div>

{{--                        <div class="domain-preview-desc-pages-search">--}}
{{--                            Готовые страницы для поиска: {{$domain->countLink->first()->count_content}}--}}
{{--                        </div>--}}

{{--                        <div class="domain-preview-desc-found">--}}
{{--                            Найдено: {{$domain->countLink->first()->count_link}}--}}
{{--                        </div>--}}

                        <div class="domain-preview-desc-viewed">
                            Просмотрено: {{ $follow }}
                        </div>
                    </div>

                </div>

                <div class="domains-channel-btns">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 btn-facebook-box">
                            <a href="#null" onClick="openWin2()" class="btn btn-facebook d-block">
                                Поделиться в Facebook
                            </a>
                            <script>
                                function openWin2() {
                                    myWin = open("http://www.facebook.com/sharer.php?u={{ route('front.domains', $domain->domain) }}", "displayWindow", "width=520,height=300,left=350,top=170,status=no,toolbar=no,menubar=no");
                                }
                            </script>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 btn-added-tag-box">
                            <a href="{{ route('offer-tags') }}" class="btn btn-success d-block" data-id="{{$domain->id}}">
                                Предложить тег
                            </a>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 domain-description-box">
                <div class="domain-description">
                    <p class="title-for-block">Описание</p>
                    <p class="domain-description-desc">{{ $domain->description }}</p>

                    <p class="title-for-block similar-channels-title">Похожие каналы</p>
                    <div class="similar-channels">
                        @foreach($similarChannels as $channel)
                            <a href="{{ route('front.domains', $channel->domain) }}" target="_blank" class="similar-channels-link">
                                {{ $channel->getPreviewChannel() }}
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <hr style="border-top: 1px solid #e7eaec">

        <div class="row">
            <div class="col-lg-8">
                <p class="links-tags-title">Лента</p>
                <div class="header__searchBlock">
                    <ul>
                        @if(!empty($domainContents))
                            <div class="popular-search searchPageAjax">
                                @include('Domains.load')
                            </div>
                        @endif
                    </ul>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="linkTags">
                    <p class="links-tags-title">Теги домена</p>
                    @foreach($tags->slice(0, 100) as $domainTag)
                        <a href="/?search={{$domainTag->tag}}&typeSearch=search_name">
                            <span class="tags-link">{{$domainTag->tag}} ({{$domainTag->count}}) ({{ $domainTag->id }})</span>
                        </a>
                    @endforeach
                    @if (count($tags) >= 100)
                        <a href="/domains/{{ $domain->domain }}/tags">
                            <span class="tags-link">...</span>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
