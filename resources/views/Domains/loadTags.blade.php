<div id="load">
    @foreach($tags as $domainTag)
        <a href="{{ route('tags.urls', $domainTag->slug) }}">
            <span class="tags-link">{{$domainTag->tag}} ({{$domainTag->count}})</span>
        </a>
    @endforeach
    {{ $tags->links('paginate_one') }}
</div>
