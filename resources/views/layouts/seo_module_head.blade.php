@php
    $defaultSeoTExt = env('APP_NAME') . ' - поисковая система';
    $metaTitle = get_seo_value('meta_title') ?? $defaultSeoTExt;
    $metaDescription = get_seo_value('meta_description') ?? $defaultSeoTExt;
@endphp

<meta name="keywords" content="unni.io - поисковая система"/>

<meta property="og:type" content="website search"/>
<meta property="og:url" content="{{ request()->fullUrl() }}"/>

<title>@section('meta_title'){{ $metaTitle }}@show</title>
<meta property="og:title" content="@section('meta_title'){{ $metaTitle }}@show"/>

<meta name="description" content="@section('meta_description'){{ $metaDescription }}@show">
<meta property="og:description" content="@section('meta_description'){{ $metaDescription }}@show"/>

<meta property="og:image" content="@section('meta_image')https://unni.io/image/unni_logo.png @show"/>
