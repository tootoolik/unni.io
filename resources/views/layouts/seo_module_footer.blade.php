@php
    $seoTextTitle = get_seo_value('title');
    $seoTextDesc = get_seo_value('description');
@endphp

@if($seoTextTitle || $seoTextDesc)
    <div class="mainBlocks seo-texts-box">
        @if($seoTextTitle)
            <div class="seo-text-title">{!! $seoTextTitle !!}</div>
        @endif

        @if($seoTextDesc)
             <div class="seo-text-description">{!! $seoTextDesc !!}</div>
        @endif
    </div>
@endif
