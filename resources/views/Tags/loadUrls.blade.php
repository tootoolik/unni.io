<div id="load">
    <ul class="list_result_ul">
        @if(!empty($tags))
            @foreach($tags as $tag)
                @php
                    $url = '';
                    if(isset($tag->url))
                        $url = $tag->url;
                @endphp
                <li>
                    <div class="col-8 tablet">
                        <div>
                            <h3 class="title-blok">{{ $tag->title }}</h3>
                            <noindex>
                                <a href="/link?i={{ $url }}" target="_blank" rel="nofollow">
                                    <i class="fas fa-external-link-alt"></i>
                                </a>
                            </noindex>
                        </div>

                        <div class="list_result_ul_chanel">
                            <a id="list_result_ul_chanel"
                               href="{{ route('front.domains', str_replace('www.', '', parse_url($url, PHP_URL_HOST))) }}"
                               target="_blank">
                                {{ '@ '.str_replace('www.', '', parse_url($url, PHP_URL_HOST)) }}
                            </a>
                        </div>
                        <div>@if($tag->description != 1){{ $tag->description }}@endif</div>

                        <div class="dataindex">Дата: {{ $tag->created_page }}</div>
                    </div>
                    <div class="col-4 tablet">
                        @if($tag->image)
                            <a class="images_popup" href="{{ $tag->image }}">
                                <img src="{{ $tag->image}}" alt="">
                            </a>
                        @else
                            <img wsrc="{{asset('/images/default-200x200.png') }}" alt="">
                        @endif
                    </div>
                    <div class="col-12">
                        @if(!empty($tag->url_id))
                            @foreach ($tagsName->tags($tag->url_id) as $tag)
                                @if(!empty($tag))
                                <span class="tags-link">
                                    <a href="/tag/{{$tag->slug}}"
                                       style="text-decoration: none">{{$tag->tag}}</a>
                                </span>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </li>
            @endforeach
            <div class="col-lg-12 text-center">
                <ul class="pagination modal-1" style="text-align: center">
                    {{ $tags->links('paginate_one') }}
                </ul>
            </div>
        @endif
    </ul>
</div>
