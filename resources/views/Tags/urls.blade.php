@extends('layouts.app')

@php
     $tagName = (isset($tagsName)) ? $tagsName->tag : '';
@endphp

@section('meta_title')
    {{ $tagName }} на Unni.io
@endsection

@section('meta_description')
    Новости о {{ $tagName }} в ленте новостей на Unni.io в Украине, поиск ИТ- компаний, поиск стартапов и новостей мира технологий
@endsection

@section('content')
    <div class="container pageTags">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <h3 class="panel-heading textCenter">
                        Найденные страницы с тегом <strong>{{ $tagName }}</strong>
                    </h3>
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                            <div class="mainBlocks">
                                <div class="list_result">
                                    <div class="searchPageAjax">
                                        @include('Tags.loadUrls')
                                    </div>

                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
