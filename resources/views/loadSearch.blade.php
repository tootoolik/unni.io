<ul id="load">
    @if(!empty($resultSearch))
        @foreach($resultSearch as $account)
            <li>
                <div class="col-8 tablet">
                    @if(!empty($account['_source']['url']))
                        <div>
                            <h3 class="title-blok">{{$account['_source']['title']}} </h3>
                            <noindex>
                                <a href="/link?i={{$account['_source']['url']}}" target="_blank" rel="nofollow">
                                    <i class="fas fa-external-link-alt"></i>
                                </a>
                            </noindex>
                        </div>
                        <div>
                            <a id="list_result_ul_chanel"
                               href="domains/{{ str_replace('www.', '', parse_url($account['_source']['url'], PHP_URL_HOST)) }}">
                                {{'@ '.parse_url($account['_source']['url'], PHP_URL_HOST)}}
                            </a>
                        </div>
                    @endif
                    <div>
                        @if($account['_source']['description'] != 1)
                            {{$account['_source']['description']}}
                        @endif
                    </div>
                    <div class="dataindex">Дата:
                        @if(isset($account['_source']['created_at']['date']))
                            {{ \Carbon\Carbon::parse($account['_source']['created_at']['date'])->format('d.m.Y') }}
                        @else
                            no date
                        @endif
                    </div>
                    <div>
                        @foreach($tags[$account['_source']['id_url']] ?? [] as $tag)
                            <span class="tags-link">
                                <a href="{{ route('tags.urls', $tag->slug) }}">{{$tag->tag}}</a>
                            </span>
                        @endforeach
                    </div>
                </div>
{{--                @if(isset($account['_source']['image']))--}}
                    <div class="col-4 tablet">
                        <div><a href="{{ $account['_source']['image'] }}" class="images_popup">
                                <img src="{{ $account['_source']['image'] ?? asset('/images/default-200x200.png') }}" alt="">
                            </a>
                        </div>
                    </div>
{{--                @endif--}}
            </li>

        @endforeach

        @php
            $url = '?search_word=' . urlencode($searchWord);
        @endphp

        <script>
            $(document).ready(function () {
                gtag('config', 'UA-128551053-1', {'page_path': '{{ $url }}'});
            });
        </script>

    @endif

    @if ($paginates)
        <ul class="pagination modal-1" style="text-align: center">

            @if ($paginates->getPrevUrl())
                <li><a href="{{$paginates->getPrevUrl()}}">&laquo;</a></li>
            @endif

            @foreach ($paginates->getPages() as $page)
                @if ($page['url'])

                    <li {{($page['isCurrent']) ? 'class=active' : ''}}>
                        <a href="{{$page['url']}}">{{$page['num']}}</a>
                    </li>
                @else
                    <li class="disabled"><span>{{$page['num']}}</span></li>
                @endif
            @endforeach

            @if ($paginates->getNextUrl())
                <li><a href="{{$paginates->getNextUrl()}}">&raquo;</a></li>
            @endif
        </ul>
    @endif
</ul>
