<strong>DOMAIN ID: {{ $domain->id }}</strong>
<br>
<strong>DOMAIN TITLE: {{ $domain->domain }}</strong>
<br>
<br>
@foreach($domainContents as $content)
    @if(count($content->tags) > 0)
        <div style="padding: 20px;border: 2px solid black; margin-bottom: 20px">
            <div><strong>jeweler_content</strong></div>
            <div style="padding: 5px">
                <div>id => {{ $content->id }}</div>
                <div>id_url => {{ $content->id_url }}</div>
                <div>title => {{ $content->title }}</div>
                <div>description => {{ $content->description }}</div>
                <div>image => {{ $content->image }}</div>
                <div>created_page => {{ $content->created_page }}</div>
                <div>laravel_through_key => {{ $content->laravel_through_key }}</div>
                <div>id_domain => {{ $content->id_domain }}</div>
            </div>
            <hr>
            <div><strong>auditor_url_children</strong></div>
            <div style="padding: 5px">
                <div>id => {{ $content->linksContent->id }}</div>
                <div>url => {{ $content->linksContent->url }}</div>
            </div>
            <hr>
            <div>
                <div><strong>tags</strong></div>
                @foreach($content->tags as $tag)
                    <div style="border-bottom: 1px solid #9a9a9a;width: 300px;margin-bottom: 5px; padding: 5px">
                        <div>id => {{ $tag->id }}</div>
                        <div>tag => {{ $tag->tag }}</div>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
@endforeach