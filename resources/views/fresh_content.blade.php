@foreach($freshContents as $content)
    @if ($content->image)
    <li class="list-group-item">

        <div class="row list-group-item-row">
            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 col-6 text-center">
                @if ($content->image)
                    <img class="rounded-circle fresh-content-img" src="{{ $content->image ?? '' }}" alt="">
                @else
                    <img class="rounded-circle fresh-content-img" src="/images/default-200x200.png" alt="">
                @endif
            </div>

            <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7 col-6">
                <div class="media-body">
                    <h3 class="title-blok">{{$content->title}}</h3>
                        <noindex>
                            <a href="/link?i={{ $content->linksContent->url ?? '' }}" target="_blank" rel="nofollow">
                                <i class="fas fa-external-link-alt"></i>
                            </a>
                        </noindex>

                    <div class="fresh-content-des">{{$content->description}}</div>
                    <div class="col-12" id="feed-tags">
                        @if(!empty($content->tags))
                            @foreach ($content->tags as $tag)
                                @if(!empty($tag->slug))
                                    <span>
                                            <a href="/tag/{{$tag->slug}}"
                                               class="tags-link domains-tag-link">{{$tag->tag}}</a>
                                    </span>
                                @endif
                            @endforeach
                        @endif
                    </div>
                    <div class="fresh-content-dates">
{{--                        <div class="text-muted smaller">Дата разбора страницы: {{$content->created_at}}</div>--}}
                        @if ($content->created_page)
                            <div class="text-muted smaller">Дата: {{$content->created_page}}</div>
                        @else
                            <div class="text-muted smaller">Дата создания страницы не известна</div>
                        @endif
                    </div>

                </div>
            </div>

        </div>
    </li>
    @endif
@endforeach
