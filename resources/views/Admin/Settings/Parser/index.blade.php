{{$adminURL = env('APP_ADMIN_URL', 'test')}}
@extends('Admin/layouts.app')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container">
        <status-parser-settings></status-parser-settings>
        <div class="row">
            <div class="col-12">
                <div class="panel panel-default">
                    <div class="panel-body delete-parser">
                        <form action="/test/settings/paser/stopstart" method="post">
                            {{ csrf_field() }}
                            @if($mainParser->level == 0)
                                <input type="hidden" value="1" name="stop">
                                <button class="btn btn-xs btn-danger"
                                        onclick="alert('Вы уверены что хотите остановить парсер?')">Остановить
                                </button>
                            @elseif($mainParser->level == 1)
                                <input type="hidden" value="1" name="start">
                                <button class="btn btn-xs btn-success"
                                        onclick="alert('Вы уверены что хотите запустить парсер с места последнего завершения?')">
                                    Запустить
                                </button>
                            @endif
                        </form>
                        <form action="/test/settings/paser/delete" method="post">
                            {{ csrf_field() }}
                            <div class="col-lg-12">
                                <h1>Удаление парсера</h1>
                                <div class="card-header">Выберите индексы ElasticSearch для удаления</div>
                                @foreach($elastics as $elastic)
                                    <div class="toggle-button toggle-button--vesi">
                                        <input id="{{$elastic->index_elastic}}" type="checkbox" name="elasticIndexs[]"
                                               value="{{$elastic->index_elastic}}">
                                        <label for="{{$elastic->index_elastic}}"
                                               data-on-text="Удалить {{$elastic->index_elastic}}"
                                               data-off-text="Не удалять {{$elastic->index_elastic}}"></label>
                                        <div class="toggle-button__icon"></div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="col-lg-12">
                                <div class="card-header">Выбрать таблицы для удаления</div>
                                <div class="toggle-button toggle-button--vesi">
                                    <input id="AuditorUrls" type="checkbox" name="models[]" value="AuditorUrls">
                                    <label for="AuditorUrls" data-on-text="Удалить ссылки"
                                           data-off-text="Не удалять ссылки"></label>
                                    <div class="toggle-button__icon"></div>
                                </div>
                                <div class="toggle-button toggle-button--vesi">
                                    <input id="AuditorErrorUrls" type="checkbox" name="models[]" value="AuditorErrorUrls">
                                    <label for="AuditorErrorUrls" data-on-text="Удалить ошибки"
                                           data-off-text="Не удалять ошибки"></label>
                                    <div class="toggle-button__icon"></div>
                                </div>
                                <div class="toggle-button toggle-button--vesi">
                                    <input id="AuditorContent" type="checkbox" name="models[]" value="AuditorContent">
                                    <label for="AuditorContent" data-on-text="Удалить html контент"
                                           data-off-text="Не удалять html контент"></label>
                                    <div class="toggle-button__icon"></div>
                                </div>
                                <div class="toggle-button toggle-button--vesi">
                                    <input id="JewelerContent" type="checkbox" name="models[]" value="JewelerContent">
                                    <label for="JewelerContent" data-on-text="Удалить контент"
                                           data-off-text="Не удалять контент"></label>
                                    <div class="toggle-button__icon"></div>
                                </div>
                                <div class="toggle-button toggle-button--vesi">
                                    <input id="CountParser" type="checkbox" name="models[]" value="CountParser">
                                    <label for="CountParser" data-on-text="Удалить счетчик"
                                           data-off-text="Не удалять счетчик"></label>
                                    <div class="toggle-button__icon"></div>
                                </div>
                                <div class="toggle-button toggle-button--vesi">
                                    <input id="Tags" type="checkbox" name="models[]" value="Tags">
                                    <label for="Tags" data-on-text="Удалить теги"
                                           data-off-text="Не удалять теги"></label>
                                    <div class="toggle-button__icon"></div>
                                </div>
                            </div>
                            <button class="btn btn-xs btn-danger"
                                    onclick="alert('Вы уверене что хотите удалить всю информацию. Действие безвозвратно')">
                                Удалить
                            </button>

                            <style>
                                .dropdown-delete-db-tables .dropdown-toggle::after{
                                    border-right: none;
                                    border-left: none;
                                    margin-left: 0;
                                    margin-right: -4px;
                                }
                                .dropdown-delete-db-tables .btn{
                                    background: white;
                                }
                                .dropdown-delete-db-tables .dropdown-menu.show{
                                    width: 390px;
                                }
                                .dropdown-delete-db-tables-select,
                                .dropdown-delete-db-tables-always,
                                .dropdown-delete-db-tables-clear{
                                    padding-left: 30px;
                                }
                                .dropdown-delete-db-tables-box p{
                                    margin-bottom: 0;
                                    padding-left: 15px
                                }
                                .dropdown-delete-db-tables-box{
                                    margin-left: 50px;
                                }
                                .dropdown-delete-db-tables-last-item{
                                    display: block;
                                    margin-top: 10px;
                                }
                            </style>

                            <div class="dropdown dropdown-delete-db-tables">
                                <button class="btn btn-default dropdown-toggle" type="button" id="delete_db_tables" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <i class="far fa-question-circle"></i>
                                </button>

                                <div class="dropdown-menu dropdown-delete-db-tables-box" aria-labelledby="delete_db_tables">
                                    <p>В зависимости от выбора удалятся:</p>
                                    <div class="dropdown-delete-db-tables-select">
                                        <small>Удалить ссылки - <strong>auditor_url_children</strong></small>
                                        <br>
                                        <small>Удалить ошибки - <strong>auditor_error_url</strong></small>
                                        <br>
                                        <small>Удалить HTML контент - <strong>auditor_content</strong></small>
                                        <br>
                                        <small>Удалить контент - <strong>jeweler_content</strong></small>
                                        <br>
                                        <small>Удалить счетчик - <strong>auditor_count</strong></small>
                                        <br>
                                        <small>Удалить теги - <strong>не работает</strong></small>
                                        <br>
                                        <small class="dropdown-delete-db-tables-last-item">Если были выбраны индексы Elastic,<br>то они будут удалены из <strong>своих репозиториев</strong>
                                            <br>
                                            и из таблицы <strong>elastic</strong>
                                        </small>
                                    </div>
                                    <hr style="margin: 8px 0">
                                    <div>
                                        <p>Удалятся в любом случае:</p>
                                        <div class="dropdown-delete-db-tables-always">
                                            <small><strong>auditor_url_anhors</strong></small>
                                            <br>
                                            <small><strong>tags_domain</strong></small>
                                            <br>
                                            <small><strong>tags_count</strong></small>
                                            <br>
                                            <small><strong>tags_section</strong></small>
                                            <br>
                                            <small><strong>tags_urls</strong></small>
                                            <br>
                                            <small class="dropdown-delete-db-tables-last-item">
                                                <strong>ВСЕ</strong> таблицы из базы <strong>unni_tmp_url</strong>
                                            </small>
                                        </div>
                                    </div>
                                    <hr style="margin: 8px 0">
                                    <div>
                                        <p>Очистятся данные:</p>
                                        <div class="dropdown-delete-db-tables-clear">
                                            <small>
                                                Таблица <strong>auditor_url_anhors</strong>
                                                колонка <strong>count</strong>
                                                - обнулится
                                            </small>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="card-header">Настройки парсера</div>
                <form action="/test/settings/paser/setting" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="type" value="parser">
                    <div class="col-lg-12">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="">Количество ссылок</label>
                                <input type="number" class="form-control" name="count_links" placeholder="10">
                                <small class="form-text text-muted">При каждом проходе цикла, для одного домена</small>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="">Ожидание ответа в секундах</label>
                                <input type="number" class="form-control" name="time_timeout" placeholder="10">
                                <small class="form-text text-muted">Время ожидание запроса ссылки</small>
                            </div>
                        </div>
                        <button class="btn btn-xs btn-success">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection