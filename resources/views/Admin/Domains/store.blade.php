@extends('Admin/layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 domainPage">
                <h1 class="col-lg-12">Информация домена {{ $domain->domain }}</h1>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="container alert-info">
                                        <div class="row">
                                            <div class="col-lg-7">Дата добавление</div>
                                            <div class="col-lg-5">{{ $domain->created_at }}</div>
                                            <div class="col-lg-7">Ответ сервера</div>
                                            <div class="col-lg-5">{{ $domain->ping }}</div>
                                            <div class="col-lg-7">Приоритет</div>
                                            <div class="col-lg-5">{{ $domain->priority }}</div>
                                            <div class="col-lg-7">Статус для парсера</div>
                                            <div class="col-lg-5">
                                                @if($domain->parser_content == 1)
                                                    Включено
                                                    @else
                                                Выключен
                                                    @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <!-- Навигация -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="active">
                                    <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Ошибки</a>
                                </li>
                                <li>
                                    <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Управление ссылками</a>
                                </li>
                            </ul>
                            <!-- Содержимое вкладок -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">
                                    <table class="table table-bordered table-striped table-hover table-condensed">
                                        <thead>
                                        <tr>
                                            <th>Url id</th>
                                            <th>Url</th>
                                            <th>№ ошибки</th>
                                            <th>Сообщение ошиби</th>
                                            <th>Дата ошибки</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($errorsDomains as $errorsDomain)
                                            <tr>
                                                <td>{{$errorsDomain->url_id}}</td>
                                                <td><a href="{{$errorsDomain->errorsUrl['url']}}" target="_blank">Открыть ссылку</a></td>
                                                <td>{{$errorsDomain->error_code}}</td>
                                                <td>{{$errorsDomain->message}}</td>
                                                <td>{{$errorsDomain->created_at}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{ $errorsDomains->links('paginate') }}
                                </div>
                                <div role="tabpanel" class="tab-pane col-lg-12" id="profile">
                                    <form action="{{ route('admin.domains.error.parser') }}" method="post">
                                        {{ csrf_field() }}
                                        <h5>Пересканировать ссылки с ответом сервера</h5>
                                        @if (empty($errorsCodies[0]))
                                            <div class="col-lg-12 alert-success">
                                                Нет страниц с ошибками
                                            </div>
                                        @endif
                                        <div class="container">
                                            <div class="row">
                                                @foreach($errorsCodies as $errorsCode)
                                                    <div class="col-lg-1">
                                                        <label class="container tooltip_custom custom_form">
                                                            <span class="tooltiptext">{{ $errorsCode->message }}</span>
                                                            {{ $errorsCode->error_code }}
                                                            <input type="checkbox" name="errors[]" value="{{ $errorsCode->error_code }}">
                                                            <span class="checkmark"></span>
                                                        </label>
                                                    </div>
                                                @endforeach
                                                    <input type="hidden" value="{{ $domain->id }}" name="domain">
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <button class="btn btn-success">Отправить</button>
                                        </div>
                                    </form>
                                    <form action="{{ route('admin.domains.error.parser') }}" method="post">
                                        {{ csrf_field() }}
                                        <h5 class="col-lg-12">Пересканировать с поиском по тексту</h5>
                                        <div class="container alert-primary">
                                            <div class="row">
                                                <small class="col-lg-12">
                                                    Поиск будет происходить в поле description
                                                    <br>Это глобальный поиск по всем доменам
                                                </small>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <input type="text" name="search_text">
                                        </div>
                                        <div class="col-lg-12">
                                            <button class="btn btn-success">Отправить</button>
                                        </div>
                                    </form>
                                    <form action="{{ route('admin.domains.error.parser') }}" method="post">
                                        {{ csrf_field() }}
                                        <h5 class="col-lg-12">Удалить весь контент для домена</h5>
                                        <div class="container alert-primary">
                                            <div class="row">
                                                <small class="col-lg-12">
                                                    Удаляется весь контент, теги и так далее
                                                </small>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <input type="hidden" name="all_content" value="all_content">
                                            <input type="hidden" value="{{ $domain->id }}" name="domain">
                                        </div>
                                        <div class="col-lg-12">
                                            <button class="btn btn-danger">Удалить</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection