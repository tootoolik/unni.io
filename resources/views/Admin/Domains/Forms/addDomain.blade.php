<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Заголовок окна</h4>
                <button class="close" type="button" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <div id="slider"></div>
                <div id="custom-handle" class="ui-slider-handle">
                            <span class="value-wrapper">
                                Позиция<span class="value-output"></span>
                            </span>
                </div>
                <form action="" id="addDomain">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="email">
                            Укажите адрес домена без http или https.
                        </label>
                        <input type="text" name="domain" class="form-control" placeholder="Пример dou.ua">
                        <input type="hidden" class="positionDomain" name="positionDomain" value="">
                    </div>
                    <button class="change btn btn-success">
                        Добавить
                    </button>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default closeDomain" type="button" data-dismiss="modal">
                    Закрыть
                </button>
            </div>
        </div>
    </div>
</div>
<script>
    $(function() {
        var handle = $("#custom-handle .value-output");
        $("#slider").slider({
            min: 0,
            max: {{$maxPosition+1}},
            create: function() {
                handle.text($(this).slider("value"));
            },
            slide: function(event, ui) {
                handle.text(ui.value);
                $( ".positionDomain" ).attr('value', ui.value);
            }
        });
    });
</script>