@extends('Admin/layouts.app')
@section('content')

    <div class="row">
        <div class="col-12">
            <h1>{{ isset($channel) ? 'Редактировать запись' : 'Создать запись' }}</h1>
            <div class="panel panel-default">
                <div class="panel-body">
                    @if (isset($channel))

                    <form action="{{ route('admin.channels.update', $channel->id) }}" method="post">
                        <input type="hidden" name="_method" value="PUT">

                    @else
                    <form action="{{ route('admin.channels.store') }}" method="post">

                    @endif

                        {{ csrf_field() }}

                        <div class="form-group col-lg-4">
                            <label for="">Название</label>
                            <input type="text" name="name" class="form-control" value="{{ isset($channel) ? $channel->name : '' }}">
                        </div>

                        <div class="form-group col-lg-4">
                            <label for="">Ссылка на сайт</label>
                            <input type="text" name="website" class="form-control" value="{{ isset($channel) ? $channel->website : '' }}">
                        </div>

                        <div class="form-group col-lg-4">
                            <label for="">FB id</label>
                            <input type="text" name="fb_group_id" class="form-control" value="{{ isset($channel) ? $channel->fb_group_id : '' }}">
                        </div>

                        <div class="form-group col-lg-4">
                            <label for="description">Описание</label>
                            <textarea class="form-control" id="description" rows="8" name="body_intro">{{ isset($channel) ? $channel->body_intro : ''}}</textarea>
                        </div>

{{--                        <div class="form-group col-lg-4">--}}
{{--                            <label for="">Домен</label>--}}
{{--                            <select class="domain-input" name="domains">--}}
{{--                                @if(isset($channel->id))--}}
{{--                                    @foreach ($domains as $domain)--}}
{{--                                        <option value="{{ $domain->id }}" selected>{{ $domain->domain }}</option>--}}
{{--                                    @endforeach--}}
{{--                                @endif--}}
{{--                            </select>--}}
{{--                        </div>--}}

                        <div class="form-group col-lg-4">
                            <label for="">Теги</label>
                            <select class="tags-input" multiple name="tags[]">
                                @if(isset($channel->id))
                                    @foreach ($tags as $tag)
                                        <option value="{{ $tag->id }}" selected>{{ $tag->tag }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="form-group col-lg-4">
                            <button class="btn btn-success">
                                @if (isset($channel->id))
                                    Сохранить
                                @else
                                    Создать
                                @endif
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer_script')
    <link href="/admin/css/tokenize2.min.css" rel="stylesheet">
    <script src="/admin/js/tokenize2.min.js"></script>
@stop