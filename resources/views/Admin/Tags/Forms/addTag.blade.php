<div class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Добавление тега</h4>
            </div>
            <div class="modal-body">
                <form action="" id="addTag">
                    {{ csrf_field() }}
                    <div class="form-group addTag">
                        <label for="addTag">
                            Укажите тег - поисковый запрос
                        </label>
                        <input type="text" name="addTag" value="">
                    </div>

                    <div class="modal-footer">
                        <button class="change btn btn-success tagSend">
                            Добавить
                        </button>
                        <button class="btn btn-default closeModal" type="button" data-dismiss="modal">
                            Закрыть
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>