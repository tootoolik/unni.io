{{$adminURL = env('APP_ADMIN_URL', 'test')}}
@extends('Admin/layouts.app')
@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-md-6 col-12">
            <h1>Редактирование тега:</h1>
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="{{ route('admin.tags.edit_post',$tag->id) }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group addTag">
                            <label for="Tag" class="addTagBlocks">
                                Наименование Tag
                            </label>
                            <div class="form-group">
                                <input type="text" name="tag" value="{{$tag->tag}}" class="form-control" max="128">
                            </div>
                        </div>
                        <div class="form-group addTag">
                            <label for="Tag" class="addTagBlocks">
                                Slug/url
                            </label>
                            <div class="form-group">
                                <input type="text" name="slug" value="{{$tag->slug}}" class="form-control" max="128">
                            </div>
                        </div>
                        <input type="hidden" name="id" value="{{$tag->id}}">
                        <button class="change btn btn-success tagSend">
                            Сохранить
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
