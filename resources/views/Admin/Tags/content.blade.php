<table class="table table-bordered table-striped">
    <thead class="text-center">
    <tr>
        <th><a href="?sort=id" title="Новые с начала">#</a></th>
        <th>Название</th>
        <th>Slug/Url</th>
        <th>Популярность</th>
        <th>Тип добавления</th>
        <th>Создан</th>
        <th>Показывать на главной?</th>
        <th width="100">&nbsp;</th>
    </tr>
    </thead>
    <tbody>
    @foreach($tags as $tag)
        <tr class="tag-box" data-id="{{ $tag->id }}">
            <td class="text-center">{{$tag->id}}</td>
            <td class="text-left">{{$tag->tag}}</td>
            <td class="text-left">{{$tag->slug}}</td>
            <td class="text-center">{{$tag->count}}</td>
            <td class="text-center">{{ $tag->type == 'parser' ? 'Парсер' : 'Вручную'}}</td>
            <td class="text-center">{{ $tag->created_at}}</td>
            @if($tag->show)
                <td class="text-success text-center tag-show-switch-box">
                    <i class="fas fa-toggle-on fa-2x tag-show-switch"></i>
                </td>
            @else
                <td class="text-danger text-center tag-show-switch-box">
                    <i class="fas fa-toggle-on fa-rotate-180 inactive fa-2x tag-show-switch"></i>
                </td>
            @endif
            <td>
                <a href="{{route('admin.tags.edit_get',$tag->id)}}"
                   class="btn btn-xs btn-outline-primary addTagButton text-left">
                    Редактировать
                </a>

                <a href="/test/tags/delete/{{$tag->id}}"
                   class="btn btn-xs btn-outline-danger text-right">
                    Удалить
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>



