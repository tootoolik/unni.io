@extends('Admin/layouts.app')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="container">
                <h1>Редактирование страницы</h1>
                <form method="POST" action="{{$page->id}}">
                    <div class="form-group">
                        <label for="">Название статьи</label>
                        <input class="form-control" type="text" name="title" value="{{$page->title}}">
                    </div>
                    <div class="form-group">
                        <label for="">Текст статьи</label>
                        <textarea class="form-control" name="content" id="content">{{$page->content}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="">description</label>
                        <input class="form-control" type="text" name="meta_description" value="{{$page->meta_description}}">
                    </div>
                    <div class="form-group">
                        <label for="">keywords</label>
                        <input class="form-control" type="text" name="meta_keywords" value="{{$page->meta_keywords}}">
                    </div>
                    <div class="form-group">
                        <label for="">slug</label>
                        <input class="form-control" type="text" name="slug" value="{{$page->slug}}">
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label for="">Опубликовать?</label>
                            <select class="form-control" name="public">
                                @if($page->public == 1)
                                    <option value="1" selected>Да</option>
                                    <option value="0">Нет</option>
                                @elseif($page->public == 0)
                                    <option value="1">Да</option>
                                    <option value="0" selected>Нет</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="submit" value="Сохранить" class="btn btn-success">
                </form>
            </div>
        </div>
    </div>
    <script src="{{ asset('/ckeditor/ckeditor.js') }}"
            type="text/javascript" charset="utf-8" ></script>
    <script>
        var editor = CKEDITOR.replace( 'content',{
            filebrowserBrowseUrl : '/elfinder/ckeditor'
        } );
    </script>
@endsection