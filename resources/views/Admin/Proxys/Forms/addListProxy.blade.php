<div id="addProxyForm" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Добавить список proxy</h4>
            </div>
            <div class="modal-body">
                <form action="" id="addProxySend" class="ProxySendForms">
                    {{ csrf_field() }}
                    <div class="form-group addTag">
                        <label for="addTag">
                            Укажите список прокси, каждый с новой строки
                        </label>
                        <textarea name="listProxy" id="" cols="30" rows="10"></textarea>
                    </div>

                    <div class="modal-footer">
                        <button class="change btn btn-success proxySend">
                            Добавить
                        </button>
                        <button class="btn btn-default closeModal" type="button" data-dismiss="modal">
                            Закрыть
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>