@extends('Admin/layouts.app')
@section('content')

    <div class="row">
        <div class="col-12">
            <h1>Редактировать запись</h1>
            <div class="panel panel-default">
                <div class="panel-body">
                    <form method="POST" enctype="multipart/form-data" action="{{ $post->id }}">

                        <div class="form-group col-lg-4">
                            <label for="">Заголовок</label>
                            <input type="text" name="name" class="form-control" value="{{ isset($post) ? $post->name : '' }}">
                        </div>

                        <div class="form-group col-lg-12">
                            <label for="description">Контент</label>
                            <textarea class="form-control" name="post_content" id="post_content">{{ isset($post) ? $post->body : ''}}</textarea>
                        </div>

                        <div class="form-group col-lg-4">
                            <label for="post_img">
                                Изображени поста
                                <br>
                                @if( $post->post_img != NULL || $post->post_img != '' )
                                    <img src="/storage/{{ $post->post_img }}" alt="img" width="150" class="pb-3">
                                @endif
                            </label>

                            <input type="file" class="form-control-file" id="post_img"  name="post_img">
                        </div>

                        <div class="form-group col-lg-4">
                            <label for="">Теги</label>
                            <select class="tags-input" multiple name="tags[]">
                                @if(isset($tags))
                                    @foreach ($tags as $tag)
                                        <option value="{{ $tag->id }}" selected>{{ $tag->tag }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="form-group col-lg-4">
                            <label for="">Автор</label>
                            <select class="authors-input" multiple name="post_author[]">
                                @if(isset($authors))
                                    @foreach ($authors as $author)
                                        <option value="{{ $author->id }}" selected>{{ $author->name }} ({{ $author->email }})</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="form-group col-lg-4">
                            <label for="">Опубликовать?</label>
                            <select class="form-control" name="public">
                                @if($post->active == 1)
                                    <option selected value="1">Да</option>
                                    <option value="0">Нет</option>
                                @elseif($post->active == 0)
                                    <option value="1">Да</option>
                                    <option selected value="0">Нет</option>
                                @endif
                            </select>
                        </div>

                        <div class="form-group col-lg-12">
                            <button class="btn btn-success">Сохранить</button> <a href="{{ route('admin.posts') }}" class="btn btn-danger">Закрыть</a>
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer_script')
    <script src="{{ asset('/ckeditor/ckeditor.js') }}"
            type="text/javascript" charset="utf-8" ></script>
    <script>
        var editor = CKEDITOR.replace( 'post_content',{
            filebrowserBrowseUrl : '/elfinder/ckeditor'
        } );
    </script>
    <link href="/admin/css/tokenize2.min.css" rel="stylesheet">
    <script src="/admin/js/tokenize2.min.js"></script>
@stop