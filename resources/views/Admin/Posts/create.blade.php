@extends('Admin/layouts.app')
@section('content')
    <div class="row">
        <div class="col-12">
            <h1>Создать пост</h1>
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="store" enctype="multipart/form-data" method="post">

                        <div class="form-group col-lg-12">
                            <label for="">Заголовок</label>
                            <input type="text" name="name" class="form-control">
                        </div>

                        <div class="form-group col-lg-12">
                            <label for="content">Контент</label>
                            <textarea class="form-control" name="post_content" id="post_content"></textarea>
                        </div>

                        <div class="form-group col-lg-6">
                            <label for="">Изображени поста</label>
                            <input type="file" name="post_img">
                        </div>

                        <div class="form-group col-lg-6">
                            <label for="">Теги</label>
                            <select class="tags-input col-lg-12" multiple name="tags[]">

                            </select>
                        </div>

                        <div class="form-group col-lg-4">
                            <label for="">Опубликовать?</label>
                            <select class="form-control" name="public">
                                <option value="1">Да</option>
                                <option selected value="0">Нет</option>
                            </select>
                        </div>

                        <div class="form-group col-lg-12">
                            <button class="btn btn-success">Создать публикацию</button> <a href="{{ route('admin.posts') }}" class="btn btn-danger">Закрыть</a>
                        </div>

                        <input type="hidden" name="post_author[]" value="{{$user}}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer_script')
    <script src="{{ asset('/ckeditor/ckeditor.js') }}"
            type="text/javascript" charset="utf-8" ></script>
    <script>
        var editor = CKEDITOR.replace( 'post_content',{
            filebrowserBrowseUrl : '/elfinder/ckeditor'
        } );
    </script>
    <script src="/admin/vendor/select/selectstyle.js"></script>
    <link href="/admin/vendor/select/selectstyle.css" rel="stylesheet" type="text/css">
@stop