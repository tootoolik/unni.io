@extends('Admin/layouts.app')

@section('content')
    <div class="row seo-box-row">

        <h3>Создать SEO текст</h3>

        <div class="col-lg-12 pt-2">

            <form method="POST" action="{{ route('admin.seo.store') }}" class="mb-5">

                {{ csrf_field() }}

                <div class="form-group">
                    <label for="seo-url"><strong>URL</strong></label>
                    <input type="text" class="form-control" name="url" id="seo-url" placeholder="Введите URL" value="{{ old('url') }}">
                    @if ($errors->has('url'))
                        <span class="help-block">
                            <strong>{{ $errors->first('url') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="seo-meta-title"><strong>Мета title</strong></label>
                    <input type="text" class="form-control" name="meta_title" id="seo-meta-title" placeholder="Введите мета title" value="{{ old('meta_title') }}">
                    @if ($errors->has('meta_title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('meta_title') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="seo-meta-description"><strong>Мета description</strong></label>
                    <textarea class="form-control" id="seo-meta-description" rows="3" name="meta_description" placeholder="Введите мета description">{{ old('meta_description') }}</textarea>
                    @if ($errors->has('meta_description'))
                        <span class="help-block">
                            <strong>{{ $errors->first('meta_description') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="seo-title"><strong>Короткое описание</strong></label>
                    <textarea class="form-control" id="seo-title" rows="3" name="title" placeholder="Введите короткое описание">{{ old('title') }}</textarea>
                    @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="seo-description"><strong>Общее описание</strong></label>
                    <textarea class="form-control" id="seo-description" rows="3" name="description" placeholder="Введите полное описание">{{ old('description') }}</textarea>
                    @if ($errors->has('description'))
                        <span class="help-block">
                            <strong>{{ $errors->first('description') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-check form-check-inline mt-4">
                    <input class="form-check-input" type="radio" name="active" id="active-on" value="1" checked>
                    <label class="form-check-label" for="inlineRadio1"><strong>Вкл</strong></label>
                </div>

                <div class="form-check form-check-inline mb-4">
                    <input class="form-check-input" type="radio" name="active" id="active-off" value="0">
                    <label class="form-check-label" for="inlineRadio2"><strong>Выкл</strong></label>
                </div>

                @if ($errors->has('active'))
                    <span class="help-block">
                        <strong>{{ $errors->first('active') }}</strong>
                    </span>
                @endif

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>

            </form>

        </div>
    </div>
@endsection