@foreach($seoItems as $item)
    <tr data-id="{{ $item->id }}" class="item-box">
        <td>{{ $item->id }}</td>
        <td>
            <a href="{{ url('/') }}{{ $item->url }}" target="_blank">{{ $item->url }}</a>
        </td>
        @if($item->active)
            <td class="text-success">
                <i class="fas fa-toggle-on fa-2x seo-item-active"></i>
            </td>
        @else
            <td class="text-danger">
                <i class="fas fa-toggle-on fa-rotate-180 inactive fa-2x seo-item-active"></i>
            </td>
        @endif
        <td>
            <a href="{{ route('admin.seo.edit', $item->id) }}" class="btn btn-outline-info btn-sm">Редактировать</a>
        </td>
        <td>
            <form method="POST" action="{{ route('admin.seo.destroy', ['id' => $item->id]) }}" class="mb-0">

                {{ csrf_field() }}
                {{ method_field('DELETE') }}

                <div class="form-group mb-0">
                    <button class="btn btn-outline-danger btn-sm delete-seo-item">Удалить</button>
                </div>
            </form>
        </td>
    </tr>
@endforeach
