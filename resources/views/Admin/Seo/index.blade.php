@extends('Admin/layouts.app')

@section('content')
    <h3>SEO тексты</h3>

    @if($successMsg = session('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ $successMsg }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <br>

    <div class="row seo-add-btn-box">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.seo.create') }}">Создать</a>
        </div>
    </div>

    <table class="table table-bordered">
        <thead class="text-center">
            <tr>
                <th>ID</th>
                <th>URL</th>
                <th>Вкл/Выкл</th>
                <th>Редактировать</th>
                <th>Удалить</th>
            </tr>
        </thead>
        <tbody class="text-center" id="table-seo-items">
            @include('Admin.Seo.table_items')
        </tbody>
    </table>
@endsection