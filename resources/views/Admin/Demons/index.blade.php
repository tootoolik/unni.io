{{$adminURL = env('APP_ADMIN_URL', 'test')}}
@extends('Admin/layouts.app')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <h1>Вызов демонов</h1>
            <div class="form-group">
                <a href="/{{$adminURL}}/demons/create" class="btn btn-success">Призвать демона</a>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Pid демона</th>
                            <th>Статус</th>
                            <th>Команда</th>
                            <th>Действие</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($allDeamons as $allDeamon)
                            <tr>
                                <td>{{$allDeamon->id}}</td>
                                <td>
                                    @if($allDeamon->id_pid <= 0)
                                        Pid не получен
                                    @else
                                        {{$allDeamon->id_pid}}
                                    @endif
                                </td>
                                <td>
                                    @if($allDeamon->work == 1)
                                    <i class="fas fa-spinner fa-spin" style="color:#28a745"></i>
                                        @elseif($allDeamon->work == 0)
                                        <i class="far fa-times-circle" style="color:#dc3545"></i>
                                    @endif
                                </td>
                                <td>{{$allDeamon->command}}</td>
                                <td>
                                    <div class="dropdown">
                                        <span>Выполнить действие</span>
                                        <div class="dropdown-content">
                                            <a href="demons/destroy/{{$allDeamon->id}}-{{$allDeamon->id_pid}}" class="btn btn-xs btn-outline-secondary">
                                                <i class="fas fa-trash-alt">Удалить</i>
                                            </a>
                                            <a href="demons/edit/{{$allDeamon->id}}" class="btn btn-xs btn-outline-secondary">
                                                <i class="fas fa-cog fa-spin"></i> Настройки
                                            </a>
                                            <a href="demons/start/{{$allDeamon->id}}" class="btn btn-xs btn-outline-secondary">
                                                <i class="fa fa-space-shuttle faa-passing animated">Запустить</i>
                                            </a>
                                            <a href="demons/stop/{{$allDeamon->id}}" class="btn btn-xs btn-outline-secondary">
                                                Остановить
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{--{{ $dictionary->links('paginate') }}--}}
                </div>
            </div>
        </div>
    </div>

@endsection