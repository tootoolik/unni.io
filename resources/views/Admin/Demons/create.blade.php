@extends('Admin/layouts.app')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="container">
                <h1>Создание демона</h1>
                <form method="POST" action="store" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="">Имя демона</label>
                        <input class="form-control" type="text" name="title">
                    </div>
                    <div class="form-group">
                        <label for="">Какую команду я должен выполнить?</label>
                        <input class="form-control" type="text" name="command"><br>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label for="">Начать работу немедленно?</label>
                            <select class="form-control" name="work">

                                <option value="1">Да</option>

                                <option value="0">Нет</option>

                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="submit" value="Сохранить" class="btn btn-success">
                </form>
            </div>
        </div>
    </div>
@endsection