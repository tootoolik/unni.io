@extends('Admin/layouts.app')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container tagsPage">
        <div class="row">
            <div class="col-12">
                <div class="panel panel-default">
                    <div class="panel-body delete-parser">
                        <h1>Теги по популярности</h1>
                        <div class="col-lg-12 col-xs-12">
                            @include('Admin.Statistics.blocks.popular_tags')
                        </div>
                        <h1>Обновленные теги</h1>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6 col-xs-12">
                                    @include('Admin.Statistics.blocks.table_update_tags')
                                </div>
                                <div class="col-lg-6 col-xs-12">
                                    @include('Admin.Statistics.blocks.chart_update_tags')
                                </div>
                            </div>
                        </div>
                        <h1>Новые теги</h1>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6 col-xs-12">
                                    @include('Admin.Statistics.blocks.table_new_tags')
                                </div>
                                <div class="col-lg-6 col-xs-12">
                                    @include('Admin.Statistics.blocks.chart_new_tags')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection