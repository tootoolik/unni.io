@extends('Admin/layouts.app')
@section('content')
    <h3>Процессы</h3>
    <table class="table table-hover text-center">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Имя</th>
                <th scope="col">Описание</th>
                <th scope="col">Вкл/Выкл логирование</th>
            </tr>
        </thead>
        <tbody class="statistic-parser-log-process-list">
           @include('Admin.Statistics.ParserLog.processes_list')
        </tbody>
    </table>
@endsection