@foreach($processes as $process)
    <tr class="statistics-parser-log-process-container" data-id="{{ $process->id }}">
        <td>{{$process->id }}</td>

        <td>
            <a href="{{ route('admin.statistics.parser.logs.process', [$process->id]) }}">
                {{$process->process_name }}
            </a>
        </td>

        <td>{{$process->description }}</td>

        @if($process->is_active_logger)
            <td class="text-success">
                <i class="fas fa-toggle-on fa-2x statistic-parser-log-switch-active"></i>
            </td>
        @else
            <td class="text-danger">
                <i class="fas fa-toggle-on fa-rotate-180 inactive fa-2x statistic-parser-log-switch-active"></i>
            </td>
        @endif
    </tr>
@endforeach