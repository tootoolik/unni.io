@extends('Admin/layouts.app')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container tagsPage">
        <div class="row">
            <div class="col-12">
                <div class="panel panel-default">
                    <div class="panel-body delete-parser">
<statistics-domains></statistics-domains>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection