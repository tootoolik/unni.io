<table class="table">
    <thead>
    <tr>
        <th>Дата</th>
        <th>Кол-во</th>
    </tr>
    </thead>
    <tbody>
    @foreach($tagsNew as $tag)
        <tr>
            <td data-label="Время">{{ $tag->date }}</td>
            <td data-label="Кол-во">{{ $tag->views }}</td>
        </tr>
    @endforeach
    </tbody>
</table>