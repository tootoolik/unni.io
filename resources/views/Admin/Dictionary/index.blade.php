@extends('Admin/layouts.app')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <h1>Словарь подсказок</h1>
            <div class="form-group">
                <a href="/{{ env('APP_ADMIN_URL') }}/hint_with_dictionary/add" class="btn btn-success">Добавить список</a>
            </div>
            <div class="form-group">
                <a href="/{{ env('APP_ADMIN_URL') }}/hint_with_dictionary/add_file" class="btn btn-success">Добавить файлом</a>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Наименование</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($dictionary as $dict)
                            <tr>
                                <td>{{$dict->id}}</td>
                                <td>{{$dict->title}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $dictionary->links('paginate') }}
                </div>
            </div>
        </div>
    </div>

@endsection