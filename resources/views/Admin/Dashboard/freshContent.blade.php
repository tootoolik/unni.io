<div class="card mb-12">
    <div class="list-group list-group-flush small">
        @foreach($freshContents as $freshContent)
            @if(isset($freshContent->linksContent->url))
            <a class="list-group-item list-group-item-action" href="{{ $freshContent->linksContent->url }}" target="_blank">
                <div class="media">
                    @if ($freshContent->image)
                        <img class="d-flex mr-3 " src="{{ $freshContent->image }}" alt="">
                    @else
                        <img class="d-flex mr-3 " src="/images/default-200x200.png" alt="">
                    @endif
                    <div class="media-body">
                        <strong>{{$freshContent->title}}</strong>
                        <div>{{$freshContent->description}}</div>
                        <strong>David Miller Website</strong>.
                        <div class="text-muted smaller">Дата разбора страницы {{$freshContent->created_at}}</div>
                        @if ($freshContent->created_page)
                            <div class="text-muted smaller">Дата Создания страницы {{$freshContent->created_page}}</div>
                        @else
                            <div class="text-muted smaller">Дата Создания страницы не известна</div>
                        @endif
                    </div>
                </div>
            </a>
            @endif
        @endforeach
    </div>
    <div class="card-footer small text-muted">Обновлено в </div>
</div>
