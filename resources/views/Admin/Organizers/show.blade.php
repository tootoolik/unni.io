{{$adminURL = env('APP_ADMIN_URL', 'test')}}
@extends('Admin/layouts.app')
@section('content')

<div class="row">
    <div class="col-12">

        <h1>{{ isset($organizer) ? 'Редактировать запись' : 'Создать запись' }}</h1>

        <div class="panel panel-default">
            <div class="panel-body">

                @if (isset($organizer))
                    <form action="{{ route('admin.organizers.update', $organizer->id) }}" method="post">
                        <input type="hidden" name="_method" value="PUT">
                @else
                     <form action="{{ route('admin.organizers.store') }}" method="post">
                @endif

                    {{ csrf_field() }}
                    <div class="form-group col-lg-4">
                        <label for="">Название</label>
                        <input type="text" name="name" class="form-control" value="{{ isset($organizer) ? $organizer->name : '' }}">
                    </div>
                    <div class="form-group col-lg-4">
                        <label for="">Ссылка на сайт</label>
                        <input type="text" name="website" class="form-control" value="{{ isset($organizer) ? $organizer->website : '' }}">
                    </div>
                    <div class="form-group col-lg-4">
                        <label for="">FB id</label>
                        <input type="text" name="fb_group_id" class="form-control" value="{{ isset($organizer) ? $organizer->fb_group_id : '' }}">
                    </div>

                        {{--<div class="form-group col-lg-4">--}}
                            {{--<label for="">Страна</label>--}}
                            {{--<select theme="google" name="country" class="form-control selectpicker countrypicker">--}}
                                {{--@foreach($country as $coun)--}}
                                    {{--<option value="{{ $coun->id }}"--}}
                                    {{--@if (isset($organizer->id) && ($coun->id == $organizer->country_id || $countryId))--}}
                                        {{--selected--}}
                                    {{--@endif>{{ $coun->name_ru }}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                        {{--</div>--}}

                        {{--<div class="form-group col-lg-4 city_ajax"></div>--}}


                         <div class="form-group col-lg-4">
                             <label for="description">Описание</label>
                             <textarea class="form-control" id="description" rows="8" name="body_m">{{ isset($organizer) ? $organizer->body_m : ''}}</textarea>
                         </div>

                         <div class="form-group col-lg-4">
                             <label for="">Теги организатора</label>
                             <select class="tags-input" multiple name="tags[]">
                                 @if(isset($organizer->id))
                                     @foreach ($tags as $item)
                                         <option value="{{ $item->id }}" selected>{{ $item->tag }}</option>
                                     @endforeach
                                 @endif
                             </select>
                         </div>

                        <div class="form-group col-lg-4">
                            <button class="btn btn-success">
                                @if (isset($organizer->id))
                                    Сохранить
                                @else
                                    Создать
                                @endif
                            </button>
                        </div>
                    </form>
                </div>
            </div>
    </div>
</div>

@endsection
@section('footer_script')
    <link href="/admin/css/tokenize2.min.css" rel="stylesheet">
    <script src="/admin/js/tokenize2.min.js"></script>

    <script>
        $('.tags-input').tokenize2({
            searchMinLength: 3,
            dataSource: function(text, object){
                $.ajax('/{{ $adminURL }}/tags/get', {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        text: text
                    },
                    type: 'POST',
                    dataType: 'json',
                    success: function(data){
                        var $items = [];
                        $.each(data, function(k, v){
                            $items.push(v);
                        });

                        object.trigger('tokenize:dropdown:fill', [$items]);
                    }
                });
            }
        });
    </script>

    <script>
        $('select[name="country"]').change(function () {
            var country_id = $(this).val();
            $.ajax({
                type: 'POST',
                url: '{{ route('admin.organizers.loading.city') }}',
                data: {"country_id": country_id},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    $('.city_ajax').html(data);
                }
            });
        });
        @if (isset($organizer->id))
        $.ajax({
            type: 'POST',
            url: '{{ route('admin.organizers.loading.city') }}',
            data: {
                "country_id": "{{ $countryId }}",
                "city_id": "{{ $organizer->city_id }}"
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $('.city_ajax').html(data);
                console.log(data);
            }
        });
        @endif
    </script>
@stop