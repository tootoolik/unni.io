<label for="">Город</label>
<select theme="google" name="city" id="" class="form-control" placeholder="Выбрать страну" data-search="true">
    @foreach($cities as $city)
        <option value="{{ $city->id }}" @if (isset($city_id) && $city->id == $city_id)
        selected
                @endif>{{ $city->name_ru }}</option>
    @endforeach
</select>