@extends('Admin/layouts.app')
@section('content')
    <div class="row">
        <div class="col-12">
            <h1>
                @if ($organizer->id)
                    Редактировать запись
                @else
                    Создать запись
                @endif
            </h1>
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="{{ route('admin.organizers.update', $organizer->id) }}" method="post">
                        <div class="form-group col-lg-4">
                            <label for="">Название</label>
                            <input type="text" class="form-control" value="{{ $organizer->name or '' }}">
                        </div>
                        <div class="form-group col-lg-4">
                            <label for="">Ссылка на сайт</label>
                            <input type="text" class="form-control" value="{{ $organizer->website or '' }}">
                        </div>
                        <div class="form-group col-lg-4">
                            <label for="">FB id</label>
                            <input type="text" class="form-control" value="{{ $organizer->fb_group_id or '' }}">
                        </div>
                        <div class="form-group col-lg-4">
                            <label for="">Страна</label>
                            <select theme="google" name="" id="" class="form-control" placeholder="Выбрать страну"
                                    data-search="true">
                                @foreach($country as $coun)
                                    <option value="{{ $coun->id }}">{{ $coun->name_ru }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-lg-4 city_ajax"></div>
                        <div class="form-group col-lg-4">
                            <button class="btn btn-success">Добавить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer_script')
    <script src="/admin/vendor/select/selectstyle.js"></script>
    <link href="/admin/vendor/select/selectstyle.css" rel="stylesheet" type="text/css">
    <script>
        $('select').selectstyle({
            onchange: function (val) {
                console.log(val);
            }
        });
        $('.ss_ul li').click(function () {
            var country_id = $(this).attr('value');
            $.ajax({
                type: 'POST',
                url: '{{ route('admin.organizers.loading.city') }}',
                data: {"country_id": country_id},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    $('.city_ajax').html(data);
                }
            });
        });
    </script>
@stop