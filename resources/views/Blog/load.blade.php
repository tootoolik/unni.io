<div id="load">
    <ul class="list_result_ul">
        @if(!empty($posts))
            @foreach($posts as $post)

{{--                @php--}}
{{--                    $url = '';--}}
{{--                    if(isset($post->linksContent))--}}
{{--                        $url = $post->linksContent->url;--}}
{{--                @endphp--}}

                <li>
                    <div class="col-8 tablet">
                        <div>
                            <h3 class="title-blok">{{ $post->name }}</h3>
                            <noindex>
                                <a href="/blog/{{ $post->id }}" target="_blank" rel="nofollow">
                                    <i class="fas fa-external-link-alt"></i>
                                </a>
                            </noindex>
                        </div>
{{--                        <div class="list_result_ul_chanel">--}}
{{--                            <a id="list_result_ul_chanel"--}}
{{--                               href="{{ route('front.domains', str_replace('www.', '', parse_url($url, PHP_URL_HOST))) }}"--}}
{{--                               target="_blank">--}}
{{--                                {{ '@ '.str_replace('www.', '', parse_url($url, PHP_URL_HOST)) }}--}}
{{--                            </a>--}}
{{--                        </div>--}}
                        <div>
                            @if($post->body)
                                {!! htmlspecialchars_decode($post->body) !!}
                            @endif
                        </div>
                        <div class="dataindex">Дата: {{ $post->created_at }}</div>
                    </div>
                    <div class="col-4 tablet">
                            <a class="images_popup" href="{{ $post->post_img }}">
                                <img src="{{ $post->post_img ?? asset('images/default-200x200.png') }}" alt="">
                            </a>
                    </div>
                    <div class="col-12" id="feed-tags">

                        @php
                            $tags = $post->tags->pluck('name')->toArray();
                            $tags = array_map(function ($v){
                                return mb_strtolower($v);
                            },$tags);
                        @endphp

                        @foreach (array_unique($tags) as $tag)
                            {{--<a href="/?search={{$tag->tag}}&typeSearch=search_name">--}}
                            <span class="tags-link">{{$tag}}</span>
                            {{--</a>--}}
                        @endforeach
                    </div>
                </li>
            @endforeach

        @endif
    </ul>
    {{ $posts->links('paginate_one') }}
</div>
