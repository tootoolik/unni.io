@extends('layouts.app')

@section('meta_title')
    Статьи ИТ {{$title_page}} на Unni.io
@endsection

@section('meta_description')
    Статьи ИТ {{$title_page}} на Unni.io
@endsection

@section('content')
    {{--<h3 class="panel-heading textCenter"></h3>--}}
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="mainBlocks">
        <div class="list_result">
            <div class="searchPageAjax">
                @include('Blog.item')
            </div>
        </div>
    </div>
@endsection
