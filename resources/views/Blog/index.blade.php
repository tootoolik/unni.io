@extends('layouts.app')

@section('meta_title')
    Новости ИТ {{$title_page}} на Unni.io
@endsection

@section('meta_description')
    Новости ИТ {{$title_page}} на Unni.io в Украине, поиск ИТ- компаний, поиск стартапов и новостей мира технологий
@endsection

@section('content')
    {{--<h3 class="panel-heading textCenter"></h3>--}}
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="mainBlocks">
        <div class="list_result">
            <h1>Блог пользовательских публикаций</h1>
            <div class="searchPageAjax">
                @include('Blog.load')
            </div>

        </div>
    </div>
@endsection
