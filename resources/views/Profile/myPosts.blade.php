@extends('layouts.app')

@section('content')
    <div class="container pageTags">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <h3 class="panel-heading textCenter">Мои публикации</h3>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <a href="{{ route('front.profile.posts.my') }}" class="btn btn-success">Все публикации</a>
                                <a href="{{ route('front.profile.posts.my.approved') }}" class="btn btn-success">Одобренные публикации</a>
                                <a href="{{ route('front.profile.posts.my.moderation') }}" class="btn btn-danger">На модерации</a>
                                <br>
                                <br>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Наименование</th>
                                        <th>Изображение</th>
                                        <th>Дата создания</th>
                                        <th>Статус</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($posts as $post)
                                        <tr>
                                            <td>{{$post->id}}</td>
                                            <td>{{$post->name}}</td>
                                            <td>
                                                @if( $post->post_img != NULL || $post->post_img != '' )
                                                    <img src="/storage/{{$post->post_img}}" alt="img" width="150px" class="pb-3">
                                                @endif
                                            </td>
                                            <td>{{$post->created_at}}</td>
                                            <td>
                                                @if( $post->active == '1' )
                                                    Опубликован
                                                @else
                                                    На модерации
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 add-post">
                                <a href="{{ route('home') }}">Вернуться в кабинет</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
