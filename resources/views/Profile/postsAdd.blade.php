@extends('layouts.app')

@section('content')
    <div class="container pageTags">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <h3 class="panel-heading textCenter">Создать новую публикацию</h3>
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-lg-12 add-post">
                                <form method="POST" enctype="multipart/form-data" action="{{ route('front.profile.posts.store') }}">
                                    {{ csrf_field() }}

                                    <div class="form-group col-lg-12">
                                        <label for="">Заголовок</label>
                                        <input type="text" name="post_name" class="form-control col-lg-12" value="">
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <label for="description">Контент</label>
                                        <textarea class="form-control col-lg-12" name="post_content" id="chanel_content"></textarea>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label for="">Изображени поста</label>
                                        <input type="file" name="post_img">
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label for="">Теги</label>
                                        <select class="tags-input col-lg-12" multiple name="tags[]">

                                        </select>
                                    </div>
                                    <input type="hidden" name="post_author[]" value="{{$user}}">
                                    <div class="form-group flex-button-wrapper flex-end col-lg-12">
                                        <button class="post-create-button">Создать публикацию</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 add-post">
                                <a href="{{ route('home') }}">Вернуться в кабинет</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <link href="/admin/css/tokenize2.min.css" rel="stylesheet">
    <script src="/admin/js/tokenize2.min.js" defer=""></script>
    <script src="{{ asset('/ckeditor/ckeditor.js') }}"
            type="text/javascript" charset="utf-8" ></script>
    <script>
        var editor = CKEDITOR.replace( 'post_content',{
            filebrowserBrowseUrl : '/elfinder/ckeditor'
        } );
    </script>
@stop
